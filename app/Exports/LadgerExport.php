<?php

namespace App\Exports;

use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;
use Maatwebsite\Excel\Concerns\WithStyles;
use Maatwebsite\Excel\Concerns\WithColumnWidths;
use PhpOffice\PhpSpreadsheet\Worksheet\Worksheet;

class LadgerExport implements FromView
{
    protected $levels ='';
    // protected $level1 ='';
    // protected $level2 ='';
    // protected $accounts = '';
    // protected $fromDate ='';
    // protected $toDate ='';

    public function __construct($levels) {
        $this->levels = $levels;
        // $this->level1 = $level1;
        // $this->level2 = $level2;
        // $this->accounts = $accounts;
        // $this->fromDate = $fromDate;
        // $this->toDate  = $toDate;
        
    }

    public function view(): View
    {
        return view('admin.report.genral_ladger_excel', [
            'levels' => $this->levels,
            // 'level1' => $this->level1,
            // 'level2' => $this->level2,
            // 'accounts' => $this->accounts,
            // 'fromDate' => $this->fromDate,
            // 'toDate' => $this->toDate,
        ]);
    }

    public function columnWidths(): array
    {
        return [
            'A' => 25,
            'B' => 35,       
            'C' => 25,        
            'D' => 25,        
            'E' => 25,        
            'F' => 25,        
            'G' => 25,        
            'H' => 25,        
        ];
    }

    public function styles(Worksheet $sheet)
    {
        $sheet->getStyle('A')->getAlignment()->setVertical('center');
        $sheet->getStyle('B')->getAlignment()->setVertical('center');
        $sheet->getStyle('C')->getAlignment()->setVertical('center');
        $sheet->getStyle('D')->getAlignment()->setVertical('center');
        $sheet->getStyle('E')->getAlignment()->setVertical('center');
        $sheet->getStyle('F')->getAlignment()->setVertical('center');
        $sheet->getStyle('G')->getAlignment()->setVertical('center');
        $sheet->getStyle('H')->getAlignment()->setVertical('center');
        return [
            // Style the first row as bold text.
            1    => ['font' => ['bold' => true]],
            'A' => ['alignment' => ['wrapText' => true]],
            'B' => ['alignment' => ['wrapText' => true]],
            'C' => ['alignment' => ['wrapText' => true]],
            'D' => ['alignment' => ['wrapText' => true]],
            'E' => ['alignment' => ['wrapText' => true]],
            'F' => ['alignment' => ['wrapText' => true]],
            'G' => ['alignment' => ['wrapText' => true]],
            'H' => ['alignment' => ['wrapText' => true]]
        ];
    }
}
