@extends('admin.layouts.master')
@section('title', 'Edit Account')
@section('content')
<style>
    .btnrelated {
        display: inline-block;
        padding: 6px 12px;
        margin-bottom: 0;
        font-size: 14px;
        font-weight: normal;
        line-height: 1.42857143;
        text-align: center;
        white-space: nowrap;
        vertical-align: middle;
        cursor: pointer;
        -webkit-user-select: none;
        -moz-user-select: none;
        -ms-user-select: none;
        user-select: none;
        background-image: none;
        border: 1px solid transparent;
        border-radius: 4px;
    }

    /*Also */
    .btnrelated-success {
        border: 1px solid #c5dbec;
        background: #d0e5f5;
        font-weight: bold;
        color: #2e6e9e;
    }

    /* This is copied from https://github.com/blueimp/jQuery-File-Upload/blob/master/css/jquery.fileupload.css */
    .fileinput-button {
        position: relative;
        overflow: hidden;
    }

    .fileinput-button input {
        position: absolute;
        top: 0;
        right: 0;
        margin: 0;
        opacity: 0;
        /* -ms-filter: "alpha(opacity=0)"; */
        font-size: 200px;
        direction: ltr;
        cursor: pointer;
    }

    .thumb {
        height: 80px;
        width: 100px;
        border: 1px solid #000;
    }

    ul.thumb-Images li {
        width: 120px;
        /* float: left; */
        display: inline-block;
        vertical-align: top;
        height: 120px;
    }

    .img-wraprelated {
        position: relative;
        display: inline-block;
        font-size: 0;
    }

    .img-wraprelated .closerelated {
        position: absolute;
        top: 2px;
        right: 2px;
        z-index: 100;
        background-color: #d0e5f5;
        padding: 5px 2px 2px;
        color: #000;
        font-weight: bolder;
        cursor: pointer;
        opacity: 0.5;
        font-size: 23px;
        line-height: 10px;
        border-radius: 50%;
    }

    .img-wraprelated:hover .closerelated {
        opacity: 1;
        background-color: #ff0000;
    }

    .FileNameCaptionStyle {
        font-size: 12px;
    }
</style>
<section class="content-wrapper container-xxl p-0">
    <div class="content-header row">
        <div class="content-header-left col-md-9 col-12 mb-2">
            <div class="row breadcrumbs-top">
                <div class="col-12">
                    <h2 class="content-header-title float-start mb-0">Edit Account</h2>
                    <div class="breadcrumb-wrapper">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="{{ url('/dashboard') }}">Dashboard</a>
                            </li>
                            <li class="breadcrumb-item"><a href="{{ url('/accounts') }}">Accounts</a>
                            </li>
                            <li class="breadcrumb-item active">Edit Account
                            </li>
                        </ol>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="content-body">
        <section id="basic-datatable">
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-body">
                            <form class="form" id="account_form" enctype="multipart/form-data">
                                @csrf
                                @method('PUT')
                                <div class="row">
                                    <div class="col-md-6 col-12">
                                        <div class="mb-1">
                                            <label class="form-label" for="role">Select Level</label>
                                            <select name="level2" id="level2" class="select2 form-select" data-placeholder="Select Level">
                                                <option value=""></option>
                                                @foreach ($levels as $level)
                                                <option value="{{$level->id}}" {{$account->level2_code == $level->code ? 'selected' : ' '}}>{{$level->code . '-'. $level->description}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-6 col-12">
                                        <div class="mb-1">
                                            <label class="form-label" for="code">Code</label>
                                            <input type="text" name="code" id="code" class="form-control" value="{{$account->account_code}}" placeholder="Enter Code" required />
                                        </div>
                                    </div>
                                    {{-- <div class="col-md-4 col-12">
                                        <div class="mb-1">
                                            <label class="form-label" for="voucher_type"> Select Account Group</label>
                                            <select name="account_class" id="account_class" class="select2 form-select" data-placeholder="Select Voucher Type" required disabled >
                                                <option value=""></option>
                                                @foreach ($account_class as $account_clas)
                                                <option value="{{ $account_clas->id }}" {{$account_clas->id==$account->account_class_id ? 'selected' : ''}}>{{ $account_clas->name }}
                                                </option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div> --}}
                                    <div class="col-md-6 col-12">
                                        <div class="mb-1">
                                            <label class="form-label" for="description">Description</label>
                                            <input type="text" name="description" id="description" class="form-control" value="{{$account->description}}" placeholder="Enter Description" required />
                                        </div>
                                    </div>
                                    <div class="col-md-6 col-12">
                                        <div class="mb-1">
                                            <label class="form-label" for="gl_type">Gl Type</label>
                                            <input type="text" name="gl_type" id="gl_type" class="form-control" value="{{$account->gl_type}}" required />
                                        </div>
                                    </div>
                                    @if($account->gl_type == 'Customer')
                                        <input type="hidden" name="customer_id" value="{{$customer->id}}">
                                        <div class="col-md-4 col-12">
                                            <div class="mb-1">
                                                <label class="form-label" for="customer_name">Customer Name</label>
                                                <input type="text" name="customer_name" id="customer_name" class="form-control" value="{{$account->name}}" placeholder="Customer Name" required/>
                                            </div>
                                        </div>
                                        <div class="col-md-4 col-12">
                                            <div class="mb-1">
                                                <label class="form-label" for="customer_short_name">Short Name</label>
                                                <input type="text" name="customer_short_name" id="customer_short_name" class="form-control" value="{{$account->short_name}}" placeholder="Short Name" required/>
                                            </div>
                                        </div>
                                        <div class="col-md-4 col-12">
                                            <div class="mb-1">
                                                <label class="form-label" for="customer_email">Email</label>
                                                <input type="email" name="customer_email" id="customer_email" class="form-control" value="{{$account->email}}" placeholder="Email" required/>
                                            </div>
                                        </div>
                                        <div class="col-md-4 col-12">
                                            <div class="mb-1">
                                                <label class="form-label" for="customer_contact_no">Contact No</label>
                                                <input type="text" name="customer_contact_no" id="customer_contact_no" class="form-control" value="{{$account->contact_no}}" placeholder="Contact no" required/>
                                            </div>
                                        </div>
                                        <div class="col-md-4 col-12">
                                            <div class="mb-1">
                                                <label class="form-label" for="customer_ntn">NTN</label>
                                                <input type="text" name="customer_ntn" id="customer_ntn" class="form-control" value="{{$account->ntn}}" placeholder="NTN" />
                                            </div>
                                        </div>
                                        <div class="col-md-4 col-12">
                                            <div class="mb-1">
                                                <label class="form-label" for="customer_stn">STN</label>
                                                <input type="text" name="customer_stn" id="customer_stn" class="form-control" value="{{$account->stn}}" placeholder="STN" />
                                            </div>
                                        </div>
                                        <div class="col-12">
                                            <div class="mb-1">
                                                <label class="form-label" for="customer_address">Address</label>
                                                <input type="text" name="customer_address" id="customer_address" class="form-control" value="{{$account->address}}" placeholder="Customer Address" />
                                            </div>
                                        </div>
                                    @endif
                                    @if($account->gl_type == 'Supplier')
                                        <input type="hidden" name="supplier_id" value="{{$supplier->id}}">
                                        <div class="col-md-4 col-12">
                                            <div class="mb-1">
                                                <label class="form-label" for="suplier_name">Supplier Name</label>
                                                <input type="text" name="suplier_name" id="suplier_name" class="form-control" value="{{$account->name}}" placeholder="Supplier Name" required/>
                                            </div>
                                        </div>
                                        <div class="col-md-4 col-12">
                                            <div class="mb-1">
                                                <label class="form-label" for="supplier_short_name">Short Name</label>
                                                <input type="text" name="supplier_short_name" id="supplier_short_name" class="form-control" value="{{$account->short_name}}" placeholder="Supplier Short Name" required/>
                                            </div>
                                        </div>
                                        <div class="col-md-4 col-12">
                                            <div class="mb-1">
                                                <label class="form-label" for="supplier_email">Email</label>
                                                <input type="email" name="supplier_email" id="supplier_email" class="form-control" value="{{$account->email}}" placeholder="Supplier Email" required/>
                                            </div>
                                        </div>
                                        <div class="col-md-4 col-12">
                                            <div class="mb-1">
                                                <label class="form-label" for="supplier_contact_no">Contact No</label>
                                                <input type="number" name="supplier_contact_no" id="supplier_contact_no" class="form-control" value="{{$account->contact_no}}" placeholder="Supplier Contact No" required/>
                                            </div>
                                        </div>
                                        <div class="col-md-4 col-12">
                                            <div class="mb-1">
                                                <label class="form-label" for="ntn">NTN</label>
                                                <input type="text" name="supplier_ntn" id="supplier_ntn" class="form-control" value="{{$account->ntn}}" placeholder="Enter Supplier NTN" />
                                            </div>
                                        </div>
                                        <div class="col-md-4 col-12">
                                            <div class="mb-1">
                                                <label class="form-label" for="supplier_stn">STN</label>
                                                <input type="text" name="supplier_stn" id="supplier_stn" class="form-control" value="{{$account->stn}}" placeholder="Enter Supplier STN" />
                                            </div>
                                        </div>
                                        <div class="col-12">
                                            <div class="mb-1">
                                                <label class="form-label" for="supplier_address">Address</label>
                                                <input type="text" name="supplier_address" id="supplier_address" class="form-control" value="{{$account->address}}" placeholder="Enter Supplier Address" />
                                            </div>
                                        </div>
                                    @endif
                                    @if($account->gl_type == 'Bank')
                                    @if (!empty($bank_cash))
                                        <input type="hidden" name="bankCash_id" value="{{$bank_cash->id}}">
                                        <div class="col-md-4 col-12">
                                            <div class="mb-1">
                                                <label class="form-label" for="bank_name">Bank Name</label>
                                                <input type="text" name="bank_name" id="bank_name" class="form-control" value="{{$account->name}}" placeholder="Bank Name" required/>
                                            </div>
                                        </div>
                                        <div class="col-md-4 col-12">
                                            <div class="mb-1">
                                                <label class="form-label" for="account_title">Account Title</label>
                                                <input type="text" name="account_title" id="account_title" class="form-control" value="{{$account->account_title}}" placeholder="Account Title" required/>
                                            </div>
                                        </div>
                                        <div class="col-md-4 col-12">
                                            <div class="mb-1">
                                                <label class="form-label" for="account_number">Account Number</label>
                                                <input type="text" name="account_number" id="account_number" class="form-control" value="{{$account->account_number}}" placeholder="Account Number"/>
                                            </div>
                                        </div>
                                        <div class="col-md-6 col-12">
                                            <div class="mb-1">
                                                <label class="form-label" for="location">Location</label>
                                                <input type="text" name="location" id="location" class="form-control" value="{{$account->location}}" placeholder="Bank Location" />
                                            </div>
                                        </div>
                                        <div class="col-md-6 col-12">
                                            <div class="mb-1">
                                                <label class="form-label" for="bank_address">Branch Address</label>
                                                <input type="text" name="bank_address" id="bank_address" class="form-control" value="{{$account->address}}" placeholder="Bank Address" />
                                            </div>
                                        </div>
                                        
                                    @else
                                        <div class="col-md-4 col-12">
                                            <div class="mb-1">
                                                <label class="form-label" for="bank_name">Bank Name</label>
                                                <input type="text" name="bank_name" id="bank_name" class="form-control" placeholder="Bank Name" required/>
                                            </div>
                                        </div>
                                        <div class="col-md-4 col-12">
                                            <div class="mb-1">
                                                <label class="form-label" for="account_title">Account Title</label>
                                                <input type="text" name="account_title" id="account_title" class="form-control" placeholder="Account Title" required/>
                                            </div>
                                        </div>
                                        <div class="col-md-4 col-12">
                                            <div class="mb-1">
                                                <label class="form-label" for="account_number">Account Number</label>
                                                <input type="text" name="account_number" id="account_number" class="form-control" placeholder="Account Number" required/>
                                            </div>
                                        </div>
                                        <div class="col-md-6 col-12">
                                            <div class="mb-1">
                                                <label class="form-label" for="location">Location</label>
                                                <input type="text" name="location" id="location" class="form-control" placeholder="Bank Location" />
                                            </div>
                                        </div>
                                        <div class="col-md-6 col-12">
                                            <div class="mb-1">
                                                <label class="form-label" for="bank_address">Branch Address</label>
                                                <input type="text" name="bank_address" id="bank_address" class="form-control" placeholder="Bank Address" />
                                            </div>
                                        </div>
                                    @endif
                                    @endif
                                    <div class="col-md-2">
                                        <div class="mb-1">
                                            <div class="form-check">
                                                <input class="form-check-input" type="checkbox" id="tag"
                                                    name="tag" value="1" {{$account->tag == "1" ? 'checked' : ''}}>
                                                <label class="form-check-label" for="tag">
                                                    Tag
                                                </label>
                                            </div>
                                        </div>
                                    </div>
                                    @if($account->gl_type == 'Customer' || $account->gl_type == 'Supplier')
                                    <div class="col-md-4 pb-2" id="is_exemption">
                                        <div class="mb-1">
                                            <div class="form-check">
                                                <input class="form-check-input" type="checkbox" id="isExemption" name="isExemption" value="1" {{$account->exemption_certificate == "1" ? 'checked' : ''}}>
                                                <label class="form-check-label" for="isExemption">
                                                    Exemption Certificate
                                                </label>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row" id="exemption_certificates">
                                        <div class="exemptionCertificates row">
                                            <div data-repeater-list="exemption_certificates">
                                                @if(count($exemption_certificates) > 0)
                                                    @foreach($exemption_certificates as $exemption_certificates)
                                                    <div data-repeater-item>
                                                        <input type="hidden" id="id" name="id" value="{{$exemption_certificates->id}}">
                                                        <div class="row d-flex align-items-end">
                                                            <div class="mb-1 col-md-4 col-12">
                                                                <label class="form-label" for="exemption_certificateNo">Exemption Certificate Number</label>
                                                                <input type="type" id="exemption_certificateNo" class="form-control" autocomplete="off" value="{{$exemption_certificates->exemption_certificate_number}}" placeholder="Enter Exemption Certificate Number " name="exemption_certificate_number" required />
                                                            </div>
                                                            <div class="mb-1 col-md-4 col-12">
                                                                <label class="form-label" for="from_date">From Date</label>
                                                                <input type="date" id="from_date" class="form-control" autocomplete="off" value="{{$exemption_certificates->from_date}}" placeholder="" name="from_date" required />
                                                            </div>
                                                            <div class="mb-1 col-md-4 col-12">
                                                                <label class="form-label" for="expire_date">Expire Date</label>
                                                                <input type="date" id="expire_date" class="form-control" autocomplete="off" value="{{$exemption_certificates->expire_date}}" placeholder="" name="expire_date" required />
                                                            </div>
                                                            <div class="mb-1 col-12">
                                                                <label class="form-label" for="image_file">Certificate Image</label>
                                                                <input type="file" name="image_file" id="image_file" class="form-control" value="" />
                                                            </div>
                                                            <div class="col-md-2 col-12 mb-51">
                                                                <div class="mb-1">
                                                                    <button class="btn btn-outline-danger text-nowrap px-1" data-repeater-delete type="button">
                                                                        <i data-feather="x" class="me-25"></i>
                                                                        <span>Delete</span>
                                                                    </button>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    @endforeach
                                                @else
                                                <div data-repeater-item>
                                                    <input type="hidden" id="id" name="id">
                                                    <div class="row d-flex align-items-end">
                                                        <div class="mb-1 col-md-4 col-12">
                                                            <label class="form-label" for="exemption_certificateNo">Exemption Certificate Number</label>
                                                            <input type="type" id="exemption_certificateNo" class="form-control" autocomplete="off" placeholder="Enter Exemption Certificate Number " name="exemption_certificate_number" required />
                                                        </div>
                                                        <div class="mb-1 col-md-4 col-12">
                                                            <label class="form-label" for="from_date">From Date</label>
                                                            <input type="date" id="from_date" class="form-control" autocomplete="off" placeholder="" name="from_date" required />
                                                        </div>
                                                        <div class="mb-1 col-md-4 col-12">
                                                            <label class="form-label" for="expire_date">Expire Date</label>
                                                            <input type="date" id="expire_date" class="form-control" autocomplete="off" placeholder="" name="expire_date" required />
                                                        </div>
                                                        <div class="mb-1 col-12">
                                                            <label class="form-label" for="image_file">Certificate Image</label>
                                                            <input type="file" name="image_file" id="image_file" class="form-control" placeholder="Image" />
                                                        </div>
                                                        <div class="col-md-2 col-12 mb-51">
                                                            <div class="mb-1">
                                                                <button class="btn btn-outline-danger text-nowrap px-1" data-repeater-delete type="button">
                                                                    <i data-feather="x" class="me-25"></i>
                                                                    <span>Delete</span>
                                                                </button>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                @endif
                                            </div>
                                            <div class="row">
                                                <div class="col-12">
                                                    <button class="btn btn-icon btn-primary" type="button" data-repeater-create>
                                                        <i data-feather="plus" class="me-25"></i>
                                                        <span>Add New</span>
                                                    </button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    @endif
                                </div>
                                <div class="d-flex justify-content-end">
                                    <a href="{{route('accounts.index')}}" class="btn btn-outline-danger">Cancel</a>
                                    <button type="submit" class="btn btn-primary ms-1" id="save">Save</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
        </section>
    </div>
</section>
@endsection
@section('scripts')
<script>
    $("#code").inputmask({
        "mask": "9999"
    });
    $("#edit_code").inputmask({
        "mask": "9999"
    });
    var rowid;
    $(document).ready(function() {
        if ($('#isExemption').is(":checked")) {
            $('#exemption_certificates').show();
        }
        else {
            $('#exemption_certificates').hide();
            $('#exemption_certificateNo').attr('required', false);
            $('#from_date').attr('required', false);
            $('#expire_date').attr('required', false);
        }
        $('.exemptionCertificates').repeater({
            isFirstItemUndeletable: true,
            show: function() {
                $(this).slideDown();
                // Feather Icons
                if (feather) {
                    feather.replace({
                        width: 14,
                        height: 14
                    });
                }
            },
            hide: function(deleteElement) {
                $(this).slideUp(deleteElement);
            }
        });

        $("#isExemption").change(function() {
            if (this.checked) {
                $('#exemption_certificates').show();
            } else {
                $('#exemption_certificates').hide();
            }
        });

        $("#account_form").submit(function(e) {
            e.preventDefault();
            ButtonStatus('#save',true);
            blockUI();
            $.ajax({
                url: "{{ url('accounts') }}" + "/" + '{!! $account->id !!}',
                type: "POST",
                data: new FormData(this),
                processData: false,
                contentType: false,
                responsive: true,
                success: function(response) {
                    ButtonStatus('#save',false);
                    $.unblockUI();
                    if (response.errors) {
                        $.each(response.errors, function(index, value) {
                            Toast.fire({
                                icon: 'error',
                                title: value
                            })
                        });
                    }
                    else if (response.error_message) {
                        Toast.fire({
                            icon: 'error',
                            title: response.error_message
                        })
                    } 
                    else {
                        $('#account_form')[0].reset();
                        Toast.fire({
                            icon: 'success',
                            title: 'Account has been Added Successfully!'
                        })
                        setTimeout(function(){
                            window.location.href = "{{ url('accounts') }}";
                        }, 1000);
                    }
                }
            });
        });
    });
</script>
@endsection