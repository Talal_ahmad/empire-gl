<?php

namespace App\Exports;

use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;
use Maatwebsite\Excel\Concerns\WithStyles;
use Maatwebsite\Excel\Concerns\WithColumnWidths;
use PhpOffice\PhpSpreadsheet\Worksheet\Worksheet;

class ChartExport implements FromView,WithColumnWidths,WithStyles
{
    protected $levels = '';
    protected $level_1 = '';
    protected $level_2 = '';
    protected $account = '';

    public function __construct($levels , $level_1 , $level_2 , $account) {
        $this->levels = $levels;
        $this->level_1 = $level_1;
        $this->level_2 = $level_2;
        $this->account = $account;
    }

    public function view(): View
    {
        return view('admin.report.chart_report_pdf', [
            'levels' => $this->levels,
            'level_1' => $this->level_1,
            'level_2' => $this->level_2,
            'account' => $this->account,
        ]);
    }

    public function columnWidths(): array
    {
        return [
            'A' => 35,
            'B' => 50         
        ];
    }

    public function styles(Worksheet $sheet)
    {
        $sheet->getStyle('A')->getAlignment()->setVertical('center');
        $sheet->getStyle('B')->getAlignment()->setVertical('center');
        return [
            // Style the first row as bold text.
            1    => ['font' => ['bold' => true]],
            'A' => ['alignment' => ['wrapText' => true]],
            'B' => ['alignment' => ['wrapText' => true]]
        ];
    }
}