<?php

namespace App\Http\Controllers;

use App\Exports\BankAndCashBalancesExport;
use App\Models\BankCash;
use Illuminate\Http\Request;
use App\Models\Level1;
use App\Models\Level2;
use DB;
use PDF;
use App\Exports\ChartExport;
use App\Exports\TrialExport;
use App\Exports\EditExport;
use App\Exports\LadgerExport;
use App\Imports\LevelsImport;
use App\Models\Account;
use App\Models\Voucher;
use App\Models\Vouchertype;
use App\Models\VoucherDetail;
use Maatwebsite\Excel\Facades\Excel;
use App\Imports\AccountsImports;


class ReportController extends Controller
{
    public function ChartReport(Request $request)
    {
        // dd($request);
        $level_1 = DB::table('level_1')->select('id', 'code')->get();
        $level_2 = DB::table('level_2')->select('id', 'code')->get();
        $account = DB::table('accounts')->select('id', 'code')->get();
        $levels = Level1::query();
        if (!empty($request->level1)) {
            $levels->where('id', $request->level1);
        }
        $levels = $levels->with(['level2' => function ($level2) use ($request) {
            if (!empty($request->level2)) {
                $level2->where('id', $request->level2);
            }
        }, 'level2.accounts' => function ($account) use ($request) {
            if (!empty($request->account)) {
                $account->where('id', $request->account);
            }
        }])->get();
        if (request()->type == 'pdf') {
            $pdf = PDF::loadView('admin.report.chart_report_pdf', get_defined_vars());
            return $pdf->download('Chart.pdf');
        }
        if (request()->type == "excel") {
            return Excel::download(new ChartExport($levels, $level_1, $level_2, $account), 'Chart.xlsx');
        }
        // dd($levels);
        return view('admin.report.chart', compact('levels', 'level_1', 'level_2', 'account'));
    }


    public function TrailBalance(Request $request)
    {
        // dd($request);
        $levels = [];
        $level2From = '';
        $level2To = '';
        if (!empty($request->fromDate) && !empty($request->toDate)) {
            if (!empty($request->fromRange) && !empty($request->toRange)) {
                $fromRange = explode('-', $request->fromRange);
                $toRange = explode('-', $request->toRange);
                $level2From = $fromRange[0] . '-' . $fromRange[1];
                $level2To = $toRange[0] . '-' . $toRange[1];
            }
            $levels = Level1::query();
            if (!empty($request->fromRange) && !empty($request->toRange)) {
                $levels->where([['code', '>=', $fromRange[0]], ['code', '<=', $toRange[0]]]);
            }
            $levels = $levels->with(['level2' => function ($level2) use ($request, $level2From, $level2To) {
                if (!empty($request->fromRange) && !empty($request->toRange)) {
                    $level2->where([['code', '>=', $level2From], ['code', '<=', $level2To]]);
                }
                $level2->select('id', 'level_1_id', 'code', 'description');
            }, 'level2.accounts' => function ($account) use ($request) {
                if (!empty($request->fromRange) && !empty($request->toRange)) {
                    $account->where([['code', '>=', $request->fromRange], ['code', '<=', $request->toRange]]);
                }
                $account->select('id', 'level_2_id', 'code', 'description');
            }, 'level2.accounts.voucherdetails' => function ($voucher) use ($request) {
                $voucher->select('account_id', DB::raw('SUM(CASE WHEN date < "' . $request->fromDate . '" AND amount > 0 THEN amount ELSE 0 END) as openingDebitBalance'), DB::raw('SUM(CASE WHEN date < "' . $request->fromDate . '" AND amount < 0 THEN amount ELSE 0 END) as openingCreditsBalance'), DB::raw('SUM(CASE WHEN date < "' . $request->toDate . '" AND amount > 0 THEN amount ELSE 0 END) as closingDebitBalance'), DB::raw('SUM(CASE WHEN date < "' . $request->toDate . '" AND amount < 0 THEN amount ELSE 0 END) as closingCreditBalance'), DB::raw("SUM(CASE WHEN date >= '$request->fromDate' AND date <= '$request->toDate' AND amount > 0 THEN amount ELSE 0 END) as transcationDebit"), DB::raw("SUM(CASE WHEN date >= '$request->fromDate' AND date <= '$request->toDate' AND amount < 0 THEN amount ELSE 0 END) as transcationCredit"))->groupBy('account_id');
            }])->get();
            // dd($levels);
        }
        if (request()->type == 'pdf') {
            $pdf = PDF::loadView('admin.report.trial_balance_report', get_defined_vars());
            $pdf->setPaper('A4', 'landscape');
            return $pdf->download('TrialBalance.pdf');
        }
        if (request()->type == "excel") {
            return Excel::download(new TrialExport($levels), 'Trial.xlsx');
        }
        // dd('dfdfgdfgf');
        return view('admin.report.TrailBalance', compact('levels'));
    }
    public function getlevels($id)
    {

        $data = Level1::find($id)->level2;
        return response()->json($data);
    }
    public function getaccounts($id)
    {
        // dd('test');
        $data = Level2::find($id)->accounts;
        return response()->json($data);
    }

    public function generalLedger(Request $request)
    {
        // dd($request);
        $level1 = DB::table('level_1')->select('id', 'code', 'description')->get();
        $level2 = DB::table('level_2')->where('level_1_id', $request->level1)->select('id', 'code', 'description')->get();
        $accounts = DB::table('accounts')->where('level_2_id', $request->level2)->select('id', 'code', 'description')->get();
        $levels = [];
        $level2From = '';
        $level2To = '';
        if (!empty($request->fromDate) && !empty($request->toDate)) {
            if (!empty($request->fromRange) && !empty($request->toRange)) {
                $fromRange = explode('-', $request->fromRange);
                $toRange = explode('-', $request->toRange);
                $level2From = $fromRange[0] . '-' . $fromRange[1];
                $level2To = $toRange[0] . '-' . $toRange[1];
            }
            $levels = Level1::select('id');
            if (!empty($request->fromRange) && !empty($request->toRange)) {
                $levels->where([['code', '>=', $fromRange[0]], ['code', '<=', $toRange[0]]]);
            }
            $levels = $levels->with(['level2' => function ($level2) use ($request, $level2From, $level2To) {
                if (!empty($request->fromRange) && !empty($request->toRange)) {
                    $level2->where([['code', '>=', $level2From], ['code', '<=', $level2To]]);
                }
                $level2->select('id', 'level_1_id');
            }, 'level2.accounts' => function ($account) use ($request) {
                if (!empty($request->fromRange) && !empty($request->toRange)) {
                    $account->where([['code', '>=', $request->fromRange], ['code', '<=', $request->toRange]]);
                }
                $account->with(['voucherdetails' => function ($detail) use ($request) {
                    // $fromDate = !empty($request->fromDate) ? $request->fromDate : date('Y-m-d'); 
                    if (!empty($request->fromDate) && !empty($request->toDate)) {
                        $detail->where([
                            ['date', '>=', $request->fromDate],
                            ['date', '<=', $request->toDate],
                        ]);
                    }
                    $detail->with(['location', 'vouchertype', 'voucher' => function ($voucher) {
                        $voucher->select('id', 'igp_number', 'ogp_number', 'igp_ogp_date', 'cheque_no');
                    }])->select('id', 'gl_trans_id', 'account_id', 'voucher_type', 'location_id', 'date', 'amount', 'voucher_no', 'line_remarks')->orderBy('date', 'asc')->orderBy('id', 'asc');
                    // $detail->addSelect(['openingBalance' => VoucherDetail::whereDate('date', '<', $fromDate)
                    // ->select(DB::raw('SUM(amount)'))->groupBy('account_id')->take(1)]);
                }]);
                $account->select('id', 'code', 'description', 'level_2_id');
            }])->get();
            if (request()->type == 'pdf') {
                $filename = "";
                $filename = "GenralLadger_.pdf" . date("l jS \of F Y h:i:s A") . ".pdf";
                $pdf = PDF::loadView('admin.report.genral_ladger_pdf_excel', compact('levels', 'level1', 'level2', 'accounts'));
                $pdf->setPaper('A4', 'landscape');
                return $pdf->download($filename);
            }
            if (request()->type == "excel") {
                $filename = "";
                $filename = "GenralLadger_" . date("l jS \of F Y h:i:s A") . ".xlsx";
                return Excel::download(new LadgerExport($levels), 'generalLedger.xlsx');
            }
        }
        return view('admin.report.general_ledger', compact('levels', 'level1', 'level2', 'accounts'));
    }

    public function ImportLevels()
    {
        Excel::import(new LevelsImport, request()->file('file'));
        return redirect()->back();
    }
    public function   file_upload_for_accounts()
    {
        Excel::import(new AccountsImports, request()->file('file'));
        return redirect()->back();
    }
    public function uploadData()
    {
        return view('admin.report.upload_data');
    }
    public function editList(Request $request)
    {

        $VoucherTypes = Vouchertype::get();
        $vouchers = [];
        if (!empty($request->voucher_type) && $request->fromDate && $request->toDate) {
            $vouchers = Voucher::with('vouchertype', 'voucherdetails:id,gl_trans_id,account_id,date,amount,line_remarks', 'voucherdetails.account')->where([['voucher_type', $request->voucher_type], ['date', '>=', $request->fromDate], ['date', '<=', $request->toDate]])
                ->get(['id', 'voucher_type', 'voucher_no']);
        }
        // dd($vouchers);
        // $editList = DB::table('gl_trans_details')
        // ->join('vouhertypes','gl_trans_details.voucher_type','vouhertypes.id')
        // ->join('accounts','gl_trans_details.account_id','accounts.id');
        // // dd($editList);
        // if ($request->voucher_type) {
        //     $editList->where('gl_trans_details.voucher_type',$request->voucher_type);
        // }
        // if ($request->fromDate) {
        //     $editList->where('date','>=',date('Y-m-d',strtotime($request->fromDate)));
        // }
        // if ($request->toDate) {
        //     $editList->where('date','<=',date('Y-m-d',strtotime($request->toDate)));
        // }
        // $debit = $editList;
        // $credit = $editList;
        // // $debit = $debit->where('gl_trans_details.amount','>',0)->sum('gl_trans_details.amount');
        // // dd($debit);
        // $editList = $editList->select('vouhertypes.voucher_type','line_remarks','amount','accounts.code','accounts.description','gl_trans_details.date','gl_trans_details.voucher_no')->get();
        // $credit = $credit->where('gl_trans_details.amount','<',0)->sum('gl_trans_details.amount');
        // $debit = $debit->where('gl_trans_details.amount','>',0)->sum('gl_trans_details.amount');
        // // dd($editList);
        if (request()->type == 'pdf') {
            $pdf = PDF::loadView('admin.report.edit_list', get_defined_vars());
            $pdf->setPaper('A4', 'landscape');
            return $pdf->download('EditList.pdf');
        }
        if (request()->type == "excel") {
            return Excel::download(new EditExport($vouchers), 'Trial.xlsx');
        }
        return view('admin.report.edit_list_report', compact('VoucherTypes', 'vouchers'));
    }

    public function StatementOfDailyAffairs(Request $request)
    {
        $VoucherTypes = Vouchertype::get();
        if (!empty($request->fromDate) && !empty($request->toDate) || !empty($request->voucher_type)) {
            // dd($request->all());
            $voucher_details = VoucherDetail::join('accounts', 'gl_trans_details.account_id', '=', 'accounts.id')
                ->join('vouhertypes', 'gl_trans_details.voucher_type', '=', 'vouhertypes.id')
                ->select(
                    'gl_trans_details.id',
                    'accounts.description',
                    'vouhertypes.voucher_type as voucher_types_voucher',
                    'gl_trans_details.account_id',
                    'gl_trans_details.voucher_no',
                    'gl_trans_details.voucher_type',
                    'gl_trans_details.line_remarks',
                    'gl_trans_details.date',
                    'gl_trans_details.amount',
                    'gl_trans_details.created_at'
                )
                ->when($request->fromDate, function ($query) use ($request) {
                    $query->where('gl_trans_details.created_at', '>=', $request->fromDate);
                })
                ->when($request->toDate, function ($query) use ($request) {
                    $query->where('gl_trans_details.created_at', '<=', $request->toDate);
                })
                ->when($request->voucher_type, function ($query) use ($request) {
                    $query->where('gl_trans_details.voucher_type', '=', $request->voucher_type);
                })
                ->get();
            // dd($voucher_details);
        } else {
            $voucher_details = null;
            // dd($voucher_details);
        }
        return view('admin.report.statement_of_daily_affairs', get_defined_vars());
    }

    public function BankAndCashBalancesReport(Request $request)
    {
        // dd($request->all());
        $accounts = BankCash::join('accounts', 'bank_cash.account_id', '=', 'accounts.id')
            ->join('gl_trans', 'bank_cash.id', '=', 'gl_trans.bank_cash_id')
            ->select(
                'bank_cash.*',
                'accounts.code',
                'gl_trans.id as gl_trans_id',
                'gl_trans.debit_amount',
                'gl_trans.credit_amount',
                'gl_trans.created_at as gl_trans_created_at',
            )
            ->when($request->fromDate, function ($query) use ($request) {
                $query->where('gl_trans.created_at', '>=', $request->fromDate);
            })
            ->when($request->toDate, function ($query) use ($request) {
                $query->where('gl_trans.created_at', '<=', $request->toDate);
            })
            ->whereIn('accounts.gl_type', ['Bank', 'Cash'])
            // ->where('bank_cash.account_title', '!=', 'CASH IN HAND - UE')
            // ->where('bank_cash.name', '=', 'Cash')
            ->orderBy('bank_cash.id')
            ->get();

        // dd($accounts);
        $all_bank_accounts_sum = $accounts->where('name', '!=', 'Cash')->sum('debit_amount') + $accounts->where('name', '!=', 'Cash')->sum('credit_amount');
        // dd($all_bank_accounts);
        $accounts = $accounts->groupBy('account_title');
        // dd($accounts);
        $accounts = $accounts->map(function ($accounts_by_name) {
            $accounts_by_name = $accounts_by_name->groupBy('name');
            // dd($accounts_by_name);
            $accounts_by_name = $accounts_by_name->map(function ($bank_accounts) {
                $opening_balance = $bank_accounts->first();
                $total_debit_amount = $bank_accounts->sum('debit_amount');
                $total_credit_amount = $bank_accounts->sum('credit_amount');
                $closing_balance = $bank_accounts->sortByDesc('gl_trans_created_at')->sortByDesc('gl_trans_id')->first();
                return [
                    'accounts_code' => $opening_balance->code,
                    'opening_balance' => $opening_balance,
                    'total_debit_amount' => $total_debit_amount,
                    'total_credit_amount' => $total_credit_amount,
                    'closing_balance' => $closing_balance,
                ];
            });

            return $accounts_by_name;
        });

        // dd($accounts);
        return view('admin.report.bank_and_cash_balances', compact('accounts'));
    }
}
