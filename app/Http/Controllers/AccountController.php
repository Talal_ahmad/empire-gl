<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DataTables;
use App\Models\Account;
use App\Models\Level2;
use App\Models\Customer;
use App\Models\Supplier;
use App\Models\BankCash;
use App\Models\AccountClass;
use App\Models\AccountDetail;
use Illuminate\Support\Facades\DB;
use Spatie\Activitylog\Facades\LogBatch;
use Illuminate\Validation\ValidationException;

class AccountController extends Controller
{
    public function __construct(){
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if($request->ajax()){
            $account = Account::query();
            if(!empty($request->level_2_id)){
                $account->where('level_2_id', $request->level_2_id);
            }
            return DataTables::eloquent($account)->addIndexColumn()->make(true);
        }
        return view('admin.AccountCode.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $levels = Level2::get();
        $accountclass = DB::table('account_classes')->get();
        return view('admin.AccountCode.create',compact('levels','accountclass'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // dd($request);
        try{
            DB::transaction(function () use ($request) {
                $this->validate($request , [
                    'level2' => 'required',
                    // 'account_code' => 'required|unique:accounts',
                    'description' => 'required',
                    'gl_type' => 'required',
                ]);
                $account_code = strlen($request->account_code);
                // dd($account_code);
                if($account_code !== 4 ){
                    // dd('test');
                    throw new \Exception('Complete the 4 digit code.', 500);
                }
                $level2 = Level2::where('id','=',$request->level2)->first();
                $code = $level2->code.'-'.$request->account_code;
                if(Account::where('code', $code)->exists()) {
                    // return ['code'=>'22','custom_message'=>'error'];
                    throw new \Exception('Already Exist.', 500);

                }
                // if(strlen($account_code) !== 4){
                //     dd('test');
                //     throw new \Exception('Complete the 4 digit code.', 520);
                // }
                // LogBatch::withinBatch(function(string $uuid) use ($request) {
                //    dd($request);
                    $account = new Account();
                    $account->code = $code;
                    $account->level_2_id = $level2->id;
                    $account->account_code = $request->account_code;
                    $account->description = $request->description;
                    $account->level2_code = $level2->code;
                    $account->gl_type = $request->gl_type;
                    $account->tag = isset($request->tag) ? $request->tag : 0;
                    if($request->gl_type == 'Customer'){
                        $account->name = $request->customer_name;
                        $account->short_name = $request->customer_short_name;
                        $account->ntn = $request->customer_ntn;
                        $account->stn = $request->customer_stn;
                        $account->email = $request->customer_email;
                        $account->contact_no = $request->customer_contact_no;
                        $account->address = $request->customer_address;
                    }
                    if($request->gl_type == 'Supplier'){
                        $account->name = $request->suplier_name;
                        $account->short_name = $request->supplier_short_name;
                        $account->address = $request->supplier_address;
                        $account->ntn = $request->supplier_ntn;
                        $account->stn = $request->supplier_stn;
                        $account->email = $request->supplier_email;
                        $account->contact_no = $request->supplier_contact_no;
                    }
                    if($request->gl_type == 'Bank'){
                        $account->name = $request->bank_name;
                        $account->address = $request->bank_address;
                        $account->account_title = $request->account_title;
                        $account->account_number = $request->account_number;
                        $account->location = $request->location;
                    }
                    $account->exemption_certificate = !empty($request->isExemption) ? $request->isExemption : 0;
                    $account->save();
                    if($request->gl_type == 'Customer'){
                        $customer = new Customer();
                        $customer->account_id = $account->id;
                        $customer->name = $request->customer_name;
                        $customer->short_name = $request->customer_short_name;
                        $customer->ntn = $request->customer_ntn;
                        $customer->stn = $request->customer_stn;
                        $customer->email = $request->customer_email;
                        $customer->contact_no = $request->customer_contact_no;
                        $customer->address = $request->customer_address;
                        $customer->is_exemption = !empty($request->isExemption) ? $request->isExemption : 0;
                        $customer->save();
                    }
                    if($request->gl_type == 'Supplier'){
                        $supplier = new Supplier();
                        $supplier->account_id = $account->id;
                        $supplier->name = $request->suplier_name;
                        $supplier->short_name = $request->supplier_short_name;
                        $supplier->address = $request->supplier_address;
                        $supplier->ntn = $request->supplier_ntn;
                        $supplier->stn = $request->supplier_stn;
                        $supplier->email = $request->supplier_email;
                        $supplier->contact_no = $request->supplier_contact_no;
                        $supplier->is_exemption = !empty($request->isExemption) ? $request->isExemption : 0;
                        $supplier->save();
                    }
                    if($request->gl_type == 'Bank'){
                        $bank = new BankCash();
                        $bank->account_id = $account->id;
                        $bank->name = $request->bank_name;
                        $bank->address = $request->bank_address;
                        $bank->account_title = $request->account_title;
                        $bank->account_number = $request->account_number;
                        $bank->location = $request->location;
                        $bank->save();
                    }
                    if(isset($request->isExemption) && count($request->exemption_certificates) > 0){
                        foreach ($request->exemption_certificates as $key => $exemption_certificate) {
                            $account_detail = new AccountDetail();
                            if(isset($exemption_certificate['image_file'])){
                                $exemption_image = InsertFile($exemption_certificate['image_file'],'/exemption_certificates');
                                $account_detail->certificate_image = $exemption_image;
                            }
                            if($request->gl_type == 'Customer'){
                                $account_detail->customer_id = $customer->id;
                            }
                            if($request->gl_type == 'Supplier'){
                                $account_detail->supplier_id = $supplier->id;
                            }
                            $account_detail->account_id = $account->id;
                            $account_detail->exemption_certificate_number = $exemption_certificate['exemption_certificate_number'];
                            $account_detail->from_date = $exemption_certificate['from_date'];
                            $account_detail->expire_date = $exemption_certificate['expire_date'];
                            $account_detail->save();
                        }
                    }
                // });
            });
            return ['code'=>'200','message' => 'success'];
        }
        catch(\Exception | ValidationException $e){
            if($e instanceof ValidationException){
                return ['code'=>'422','errors' => $e->errors()];
            }
            else{
                return ['code'=>'500','error_message'=>$e->getMessage()];
            }
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $dropdown = [
            'Expenses' => 'Expenses',
            'Cost_of_sales' => 'Cost of Sales',
            'Customer' => 'Customer',
            'Supplier' => 'Supplier',
            'Bank' => 'Bank',
            'Assets' => 'Assets',
            'Liability' => 'Liability',
            'Income/sales' => 'Income/sales',
            'Capital' => 'Capital'
        ];
        $account = Account::find($id);
        $customer = Customer::where('account_id',$id)->first();
        $supplier = Supplier::where('account_id',$id)->first();
        $bank_cash = BankCash::where('account_id',$id)->first();
        $exemption_certificates = AccountDetail::where('account_id', $id)->get();
        $levels = Level2::get();
        $account_class = AccountClass::get();
        return view('admin.AccountCode.edit',get_defined_vars());
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        try{
            DB::transaction(function () use ($request,$id) {
                $this->validate($request , [
                    'level2' => 'required',
                    'code' => 'required',
                    'description' => 'required',
                ]);
                // LogBatch::withinBatch(function(string $uuid) use ($request,$id) {
                    $level2 = Level2::where('id','=',$request->level2)->first();
                    $code = $level2->code.'-'.$request->code;
                    $checkAccount = Account::where('id', '!=', $id)->where('code', $code)->first();
                    if(!empty($checkAccount)){
                        throw new \Exception('Account Code Already exist.', 409);
                    }
                    $account = Account::find($id);
                    $account->code = $code;
                    $account->level_2_id = $level2->id;
                    $account->account_code = $request->code;
                    $account->description = $request->description;
                    $account->level2_code = $level2->code;
                    $account->tag = isset($request->tag) ? $request->tag : 0;
                    if($request->gl_type == 'Customer'){
                        $account->name = $request->customer_name;
                        $account->short_name = $request->customer_short_name;
                        $account->ntn = $request->customer_ntn;
                        $account->stn = $request->customer_stn;
                        $account->email = $request->customer_email;
                        $account->contact_no = $request->customer_contact_no;
                        $account->address = $request->customer_address;
                    }
                    if($request->gl_type == 'Supplier'){
                        $account->name = $request->suplier_name;
                        $account->short_name = $request->supplier_short_name;
                        $account->address = $request->supplier_address;
                        $account->ntn = $request->supplier_ntn;
                        $account->stn = $request->supplier_stn;
                        $account->email = $request->supplier_email;
                        $account->contact_no = $request->supplier_contact_no;
                    }
                    if($request->gl_type == 'Bank'){
                        $account->name = $request->bank_name;
                        $account->address = $request->bank_address;
                        $account->account_title = $request->account_title;
                        $account->account_number = $request->account_number;
                        $account->location = $request->location;
                    }
                    $account->exemption_certificate = !empty($request->isExemption) ? $request->isExemption : 0;
                    $account->update();
                    if($request->gl_type == 'Customer'){
                        $customer = Customer::find($request->customer_id);
                        $customer->name = $request->customer_name;
                        $customer->short_name = $request->customer_short_name;
                        $customer->ntn = $request->customer_ntn;
                        $customer->stn = $request->customer_stn;
                        $customer->email = $request->customer_email;
                        $customer->contact_no = $request->customer_contact_no;
                        $customer->address = $request->customer_address;
                        $customer->is_exemption = !empty($request->isExemption) ? $request->isExemption : 0;
                        $customer->update();
                    }
                    if($request->gl_type == 'Supplier'){
                        $supplier = Supplier::find($request->supplier_id);
                        $supplier->name = $request->suplier_name;
                        $supplier->short_name = $request->supplier_short_name;
                        $supplier->address = $request->supplier_address;
                        $supplier->ntn = $request->supplier_ntn;
                        $supplier->stn = $request->supplier_stn;
                        $supplier->email = $request->supplier_email;
                        $supplier->contact_no = $request->supplier_contact_no;
                        $supplier->is_exemption = !empty($request->isExemption) ? $request->isExemption : 0;
                        $supplier->update();
                    }
                    if($request->gl_type == 'Bank'){
                        if(isset($request->bankCash_id) && !empty($request->bankCash_id)){
                            $bank = BankCash::find($request->bankCash_id);
                        }
                        else{
                            $bank = new BankCash();
                        }
                        $bank->account_id = $account->id;
                        $bank->name = $request->bank_name;
                        $bank->address = $request->bank_address;
                        $bank->account_title = $request->account_title;
                        $bank->account_number = $request->account_number;
                        $bank->location = $request->location;
                        $bank->save();
                    }
                    if(isset($request->isExemption)){
                        if(count($request->exemption_certificates) > 0){
                            $certificates_ids = [];
                            foreach ($request->exemption_certificates as $key => $exemption_certificate) {
                                if(!empty($exemption_certificate['id'])){
                                    $certificates_ids[] = $exemption_certificate['id'];
                                }
                                $account_detail = AccountDetail::find($exemption_certificate['id']);
                                if(!empty($account_detail)){
                                    if(isset($exemption_certificate['image_file'])){
                                        if(!empty($account_detail->certificate_image)){
                                            unlink(public_path().$account_detail->certificate_image);
                                        }
                                        $exemption_image = InsertFile($exemption_certificate['image_file'],'/exemption_certificates');
                                        $account_detail->certificate_image = $exemption_image;
                                    }
                                    $account_detail->exemption_certificate_number = $exemption_certificate['exemption_certificate_number'];
                                    $account_detail->from_date = $exemption_certificate['from_date'];
                                    $account_detail->expire_date = $exemption_certificate['expire_date'];
                                    $account_detail->update();
                                }
                                else{
                                    $accountDetail = new AccountDetail();
                                    if(isset($exemption_certificate['image_file'])){
                                        $exemption_image = InsertFile($exemption_certificate['image_file'],'/exemption_certificates');
                                        $accountDetail->certificate_image = $exemption_image;
                                    }
                                    if($request->gl_type == 'Customer'){
                                        $accountDetail->customer_id = $customer->id;
                                    }
                                    if($request->gl_type == 'Supplier'){
                                        $accountDetail->supplier_id = $supplier->id;
                                    }
                                    $accountDetail->account_id = $account->id;
                                    $accountDetail->exemption_certificate_number = $exemption_certificate['exemption_certificate_number'];
                                    $accountDetail->from_date = $exemption_certificate['from_date'];
                                    $accountDetail->expire_date = $exemption_certificate['expire_date'];
                                    $accountDetail->save();
                                    $certificates_ids[] = $accountDetail->id;
                                }
                            }
                            $delete_certificates = AccountDetail::where('account_id', $id)->whereNotIn('id', $certificates_ids)->get(['id', 'certificate_image']);
                            if(count($delete_certificates) > 0){
                                foreach ($delete_certificates as $key => $Deletecertificate) {
                                    if(!empty($Deletecertificate->certificate_image)){
                                        unlink(public_path().$Deletecertificate->certificate_image);
                                    }
                                    $Deletecertificate->delete();
                                }
                            }
                        }
                    }
                    else{
                        $certificates = AccountDetail::where('account_id', $id)->get();
                        if(count($certificates) > 0){
                            foreach ($certificates as $key => $certificate) {
                                if(!empty($certificate->certificate_image)){
                                    unlink(public_path().$certificate->certificate_image);
                                }
                                $certificate->delete();
                            }
                        }
                    }
                // });
            });
            return ['code'=>'200','message' => 'success'];
        }
        catch(\Exception | ValidationException $e){
            if($e instanceof ValidationException){
                return ['code'=>'422','errors' => $e->errors()];
            }
            else{
                return ['code'=>'500','error_message'=>$e->getMessage()];
            }
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function getAccounts(Request $request)
    {
        if ($request->search == '') {
            $accounts = Account::whereIn('level_2_id',loginUserAccountCodes())->take(10)->get(['id','code','description','gl_type']);
        }
        else {
            $accounts = Account::whereIn('level_2_id',loginUserAccountCodes())->where('code', 'LIKE', "%{$request->search}%") 
            ->orWhere('description', 'LIKE', "%{$request->search}%") ->get(['id','code','description','gl_type']);
        }
        $data = array();
        foreach ($accounts as $account) {
            $data[] = array(
            "id" => $request->trial == 'trialBalance' ? $account->code : $account->id,
            "text" => $account->code.' - '.$account->description,
            );
        }
        return response()->json($data);
    }
}
