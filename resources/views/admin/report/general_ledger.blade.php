@extends('admin.layouts.master')
@section('title', 'General Ledger ')
@section('style')
    <style>
        .top_btn {
            border-right: 1px solid #000;
            border-bottom: 1px solid #000;
            border-top: 1px solid #000;
            border-left: 1px solid #000;
            box-shadow: 3px 5px;
            text-align: center;
            padding-top: 5px;

        }

        .col3 {
            width: 25%;
        }

        .col4 {
            width: 30%;
        }

        .col2 {
            width: 20%;
        }

        .col8 {
            width: 65%;
        }

        .para {
            padding-top: 10px;
        }

        .dataTable {
            border-right: 1px solid #000;
            border-bottom: 2px solid #000;
            border-top: 1px solid #000;
            border-left: 1px solid #000;
            box-shadow: 3px 5px;
            text-align: center;
            /* padding-top: 8px; */
        }
        tr td{
                        font-size:10px !important;
                    }
    </style>
@endsection
@section('content')
    <section class="content-wrapper container-xxl p-0">
        <div class="content-header row">
            <div class="content-header-left col-md-9 col-12 mb-2">
                <div class="row breadcrumbs-top">
                    <div class="col-12">
                        <h2 class="content-header-title float-start mb-0">General Ledger</h2>
                        <div class="breadcrumb-wrapper">
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="{{ url('/') }}">Dashboard</a>
                                </li>
                                <li class="breadcrumb-item"><a href="#">Reports</a>
                                </li>
                                <li class="breadcrumb-item active">General Ledger
                                </li>
                            </ol>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="content-body">
            <div class="row">
                <div class="col-12">
                    <div class="card mb-1">
                        <div class="card-body">
                            <form id="search_form" action="{{ url('generalLedger') }}" method="GET">
                                <div class="row">
                                    {{-- <div class="col-4">
                                        <label class="form-label" for="level1">Select Level 1</label>
                                        <select name="level1" id="level1" class="select2 form-select"
                                            data-placeholder="Select Level 1" required>
                                            <option value="">Select</option>
                                            @foreach ($level1 as $key => $level1)
                                                <option value="{{ $level1->id }}"
                                                    {{ $level1->id == request('level1') ? 'selected' : '' }}>
                                                    {{ $level1->code . ' ' . $level1->description }}</option>
                                            @endforeach

                                        </select>
                                    </div>
                                    <div class="col-4">
                                        <label class="form-label" for="level2">Select Level 2</label>
                                        <select name="level2" id="level2" class="select2 form-select"
                                            data-placeholder="Select Level 2">
                                            <option value="">Select</option>
                                            @if (count($level2) > 0)
                                                @foreach ($level2 as $key => $data)
                                                    <option value="{{ $data->id }}"
                                                        {{ $data->id == request('level2') ? 'selected' : '' }}>
                                                        {{ $data->code . ' ' . $data->description }}</option>
                                                @endforeach
                                            @endif
                                        </select>
                                    </div>
                                    <div class="col-4">
                                        <div class="mb-1">
                                            <label class="form-label" for="accounts">Account</label>
                                            <select name="account" id="accounts" class="select2 form-select"
                                                data-placeholder="Select Accounts">
                                                <option value="">Select</option>
                                                @if (count($accounts) > 0)
                                                    @foreach ($accounts as $key => $account)
                                                        <option value="{{ $account->id }}"
                                                            {{ $account->id == request('account') ? 'selected' : '' }}>
                                                            {{ $account->code . ' ' . $account->description }}</option>
                                                    @endforeach
                                                @endif
                                            </select>
                                        </div>
                                    </div> --}}
                                    <div class="col-md-3">
                                        <label class="form-label" for="fromRange">From Range</label>
                                        <select name="fromRange" id="fromRange" class="select2 form-select accounts"
                                            data-placeholder="Select From range" data-flag="trialBalance">
                                            <option value=""></option>
                                        </select>
                                    </div>
                                    <div class="col-md-3">
                                        <label class="form-label" for="toRange">To Range</label>
                                        <select name="toRange" id="toRange" class="select2 form-select accounts"
                                            data-placeholder="Select ToRange">
                                            <option value=""></option>
                                        </select>
                                    </div>
                                    <div class="col-md-3">
                                        <label class="form-label" for="date">From</label>
                                        <input type="date" id="date" class="form-control" name="fromDate"
                                            value="{{!empty(request('fromDate')) ? request('fromDate') : date('Y-m-d') }}" required />
                                    </div>
                                    <div class="col-md-3">
                                        <label class="form-label" for="date">To</label>
                                        <input type="date" id="date" class="form-control" name="toDate"
                                            value="{{!empty(request('toDate')) ? request('toDate') : date('Y-m-d') }}" required />
                                    </div>
                                    <div class="col-2 m-2">
                                        <div class="form-check">
                                            <input class="form-check-input" name="Cheque" id="Cheque" type="checkbox"
                                                tabindex="3" {{ request('Cheque') == 'on' ? 'checked' : '' }}/>
                                            <label class="form-check-label" for="Cheque"> With Cheque</label>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-6 col-12">
                                        <button type="button" onclick="divToPrint()"
                                            class="btn btn-primary mt-1">Print</button>
                                    </div>
                                    <div class="col-md-6 col-12 text-end">
                                        <a href="{{ url('generalLedger') }}" type="button"
                                            class="btn btn-danger mt-1">Reset</a>
                                        <button type="submit" class="btn btn-primary mt-1">Apply</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                    <div class="mb-1">
                        <div class="dt-action-buttons text-end">
                            <div class="dt-buttons d-inline-flex">
                                <a href="{{ url('generalLedger') }}?type=excel&fromRange={{ request('fromRange') }}&toRange={{ request('toRange') }}&fromDate={{ request('fromDate') }}&toDate={{ request('toDate') }}&level1={{ request('level1') }}&level2={{ request('level2') }}&account={{ request('account') }}"
                                    class="btn btn-success">Download Excel</a>
                                <a href="{{ url('generalLedger') }}?type=pdf&fromRange={{ request('fromRange') }}&toRange={{ request('toRange') }}&fromDate={{ request('fromDate') }}&toDate={{ request('toDate') }}&level1={{ request('level1') }}&level2={{ request('level2') }}&account={{ request('account') }}"
                                    class="btn btn-danger ms-1">Download Pdf</a>
                            </div>
                        </div>
                    </div>
                </div>
                <div id="div_to_print">
                    @if(isset($levels) && count($levels) > 0)
                        @foreach ($levels as $level1)
                            @if (count($level1->level2) > 0)
                                @foreach ($level1->level2 as $level2)
                                    @if (count($level2->accounts) > 0)
                                        @foreach ($level2->accounts as $data)
                                        @php
                                        // dd($data->accountdetails[0]->from_date);
                                            $openingBalance = openingBalance($data->id,request('fromDate'));
                                        @endphp
                                        <div class="p-1">
                                            <div class="row" id="test">
                                                {{-- <div style="font-size: 10px" class = "col-12">
                                                    <p>{{ date('d-m-Y  h:i:sa') }}</p>
                                                </div> --}}
                                                <div class="col-12">
                                                    <h4 style="text-align: center;font-weight: bold;text-decoration-line: underline;text-transform: uppercase;">{{ env("COMPANY") }}</h4>
                                                </div>
                                                <div class="col-12">
                                                    <h6 style="text-align: center; font-size:10px">Ledger Entries From {{ date('d-m-Y',strtotime(request('fromDate'))) }} To {{ date('d-m-Y',strtotime(request('toDate'))) }}</h6>
                                                </div>
                                                <div class="col-12">
                                                    <p style="white-space: nowrap;font-size:10px;"><b>Control A/c : {{ $data->code }} {{ $data->description }}</b></p>
                                                </div>
                                            </div>
                                            <table class="table" id="dataTable">
                                                <thead class="dataTable">
                                                    <tr>
                                                        <th colspan="7" style="font-size: 10px;text-align:right">
                                                            {{ date('d-m-Y  h:i:sa') }}
                                                        </th>
                                                    </tr>
                                                    <tr>
                                                        <th colspan="7" style="text-align: center;text-decoration-line: underline;text-transform: uppercase;font-size: 15px !important">{{ env("COMPANY") }}</th>
                                                    </tr>
                                                    <tr>
                                                        <th colspan="7" style="text-align: center;font-size: 12px !important">Ledger Entries From {{ date('d-m-Y',strtotime(request('fromDate'))) }} To {{ date('d-m-Y',strtotime(request('toDate'))) }}</th>
                                                    </tr>
                                                    <tr>
                                                        <th colspan="2" class="" style="text-align: left;font-size: 12px !important"><b>Control A/c : {{ $data->code }}</b></th>
                                                        <th colspan="2" class="" style="text-align: center;font-size: 12px !important"><b>{{ $data->description }}</b></th>
                                                        <th colspan="2"></th>
                                                    </tr>
                                                    @if (!empty($data->accountdetails[0]->from_date))
                                                    <tr>
                                                        <th colspan="7" style="text-align: center;font-size: 12px !important"><b>Exemption Certificate From : {{$data->accountdetails[0]->from_date }} To {{ $data->accountdetails[0]->expire_date }}</b></th>
                                                    </tr>
                                                    @endif
                                                    <tr class="left-row">
                                                        <th>Date</th>
                                                        <th>V.Type/No.</th>
                                                        <th>Description</th>
                                                        <th style="text-align: right !important">Debit<br><span style="font-size: 9px">Rupees</span></th>
                                                        <th style="text-align: right !important">Credit<br><span style="font-size: 9px">Rupees</span></th>
                                                        <th style="text-align: right !important">Balance<br><span style="font-size: 9px">Rupees</span></th>
                                                        @if(request('Cheque') == 'on')
                                                        <th style="text-align: right !important">Cheque #</th>
                                                        @endif
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <tr>
                                                        <td colspan="2"></td>
                                                        <td style="white-space: nowrap;"> <b>Opening Balance:</b> </td>
                                                        <td colspan="2"></td>
                                                        @if ($openingBalance > 0)
                                                        <td style="text-align: right;white-space: nowrap;"> <b> {{ number_format($openingBalance, 2) }} DR</b>
                                                        </td>
                                                        @else
                                                        <td style="text-align: right;white-space: nowrap;"> <b> {{ number_format(abs($openingBalance), 2) }} CR</b></td>
                                                        @endif
                                                        @if(request('Cheque') == 'on')
                                                        <td></td>
                                                        @endif
                                                        @php
                                                            $blance = 0;
                                                            $dbtotel = 0;
                                                            $crtotel = 0;
                                                            $closingTotalbalance = 0;
                                                            $blance = $blance + $openingBalance;
                                                        @endphp
                                                    </tr>
                                                    @if (count($data->voucherdetails) > 0)
                                                    @foreach ($data->voucherdetails as $detail)
                                                        <tr>
                                                            <td  style="white-space: nowrap;">
                                                                {{ date('j M y', strtotime($detail->date)) }}</td>
                                                            <td style="white-space: nowrap;">{{ $detail->vouchertype->voucher_type . '-' . $detail->voucher_no }} - {{ $detail->location->short_name }}</td>
                                                            <td>{{$detail->line_remarks}} - {{ !empty($detail->voucher->igp_number) ? $detail->voucher->igp_number : $detail->voucher->ogp_number }}</td>
                                                            @if ($detail->amount >= 0)
                                                                <?php $dbtotel = $detail->amount + $dbtotel; ?>
                                                                <td style="text-align: right">{{ number_format(abs($detail->amount), 2) }}</td>
                                                                <td style="text-align: right">-</td>
                                                            @else
                                                                <td style="text-align: right">-</td>
                                                                <?php $crtotel = $detail->amount + $crtotel; ?>
                                                                <td style="text-align: right">{{ number_format(abs($detail->amount), 2) }}</td>
                                                            @endif
                                                            <?php
                                                            $blance = $blance + $detail->amount;
                                                            ?>
                                                            @if ($blance < 0)
                                                                <td style="text-align: right;white-space: nowrap;">{{ number_format(abs($blance), 2)  }} CR</td>
                                                            @else
                                                                <td style="text-align: right;white-space: nowrap;">{{ number_format(abs($blance), 2) }} DR</td>
                                                            @endif
                                                            @if(request('Cheque') == 'on')
                                                            <td style="text-align: right !important">{{ !empty($detail->voucher->cheque_no) ? $detail->voucher->cheque_no : '-'  }}</td>
                                                            @endif
                                                        </tr>
                                                    @endforeach
                                                    @endif
                                                    <tr style="border-top:1px solid #000">
                                                        <td></td>
                                                        <td></td>
                                                        <td style="white-space: nowrap;"><b> Closing Balance:</b></td>
                                                        <td style="text-align: right;white-space: nowrap;"><b>{{ number_format(abs($dbtotel), 2) }}</b> </td>
                                                        <td style="text-align: right;white-space: nowrap;"><b>{{ number_format(abs($crtotel), 2) }}</b> </td>
                                                        @if ($blance < 0)
                                                            <td style="text-align: right"><b>{{ number_format(abs($blance), 2) }} CR</b></td>
                                                        @else
                                                            <td style="text-align: right"><b>{{ number_format(abs($blance), 2) }} DR</b></td>
                                                        @endif
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                        @endforeach
                                    @endif
                                @endforeach
                            @endif
                        @endforeach
                    @endif
                </div>
            </div>
        </div>
    </section>
@endsection
@section('scripts')
<script src="{{ asset('assets/js/scripts.js') }}"></script>
    <script>
        function divToPrint() {
            var divToPrint = document.getElementById("div_to_print");
            const WinPrint = window.open("");
            WinPrint.document.open();
            WinPrint.document.write('<html><head><meta name="viewport" content="width=device-width,initial-scale=1.0,user-scalable=0,minimal-ui"><link rel="stylesheet" type="text/css" href="{{ asset("app-assets/css/bootstrap.css") }}"><link rel="stylesheet" type="text/css" href="{{ asset("app-assets/css/bootstrap-extended.css") }}"><style>table {width: 100%;}#dataTable th, #dataTable td {padding: 0.3rem !important;}#test{display:none}table {border-collapse: collapse;}table { page-break-inside:auto; }tr{ page-break-inside:avoid; page-break-after:auto; }.top_btn {border-right: 1px solid #000;border-bottom: 1px solid #000;border-top: 1px solid #000;border-left: 1px solid #000;box-shadow: 3px 5px;text-align: center;padding-top: 5px;}.para {padding-top: 10px;}.dataTable {border: none;box-shadow: 0px 0px;text-align: center;}.dataTable thead,.dataTable tbody{border:none !important}tr td, th{ font-size:12px !important; border:none !important;white-space: nowrap !important;} .left-row th {text-align:left !important; text-transform: capitalize;} @page {margin-top: 30px;} body {padding-top: 10px;}</style><body   onload="window.print()">'+divToPrint.innerHTML+'</body></html>');
            WinPrint.document.close();
            WinPrint.focus();
            WinPrint.print();
            WinPrint.close();
        }
        
        $(document).ready(function() {
            getAccounts('trialBalance');
            // $('body').removeClass('menu-expanded').addClass('menu-collapsed');
            mask = $('.range');
            mask.each(function() {
                new Cleave($(this), {
                    delimiter: '-',
                    blocks: [2, 2, 4]
                });
            });
            $("#level1").change(function() {
                var level_1_id = $(this).val();
                $.ajax({
                    type: "GET",
                    url: 'get_levels/' + level_1_id,
                    data: {
                        id: level_1_id
                    },
                    success: function(response) {
                        $('#level2').html('<option value="">Select Level 2</option>');
                        $.each(response, function(index, value) {
                            $('#level2').append(
                                $('<option></option>').val(value.id).html(value
                                    .code + " " + value.description)
                            );
                        });
                    },
                });
            });

            $("#level2").change(function() {
                var level_2_id = $(this).val();
                $.ajax({
                    type: "GET",
                    url: 'get_accounts/' + level_2_id,
                    data: {
                        id: level_2_id
                    },
                    success: function(response) {
                        $('#accounts').html('<option value="">Select Level 3</option>');
                        $.each(response, function(index, value) {
                            $('#accounts').append(
                                $('<option></option>').val(value.id).html(value
                                    .code + " " + value.description)
                            );
                        });
                    },
                });
            });


        })
    </script>
@endsection
