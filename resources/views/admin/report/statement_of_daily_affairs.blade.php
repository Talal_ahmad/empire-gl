@extends('admin.layouts.master')
@section('title', 'Statement Of Daily Affairs')
@section('style')
    <style>
        table {
            font-family: arial, sans-serif;
            border-collapse: collapse;
            width: 100%;
        }

        tr,
        td,
        th {
            border: 1px solid black;
            text-align: left;
            padding: 8px;
        }

        tr:nth-child(even) {
            /* background-color: #545050; */
        }
    </style>
@endsection
@section('content')
    <section class="content-wrapper container-xxl p-0">
        <div class="content-header row">
            <div class="content-header-left col-md-9 col-12 mb-2">
                <div class="row breadcrumbs-top">
                    <div class="col-12">
                        <h2 class="content-header-title float-start mb-0">Statement Of Daily Affairs</h2>
                        <div class="breadcrumb-wrapper">
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="{{ url('/') }}">Dashboard</a>
                                </li>
                                <li class="breadcrumb-item"><a href="#">Reports</a>
                                </li>
                                <li class="breadcrumb-item active">Statement Of Daily Affairs
                                </li>
                            </ol>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="content-body">
            <div class="row">
                <div class="col-12">
                    <div class="card mb-1">
                        <div class="card-body">
                            <form id="search_form" action="">
                                <div class="row">
                                    <div class="col-md-3">
                                        <label class="form-label" for="date">From</label>
                                        <input type="date" id="date" class="form-control" name="fromDate"
                                            value="{{ request('fromDate') }}" />
                                    </div>
                                    <div class="col-md-3">
                                        <label class="form-label" for="date">To</label>
                                        <input type="date" id="date" class="form-control" name="toDate"
                                            value="{{ !empty(request('toDate')) ? request('toDate') : date('Y-m-d') }}" />
                                    </div>
                                    <div class="col-md-4 col-12">
                                        <label class="form-label" for="role">Select Voucher</label>
                                        <select name="voucher_type" class="form-select select2"
                                            data-placeholder="Select Voucher Type">
                                            <option value=""></option>
                                            @foreach ($VoucherTypes as $type)
                                                <option value="{{ $type->id }}"
                                                    {{ $type->id == request('voucher_type') ? 'selected' : '' }}>
                                                    {{ $type->voucher_type }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12 col-12 text-end">
                                        <a href="{{ url('statement_of_daily_affairs') }}" type="button"
                                            class="btn btn-danger mt-1">Reset</a>
                                        <button type="submit" class="btn btn-primary mt-1">Apply</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                    @if ($voucher_details)
                        <div id="div_to_print">
                            <div class="card p-2">
                                <div class="row pb-2">
                                    <div class="col-12">
                                        <h2 class="text-center fw-bolder text-uppercase">UE - GL</h2>
                                        <p class="text-center fw-bolder text-uppercase">STATEMENT OF DAILY AFFAIRS </p>
                                        <p class="text-center fw-bolder text-uppercase">From
                                            {{ request('fromDate') ? request('fromDate') : '-' }} To
                                            {{ request('toDate') ? request('toDate') : '-' }} </p>
                                    </div>
                                </div>
                                <table class="table table-responsive table-striped" id="dataTable">
                                    <thead>
                                        <tr>
                                            <th>Account Title</th>
                                            <th>Voucher No</th>
                                            <th>Dated</th>
                                            <th>Narration</th>
                                            <th>Debit</th>
                                            <th>Credit</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach ($voucher_details as $voucher_detail)
                                            <tr>
                                                <td>{{ $voucher_detail->description }}</td>
                                                <td>{{ $voucher_detail->voucher_types_voucher . '-' . $voucher_detail->voucher_type }}
                                                </td>
                                                <td class="text-satrt">{{ $voucher_detail->date }}</td>
                                                <td>{{ $voucher_detail->line_remarks }}</td>
                                                <td class=" text-center ">
                                                    {{ $voucher_detail->amount > 0 ? abs($voucher_detail->amount) : '-' }}
                                                </td>
                                                <td class=" text-center ">
                                                    {{ $voucher_detail->amount < 0 ? abs($voucher_detail->amount) : '-' }}
                                                </td>
                                            </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    @else
                        <div class="card p-5 text-center fs-3 fw-bold ">Please Select A Voucher Type or Date Range</div>
                    @endif
                </div>
            </div>
        </div>
        </div>
    </section>
@endsection
@section('scripts')
    <script>
        $(document).ready(function() {
            $('#dataTable').DataTable({
                processing: true,
                responsive: true,
                ordering: true,
                "columnDefs": [{
                        className: 'control',
                        searchable: false,
                        orderable: false
                    },
                    {
                        "defaultContent": "-",
                        "targets": "_all"
                    }
                ],
                "order": [
                    [0, 'asc']
                ],
                dom: '<"card-header border-bottom p-1"<"head-label"><"dt-action-buttons text-end"B>><"d-flex justify-content-between align-items-center mx-0 row"<"col-sm-12 col-md-6"l><"col-sm-12 col-md-6"f>>t<"d-flex justify-content-between mx-0 row"<"col-sm-12 col-md-6"i><"col-sm-12 col-md-6"p>>',
                displayLength: 10,
                lengthMenu: [10, 25, 50, 75, 100],
                buttons: [{
                    extend: 'collection',
                    className: 'btn btn-outline-secondary dropdown-toggle app me-2',
                    text: feather.icons['share'].toSvg({
                        class: 'font-small-4 me-50'
                    }) + 'Export',
                    buttons: [{
                            extend: 'print',
                            text: feather.icons['printer'].toSvg({
                                class: 'font-small-4 me-50'
                            }) + 'Print',
                            className: 'dropdown-item',
                            exportOptions: {
                                columns: ':not(.not_include)'
                            }
                        },
                        {
                            extend: 'csv',
                            text: feather.icons['file-text'].toSvg({
                                class: 'font-small-4 me-50'
                            }) + 'Csv',
                            className: 'dropdown-item',
                            exportOptions: {
                                columns: ':not(.not_include)'
                            }
                        },
                        {
                            extend: 'excel',
                            text: feather.icons['file'].toSvg({
                                class: 'font-small-4 me-50'
                            }) + 'Excel',
                            className: 'dropdown-item',
                            exportOptions: {
                                columns: ':not(.not_include)'
                            }
                        },
                        {
                            extend: 'pdf',
                            text: feather.icons['clipboard'].toSvg({
                                class: 'font-small-4 me-50'
                            }) + 'Pdf',
                            className: 'dropdown-item',
                            exportOptions: {
                                columns: ':not(.not_include)'
                            }
                        },
                        {
                            extend: 'copy',
                            text: feather.icons['copy'].toSvg({
                                class: 'font-small-4 me-50'
                            }) + 'Copy',
                            className: 'dropdown-item',
                            exportOptions: {
                                columns: ':not(.not_include)'
                            }
                        }
                    ],
                    init: function(api, node, config) {
                        $(node).removeClass('btn-secondary');
                        $(node).parent().removeClass('btn-group');
                        setTimeout(function() {
                            $(node).closest('.dt-buttons').removeClass('btn-group')
                                .addClass('d-inline-flex');
                        }, 50);
                    }
                }],
                responsive: {
                    details: {
                        display: $.fn.dataTable.Responsive.display.childRowImmediate,
                        type: 'column',
                    }
                },
                language: {
                    paginate: {
                        previous: '&nbsp;',
                        next: '&nbsp;'
                    }
                }
            });

            $('div.head-label').html('<h3 class="mb-0">Statements of Daily Affairs Report</h3>');
            $('.app').attr('id', 'app-export');

        });
    </script>
@endsection
