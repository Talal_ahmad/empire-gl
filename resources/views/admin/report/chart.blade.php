@extends('admin.layouts.master')
@section('title', 'Chart Of Accounts')
@section('style')
<style>
  table {
  font-family: arial, sans-serif;
  border-collapse: collapse;
  width: 100%;
}

tr, td, th {
  border: 1px solid black;
  text-align: left;
  padding: 8px;
}

tr:nth-child(even) {
  /* background-color: #545050; */
}
</style>
@endsection
@section('content')
    <section class="content-wrapper container-xxl p-0">
        <div class="content-header row">
            <div class="content-header-left col-md-9 col-12 mb-2">
                <div class="row breadcrumbs-top">
                    <div class="col-12">
                        <h2 class="content-header-title float-start mb-0">Chart Of Accounts</h2>
                        <div class="breadcrumb-wrapper">
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="{{ url('/') }}">Dashboard</a>
                                </li>
                                <li class="breadcrumb-item"><a href="#">Reports</a>
                                </li>
                                <li class="breadcrumb-item active">Chart Of Accounts
                                </li>
                            </ol>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="content-body">
            <div class="row">
                <div class="col-12">
                    <div class="card mb-1">
                        <div class="card-body">
                            <form id="search_form" action="">
                                <div class="row">
                                    {{-- <div class="col-md-3 col-12">
                                        <label class="form-label" id="calculation_group"> Group:</label>
                                        <select name="calculation_group" id="calculation_group" class="select2 form-select" data-placeholder="Select  Group" required>
                                            <option value=""></option>
                                           
                                        </select>
                                    </div> --}}
                                    <div class="col-md-4 col-12">
                                        <label class="form-label" for="role">Select Level 1</label>
                                        <select name="level1" id="level1" class="select2 form-select" data-placeholder="Select Level">
                                            <option value=""></option>
                                            @foreach ($level_1 as $level)
                                                <option value="{{$level->id}}" {{ $level->id == request('level1') ? 'selected' : '' }}>{{$level->code}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="col-md-4 col-12">
                                        <label class="form-label" for="role">Select Level 2</label>
                                        <select name="level2" id="level2" class="select2 form-select" data-placeholder="Select Level 2">
                                            <option value=""></option>
                                            @foreach ($level_2 as $level)
                                                <option value="{{$level->id}}" {{ $level->id == request('level2') ? 'selected' : '' }}>{{$level->code}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="col-md-4 col-12">
                                        <label class="form-label" for="role">Select Account</label>
                                        <select name="account" id="account" class="select2 form-select" data-placeholder="Select Account">
                                            <option value=""></option>
                                            @foreach ($account as $level)
                                                <option value="{{$level->id}}" {{ $level->id == request('account') ? 'selected' : '' }}>{{$level->code}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-6 col-12">
                                        <button type="button" onclick="divToPrint()" class="btn btn-primary mt-1">Print</button>
                                    </div>
                                    <div class="col-md-6 col-12 text-end">
                                        <a href="{{ url('Chart_report') }}" type="button" class="btn btn-danger mt-1">Reset</a>
                                        <button type="submit" class="btn btn-primary mt-1">Apply</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                    <div class="mb-1">
                        <div class="dt-action-buttons text-end">
                            <div class="dt-buttons d-inline-flex">
                                <a href="{{ url('Chart_report') }}?type=excel&level1={{request('level1')}}&level2={{request('level2')}}&account={{request('account')}}" class="btn btn-success">Download Excel</a>
                                <a href="{{ url('Chart_report') }}?type=pdf&level1={{request('level1')}}&level2={{request('level2')}}&account={{request('account')}}" class="btn btn-danger ms-1">Download Pdf</a>
                            </div>
                        </div>
                    </div>
                    <div id="div_to_print">
                        <div class="card p-2">
                            <div class="row pb-2">
                                <div class="col-12">
                                    <h2 class="text-center fw-bolder text-decoration-underline text-uppercase">{{env('COMPANY')}}</h2>
                                </div> 
                            </div>
                                <table>
                                    <thead>
                                        <tr>
                                            <th>Account Name</th>
                                            <th>Account Code</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach ($levels as $level)
                                            <tr>
                                                <th>{{ $level->code }}</th>
                                                <th>{{ $level->description }}</th>
                                            </tr>
                                            @foreach($level->level2 as $level2)
                                                <tr>
                                                    <th>{{ $level2->code }}</th>
                                                    <th>{{ $level2->description }}</th>
                                                </tr>
                                                @foreach($level2->accounts as $account)
                                                <tr>
                                                    <td>{{ $account->code }}</td>
                                                    <td>{{ $account->description }}</td>
                                                </tr>
                                                @endforeach
                                            @endforeach
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
@section('scripts')
    <script>
        // $(document).on('click', '.export', function(){
        //     blockUI();
        //     var export_type = $(this).attr('id');
        //     // var calculation_group = $('#calculation_group').select2("val");
        //     // var bank_status = $('#bank_status').select2("val");
        //     // var banksFilter = $('#banksFilter').select2("val");
        //     // var employment_status = $('#employment_status').val();
        //     var date = $('#date').val();
        //     $.ajax({
        //         url: "{{url('')}}",
        //         type: "GET",
        //         data: {
        //             export_type:export_type,
        //             calculation_group:calculation_group,
        //             banksFilter:banksFilter,
        //             date:date,
        //             employment_status:employment_status,
        //             bank_status:bank_status
        //         }, 
        //         success: function(response,status,xhr) {
        //             $.unblockUI();
        //             // if(export_type == 'excel')
        //             // {
        //                 window.open(this.url);
        //             // }
        //         },
        //         error: function() {
        //             $.unblockUI();
        //             alert('Error occured');
        //         }
        //     });
        // });
        
        $(document).ready(function(){
            $("#level1").change(function() {
                var optVal = $(this).val();
                if (optVal.length > 0) {
                    $.ajax({
                        type: "GET",
                        url: "{{ url('get_levels') }}"+'/'+optVal,
                        success: function(response) {
                            $('#level2').empty();
                            $('#level2').html('<option value="">Select Level 2</option>'); 
                            $.each(response, function(index, value) {
                                // console.log(response)
                                $('#level2').append(
                                    $('<option></option>').val(value.id).html(
                                        value.code )
                                );
                            });
                        },
                        error: function() {
                            alert('Error occured');
                        }
                    });
                } else {
                    $('#level2').empty();
                }
            });
        });
        $(document).ready(function(){
            $("#level2").change(function() {
                var optVal = $(this).val();
                if (optVal.length > 0) {
                    $.ajax({
                        type: "GET",
                        url: "{{ url('get_accounts') }}"+'/'+optVal,
                        success: function(response) {
                            $('#account').empty();
                            $('#account').html('<option value="">Select Account</option>'); 
                            $.each(response, function(index, value) {
                                $('#account').append(
                                    $('<option></option>').val(value.id).html(
                                        value.code )
                                );
                            });
                        },
                        error: function() {
                            alert('Error occured');
                        }
                    });
                } else {
                    $('#account').empty();
                }
            });
        });
        function divToPrint() {
                var divToPrint = document.getElementById("div_to_print");
                const WinPrint = window.open("");
                WinPrint.document.write(`<!DOCTYPE html>`);
                WinPrint.document.write(`<head>`);
                WinPrint.document.write(`<link rel="stylesheet" type="text/css" href="app-assets/css/bootstrap.css">`);
                WinPrint.document.write(`<style>
                *{  
                    padding: 0;
                    color: black;
                }
                table tr{
                    border: 1px solid black;
                }
                `);
                WinPrint.document.write(`</style>`);
                WinPrint.document.write(`</head><body>`);
                WinPrint.document.write(`${divToPrint.innerHTML}`);
                WinPrint.document.write(`</body></html>`);
                WinPrint.document.close();
                WinPrint.print();
                WinPrint.close();
                WinPrint.focus();
            }
    </script>
@endsection
