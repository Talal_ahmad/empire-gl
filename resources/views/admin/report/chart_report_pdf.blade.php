<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <style>
        table {
    font-family: arial, sans-serif;
    border-collapse: collapse;
    width: 100%;
    }

    tr, td, th {
    border: 1px solid black;
    text-align: left;
    padding: 8px;
    }

    tr:nth-child(even) {
    /* background-color: #545050; */
    }

    </style>
</head>

<body>
    <div class="on_print" style="margin-bottom: 1rem">
        <div style="float: right; text-align:left; padding-right: 40px">
            <h3 style="margin-top: 0px;">Chart Of Accounts</h3>
            <p><b>Print Date:</b> &nbsp;&nbsp;&nbsp; {{date('Y-m-d H:i')}}</p>
        </div>
        <div>
            <h2 class="text-center fw-bolder text-decoration-underline text-uppercase">{{env('COMPANY')}}</h2>
            {{-- <p><b>Lahore</b></p> --}}
            {{-- <p>Season: {{$season}}</p> --}}
        </div>
        <div style="clear:both"></div>
    </div>
    <table>
        <thead>
            <tr>
                <th>Account Name</th>
                <th>Account Code</th>
            </tr>
        </thead>
        <tbody>
            @foreach ($levels as $level)
                <tr>
                    <th>{{ $level->code }}</th>
                    <th>{{ $level->description }}</th>
                </tr>
                @foreach($level->level2 as $level2)
                    <tr>
                        <th>{{ $level2->code }}</th>
                        <th>{{ $level2->description }}</th>
                    </tr>
                    @foreach($level2->accounts as $account)
                    <tr>
                        <td>{{ $account->code }}</td>
                        <td>{{ $account->description }}</td>
                    </tr>
                    @endforeach
                @endforeach
            @endforeach
        </tbody>
    </table>
</body>

</html>
