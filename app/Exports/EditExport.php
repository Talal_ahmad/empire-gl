<?php

namespace App\Exports;

use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;
use Maatwebsite\Excel\Concerns\WithStyles;
use Maatwebsite\Excel\Concerns\WithColumnWidths;
use PhpOffice\PhpSpreadsheet\Worksheet\Worksheet;

class EditExport implements FromView,WithColumnWidths,WithStyles
{
    protected $levels = '';

    public function __construct($vouchers) {
        $this->vouchers = $vouchers;
    }

    public function view(): View
    {
        return view('admin.report.edit_list', [
            'vouchers' => $this->vouchers,
        ]);
    }

    public function columnWidths(): array
    {
        return [
            'A' => 25,
            'B' => 35,       
            'C' => 25,        
            'D' => 25,        
            'E' => 25,        
            'F' => 25,        
            'G' => 25,        
            'H' => 25,        
        ];
    }

    public function styles(Worksheet $sheet)
    {
        $sheet->getStyle('A')->getAlignment()->setVertical('center');
        $sheet->getStyle('B')->getAlignment()->setVertical('center');
        $sheet->getStyle('C')->getAlignment()->setVertical('center');
        $sheet->getStyle('D')->getAlignment()->setVertical('center');
        $sheet->getStyle('E')->getAlignment()->setVertical('center');
        $sheet->getStyle('F')->getAlignment()->setVertical('center');
        $sheet->getStyle('G')->getAlignment()->setVertical('center');
        $sheet->getStyle('H')->getAlignment()->setVertical('center');
        return [
            // Style the first row as bold text.
            1    => ['font' => ['bold' => true]],
            'A' => ['alignment' => ['wrapText' => true]],
            'B' => ['alignment' => ['wrapText' => true]],
            'C' => ['alignment' => ['wrapText' => true]],
            'D' => ['alignment' => ['wrapText' => true]],
            'E' => ['alignment' => ['wrapText' => true]],
            'F' => ['alignment' => ['wrapText' => true]],
            'G' => ['alignment' => ['wrapText' => true]],
            'H' => ['alignment' => ['wrapText' => true]]
        ];
    }
}