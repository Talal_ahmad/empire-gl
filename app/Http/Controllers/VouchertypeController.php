<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DataTables;
use App\Models\Vouchertype;
use App\Models\UserVoucherType;
use Illuminate\Support\Facades\DB;
use Illuminate\Validation\ValidationException;

class VouchertypeController extends Controller
{
    public function __construct(){
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if($request->ajax()){
            $vouchertypes =Vouchertype::get();
            // dd($vouchertypes);
            return DataTables::of($vouchertypes)
            ->addIndexColumn()
            ->addColumn('status', function($row){
                if($row->status == 0){
                    $status = '<input  type="checkbox" data-id="'.$row->id.'" class="toggle-class" data-onstyle="success" data-offstyle="danger" data-toggle="toggle" data-on="Active" data-off="Disable" checked>';
                }
                else{
                    $status = '<input  type="checkbox" data-id="'.$row->id.'" class="toggle-class" data-onstyle="success" data-offstyle="danger" data-toggle="toggle" data-on="Active" data-off="Disable">';
                }
                return $status;
            })
            ->rawColumns(['status'])
            ->make(true);
        }
        return view('admin.VoucherTypes.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try{
            $this->validate($request , [
                'name' => 'required',
                'type' => 'required',
                'description' => 'required',
            ]);
            $Vouchertype = new Vouchertype();
            $Vouchertype->voucher_type = $request->name;
            $Vouchertype->subType = $request->type;
            $Vouchertype->description = $request->description;
            $Vouchertype->save();
            return ['code'=>'200','message'=>'success'];
        }
        catch(\Exception | ValidationException $e){
            if($e instanceof ValidationException){
                return ['code'=>'422','errors' => $e->errors()];
            }
            else{
                return ['code'=>'500','error_message'=>$e->getMessage()];
            }
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $Vouchertype = UserVoucherType::join('vouhertypes','vouhertypes.id', '=', 'user_voucher_types.voucher_type_id')->where('user_id',loginUserId())->where('voucher_type_id',$id)->first(['user_voucher_types.id','user_voucher_types.type_limit','user_voucher_types.permissions','vouhertypes.subType']);
        return response()->json($Vouchertype);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $Vouchertype = Vouchertype::find($id);
        return response()->json($Vouchertype);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        try{
            $this->validate($request , [
                'name' => 'required',
                'type' => 'required',
                'description' => 'required',
            ]);
            $Vouchertype = Vouchertype::find($id);
            $Vouchertype->voucher_type = $request->name;
            $Vouchertype->subType = $request->type;
            $Vouchertype->description = $request->description;
            $Vouchertype->update();
            return ['code'=>'200','message'=>'success'];
        }
        catch(\Exception | ValidationException $e){
            if($e instanceof ValidationException){
                return ['code'=>'422','errors' => $e->errors()];
            }
            else{
                return ['code'=>'500','error_message'=>$e->getMessage()];
            }
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try{
            $vouchertype = Vouchertype::find($id);
            $vouchertype->delete();
            return ['code'=>'200','message'=>'success'];
        }
        catch(\Exception $e){
            return ['code'=>'500','error_message'=>$e->getMessage()];
        }
    }
}
