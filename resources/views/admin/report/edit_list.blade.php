<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <style>
      table {
            border: 1px solid black;
            border-collapse: collapse;
            width:100%;
        }
        tr, td, th {
            border: 1px solid black;
            padding:4px;
        }

    </style>
</head>

<body>
    <div class="on_print" style="margin-bottom: 1rem">
        <div style="float: right; text-align:left; padding-right: 40px">
            <h3 style="margin-top: 0px;">Edit List Report</h3>
            <p><b>Print Date:</b> &nbsp;&nbsp;&nbsp; {{date('Y-m-d H:i')}}</p>
        </div>
        <div>
            <h3 style="margin-top: 0px;">{{env('COMPANY')}}</h3>
            {{-- <p><b>Lahore</b></p> --}}
            {{-- <p>Season: {{$season}}</p> --}}
        </div>
        <div style="clear:both"></div>
    </div>
    <div class="table-responsive">
        <table class="table table-bordered table-striped" id="dataTable">
            <thead>
                <tr class="left-row">
                    <th>Date</th>
                    <th>V.Type/No.</th>
                    <th>Code</th>
                    <th>Description</th>
                    <th>Voucher Naration</th>
                    <th style="text-align: right !important">Debit</th>
                    <th style="text-align: right !important">Credit</th>
                </tr>
            </thead>
            <tbody>
                @if (count($vouchers) > 0)
                @php
                    $granddebit = 0;
                    $grandcredit = 0;
                @endphp
                    @foreach ($vouchers as $voucher)
                        @php
                            $totaldebit = 0;
                            $totalcredit = 0;
                        @endphp
                        @foreach ($voucher->voucherdetails as $details)
                            @php
                                if ($details->amount > 0) {
                                    $totaldebit = $totaldebit + $details->amount;
                                    $granddebit = $granddebit + $details->amount;
                                } else {
                                    $totalcredit = $totalcredit + $details->amount;
                                    $grandcredit = $grandcredit + $details->amount;
                                }
                            @endphp
                            <tr>
                                <td style="white-space: nowrap;">{{ $details->date }}</td>
                                <td>{{ $voucher->vouchertype->voucher_type . '-' . $voucher->voucher_no }}</td>
                                <td>{{ $details->account->code }}</td>
                                <td>{{ $details->account->description }}</td>
                                <td>{{ $details->line_remarks }}</td>
                                @if ($details->amount > 0)
                                    <td style="text-align: right">
                                        {{ number_format($details->amount, 2) }}</td>
                                    <td style="text-align: right"></td>
                                @else
                                    <td style="text-align: right"></td>

                                    <td style="text-align: right">
                                        {{ number_format(abs($details->amount), 2) }}</td>
                                @endif
                            </tr>
                        @endforeach
                        <tr>
                            <td colspan="5" style="text-align: end"><b>Total</b></td>
                            <td style="text-align: right; border-top: 2px solid black !important; border-bottom: 2px solid black !important"><b>{{ number_format($totaldebit, 2) }}</b>
                            </td>
                            <td style="text-align: right; border-top: 2px solid black !important; border-bottom: 2px solid black !important"><b>{{ number_format(abs($totalcredit), 2) }}</b>
                            </td>
                        </tr>

                    @endforeach
                    <tr>
                        <td colspan="5" style="text-align: end"><b>Grand Total</b></td>
                        <td style="text-align: right; border-top: 2px solid black !important; border-bottom: 2px solid black !important"><b>{{ number_format($granddebit, 2) }}</b>
                        </td>
                        <td style="text-align: right; border-top: 2px solid black !important; border-bottom: 2px solid black !important"><b>{{ number_format(abs($grandcredit), 2) }}</b>
                        </td>
                    </tr>
                @else
                @endif
            </tbody>
        </table>
    </div>
</body>
</html>
