@extends('admin.layouts.master')
@section('title', 'Add Account')
@section('content')
    <style>
        .btnrelated {
            display: inline-block;
            padding: 6px 12px;
            margin-bottom: 0;
            font-size: 14px;
            font-weight: normal;
            line-height: 1.42857143;
            text-align: center;
            white-space: nowrap;
            vertical-align: middle;
            cursor: pointer;
            -webkit-user-select: none;
            -moz-user-select: none;
            -ms-user-select: none;
            user-select: none;
            background-image: none;
            border: 1px solid transparent;
            border-radius: 4px;
        }

        /*Also */
        .btnrelated-success {
            border: 1px solid #c5dbec;
            background: #d0e5f5;
            font-weight: bold;
            color: #2e6e9e;
        }

        /* This is copied from https://github.com/blueimp/jQuery-File-Upload/blob/master/css/jquery.fileupload.css */
        .fileinput-button {
            position: relative;
            overflow: hidden;
        }

        .fileinput-button input {
            position: absolute;
            top: 0;
            right: 0;
            margin: 0;
            opacity: 0;
            /* -ms-filter: "alpha(opacity=0)"; */
            font-size: 200px;
            direction: ltr;
            cursor: pointer;
        }

        .thumb {
            height: 80px;
            width: 100px;
            border: 1px solid #000;
        }

        ul.thumb-Images li {
            width: 120px;
            /* float: left; */
            display: inline-block;
            vertical-align: top;
            height: 120px;
        }

        .img-wraprelated {
            position: relative;
            display: inline-block;
            font-size: 0;
        }

        .img-wraprelated .closerelated {
            position: absolute;
            top: 2px;
            right: 2px;
            z-index: 100;
            background-color: #d0e5f5;
            padding: 5px 2px 2px;
            color: #000;
            font-weight: bolder;
            cursor: pointer;
            opacity: 0.5;
            font-size: 23px;
            line-height: 10px;
            border-radius: 50%;
        }

        .img-wraprelated:hover .closerelated {
            opacity: 1;
            background-color: #ff0000;
        }

        .FileNameCaptionStyle {
            font-size: 12px;
        }
    </style>
    <section class="content-wrapper container-xxl p-0">
        <div class="content-header row">
            <div class="content-header-left col-md-9 col-12 mb-2">
                <div class="row breadcrumbs-top">
                    <div class="col-12">
                        <h2 class="content-header-title float-start mb-0">Add Account</h2>
                        <div class="breadcrumb-wrapper">
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="{{ url('/dashboard') }}">Dashboard</a>
                                </li>
                                <li class="breadcrumb-item"><a href="{{ url('/accounts') }}">Accounts</a>
                                </li>
                                <li class="breadcrumb-item active">Add Account
                                </li>
                            </ol>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="content-body">
            <section id="basic-datatable">
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-body">
                                <form class="form" id="account_form" enctype="multipart/form-data">
                                    @csrf
                                    <div class="row">
                                        <div class="col-md-6 col-12">
                                            <div class="mb-1">
                                                <label class="form-label" for="role">Select Level</label>
                                                <select name="level2" id="level2" class="select2 form-select"
                                                    data-placeholder="Select Level" required>
                                                    <option value=""></option>
                                                    @foreach ($levels as $level)
                                                        <option value="{{ $level->id }}">
                                                            {{ $level->code . '-' . $level->description }}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-md-6 col-12">
                                            <div class="mb-1">
                                                <label class="form-label" for="code">Code</label>
                                                <input type="text" name="account_code" id="code" class="form-control"
                                                    placeholder="Enter Code" required />
                                            </div>
                                        </div>
                                        {{-- <div class="col-md-4 col-12">
                                            <div class="mb-1">
                                                <label class="form-label" for="role">Select Account Group</label>
                                                <select name="account_class" id="account_class" class="select2 form-select"
                                                    data-placeholder="Select  Account Group" required>
                                                    <option value=""></option>
                                                    @foreach ($accountclass as $accountclas)
                                                        <option value="{{ $accountclas->id }}">
                                                            {{ $accountclas->name}}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div> --}}
                                        <div class="col-md-6 col-12">
                                            <div class="mb-1">
                                                <label class="form-label" for="description">Description</label>
                                                <input type="text" name="description" id="description"
                                                    class="form-control" placeholder="Enter Description" required />
                                            </div>
                                        </div>
                                        <div class="col-md-6 col-12">
                                            <div class="mb-1">
                                                <label class="form-label" for="gl_type">Select Gl Type</label>
                                                <select name="gl_type" id="gl_type" class="select2 form-select" data-placeholder="Select Gl Type" required>
                                                    <option value=""></option>
                                                    <option value="Expenses">Expenses</option>
                                                    <option value="Cost of Sales">Cost of Sales</option>
                                                    <option value="Customer">Customer</option>
                                                    <option value="Supplier">Supplier</option>
                                                    <option value="Bank">Bank</option>
                                                    <option value="Assets">Assets</option>
                                                    <option value="Liability">Liability</option>
                                                    <option value="Income/sales">Income/Sales</option>
                                                    <option value="Capital">Capital</option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row" id="customer">
                                        
                                    </div>
                                    <div class="row" id="supplier">
                                        
                                    </div>
                                    <div class="row" id="bank">
                                        
                                    </div>
                                    <div class="row">
                                        <div class="col-md-2">
                                            <div class="mb-1">
                                                <div class="form-check">
                                                    <input class="form-check-input" type="checkbox" id="tag"
                                                        name="tag" value="1">
                                                    <label class="form-check-label" for="tag">
                                                        Tag
                                                    </label>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-4 pb-2 d-none" id="is_exemption">
                                            <div class="mb-1">
                                                <div class="form-check">
                                                    <input class="form-check-input" type="checkbox" id="isExemption"
                                                        name="isExemption" value="1">
                                                    <label class="form-check-label" for="isExemption">
                                                        Exemption Certificate
                                                    </label>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="exemption_certificates row" id="exemptions">
                                    </div>
                                    <div class="d-flex justify-content-end">
                                        <a href="{{ url('accounts') }}" class="btn btn-outline-danger">Cancel</a>
                                        <button type="submit" class="btn btn-primary ms-1" id="save">Save</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
            </section>
        </div>
        <div class="content-body d-none" id="table_div">
            <section id="basic-datatable">
                <div class="row">
                    <div class="col-12">
                        <div class="card pb-2">
                            <table class="table" id="dataTable">
                                <thead>
                                    <tr>
                                        <th class="not_include"></th>
                                        <th>Sr.No</th>
                                        <th>Code</th>
                                        <th>Level 2</th>
                                        <th>GL Type</th>
                                        <th>Description</th>
                                        {{-- <th class="not_include">Actions</th> --}}
                                    </tr>
                                </thead>
                                <tbody>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </section>
        </div>
    </section>
@endsection
@section('scripts')
    <script>
        
        var rowid;
        $(document).ready(function() {
            $(":input").inputmask();
            // $("#code").inputmask({
            //     "mask": "9999"
            // });
            $("#gl_type").change(function() {
                var html = '';
                if ($('#isExemption').is(":checked")) {
                    $('#isExemption').prop("checked", false);
                }
                if ($(this).val() == "Expenses") {
                    $('#customer').html('');
                    $('#supplier').html('');
                    $('#bank').html('');
                    $('#is_exemption').addClass('d-none');
                }
                else if ($(this).val() == "Customer") {
                    $('#supplier').html('');
                    $('#bank').html('');
                    html += `
                        <div class="col-md-4 col-12">
                            <div class="mb-1">
                                <label class="form-label" for="customer_name">Customer Name</label>
                                <input type="text" name="customer_name" id="customer_name" class="form-control" placeholder="Customer Name" />
                            </div>
                        </div>
                        <div class="col-md-4 col-12">
                            <div class="mb-1">
                                <label class="form-label" for="customer_short_name">Short Name</label>
                                <input type="text" name="customer_short_name" id="customer_short_name" class="form-control" placeholder="Short Name" />
                            </div>
                        </div>
                        <div class="col-md-4 col-12">
                            <div class="mb-1">
                                <label class="form-label" for="customer_ntn">NTN</label>
                                <input type="text" name="customer_ntn" id="customer_ntn" class="form-control" placeholder="NTN"  />
                            </div>
                        </div>
                        <div class="col-md-4 col-12">
                            <div class="mb-1">
                                <label class="form-label" for="customer_stn">STN</label>
                                <input type="text" name="customer_stn" id="customer_stn" class="form-control" placeholder="STN"  />
                            </div>
                        </div>
                        <div class="col-md-4 col-12">
                            <div class="mb-1">
                                <label class="form-label" for="customer_email">Email</label>
                                <input type="text" name="customer_email" id="customer_email" class="form-control" placeholder="Email" />
                            </div>
                        </div>
                        <div class="col-md-4 col-12">
                            <div class="mb-1">
                                <label class="form-label" for="customer_contact_no">Contact No</label>
                                <input type="number" name="customer_contact_no" id="customer_contact_no" class="form-control" placeholder="Contact no" />
                            </div>
                        </div>
                        <div class="col-12">
                            <div class="mb-1">
                                <label class="form-label" for="customer_address">Address</label>
                                <input type="text" name="customer_address" id="customer_address" class="form-control" placeholder="Address"  />
                            </div>
                        </div>
                    `;
                    $('#customer').html(html);
                    $('#is_exemption').removeClass('d-none');
                } 
                else if ($(this).val() == "Supplier") {
                    $('#customer').html('');
                    $('#bank').html('');
                    html += `
                    <div class="col-md-4 col-12">
                        <div class="mb-1">
                            <label class="form-label" for="suplier_name">Supplier Name</label>
                            <input type="text" name="suplier_name" id="suplier_name" class="form-control" placeholder="Supplier Name"  />
                        </div>
                    </div>
                    <div class="col-md-4 col-12">
                        <div class="mb-1">
                            <label class="form-label" for="supplier_short_name">Short Name</label>
                            <input type="text" name="supplier_short_name" id="supplier_short_name" class="form-control" placeholder="Supplier Short Name" />
                        </div>
                    </div>
                    <div class="col-md-4 col-12">
                        <div class="mb-1">
                            <label class="form-label" for="ntn">NTN</label>
                            <input type="text" name="supplier_ntn" id="supplier_ntn"
                                class="form-control" placeholder="Supplier NTN"  />
                        </div>
                    </div>
                    <div class="col-md-4 col-12">
                        <div class="mb-1">
                            <label class="form-label" for="supplier_stn">STN</label>
                            <input type="text" name="supplier_stn" id="supplier_stn"
                                class="form-control" placeholder="Supplier STN"  />
                        </div>
                    </div>
                    <div class="col-md-4 col-12">
                        <div class="mb-1">
                            <label class="form-label" for="supplier_email">Email</label>
                            <input type="text" name="supplier_email" id="supplier_email" class="form-control" placeholder="Supplier Email"  />
                        </div>
                    </div>
                    <div class="col-md-4 col-12">
                        <div class="mb-1">
                            <label class="form-label" for="supplier_contact_no">Contact No</label>
                            <input type="number" name="supplier_contact_no" id="supplier_contact_no" class="form-control"
                                placeholder="Supplier Contact No"  />
                        </div>
                    </div>
                    <div class="col-12">
                        <div class="mb-1">
                            <label class="form-label" for="supplier_address">Address</label>
                            <input type="text" name="supplier_address" id="supplier_address"
                                class="form-control" placeholder="Supplier Address"  />
                        </div>
                    </div>
                    `;
                    $('#supplier').html(html);
                    $('#is_exemption').removeClass('d-none');
                } 
                else if ($(this).val() == "Bank") {
                    $('#supplier').html('');
                    $('#customer').html('');
                    html += `
                    <div class="col-md-4 col-12">
                        <div class="mb-1">
                            <label class="form-label" for="bank_name">Bank Name</label>
                            <input type="text" name="bank_name" id="bank_name"
                                class="form-control" placeholder="Bank Name" />
                        </div>
                    </div>
                    <div class="col-md-4 col-12">
                        <div class="mb-1">
                            <label class="form-label" for="account_title">Account Title</label>
                            <input type="text" name="account_title" id="account_title"
                                class="form-control" placeholder="Account Title" />
                        </div>
                    </div>
                    <div class="col-md-4 col-12">
                        <div class="mb-1">
                            <label class="form-label" for="account_number">Account Number</label>
                            <input type="text" name="account_number" id="account_number" class="form-control" placeholder="Account Number" />
                        </div>
                    </div>
                    <div class="col-md-6 col-12">
                        <div class="mb-1">
                            <label class="form-label" for="bank_address">Branch Address</label>
                            <input type="text" name="bank_address" id="bank_address" class="form-control" placeholder="Branch Address" />
                        </div>
                    </div>
                    <div class="col-md-6 col-12">
                        <div class="mb-1">
                            <label class="form-label" for="location">Location</label>
                            <input type="text" name="location" id="location" class="form-control" placeholder="Location"  />
                        </div>
                    </div>
                    `;
                    $('#bank').html(html);
                    $('#is_exemption').addClass('d-none');
                } else {
                    $('#customer').html('');
                    $('#supplier').html('');
                    $('#bank').html('');
                    $('#is_exemption').addClass('d-none');
                }
            });

            $("#isExemption").click(function() {
                if (this.checked) {
                    var html = `
                    <div data-repeater-list="exemption_certificates">
                        <div data-repeater-item>
                            <div class="row d-flex align-items-end">
                                <div class="mb-1 col-md-4 col-12">
                                    <label class="form-label" for="exemption_certificate_number">Exemption Certificate
                                        Number</label>
                                    <input type="type" id="exemption_certificate_number"
                                        class="form-control"autocomplete="off" placeholder="Exemption Certificate Number"
                                        name="exemption_certificate_number" />
                                </div>
                                <div class="mb-1 col-md-4 col-12">
                                    <label class="form-label" for="from_date">From Date</label>
                                    <input type="date" id="from_date" class="form-control"autocomplete="off" placeholder=""
                                        name="from_date" />
                                </div>
                                <div class="mb-1 col-md-4 col-12">
                                    <label class="form-label" for="expire_date">Expire Date</label>
                                    <input type="date" id="expire_date" class="form-control"autocomplete="off" name="expire_date" />
                                </div>
                                <div class="mb-1 col-12">
                                    <label class="form-label" for="image_file">Certificate Image</label>
                                    <input type="file" name="image_file" class="form-control image_file" placeholder="Image" />
                                </div>
                                <div class="col-md-2 col-12 mb-51">
                                    <div class="mb-1">
                                        <button class="btn btn-outline-danger text-nowrap px-1"
                                            data-repeater-delete type="button">
                                            <i data-feather="x" class="me-25"></i>
                                            <span>Delete</span>
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-12">
                            <button class="btn btn-icon btn-primary" type="button"
                                data-repeater-create>
                                <i data-feather="plus" class="me-25"></i>
                                <span>Add New</span>
                            </button>
                        </div>
                    </div>
                    `;
                    $('#exemptions').html(html);
                    $('.exemption_certificates').repeater({
                        isFirstItemUndeletable: true,
                        show: function() {
                            $(this).slideDown();
                            // Feather Icons
                            if (feather) {
                                feather.replace({
                                    width: 14,
                                    height: 14
                                });
                            }
                        },
                        hide: function(deleteElement) {
                            $(this).slideUp(deleteElement);
                        }
                    });
                } 
                else {
                    $('#exemptions').html('');
                }
            });

            $("#account_form").submit(function(e) {
                e.preventDefault();
                ButtonStatus('#save',true);
                blockUI();
                $.ajax({
                    url: "{{ route('accounts.store') }}",
                    type: "POST",
                    data: new FormData(this),
                    processData: false,
                    contentType: false,
                    responsive: true,
                    success: function(response) {
                        console.log(response);
                        ButtonStatus('#save',false);
                        $.unblockUI();
                        if (response.errors) {
                            $.each(response.errors, function(index, value) {
                                Toast.fire({
                                    icon: 'error',
                                    title: value
                                })
                            });
                        }
                        else if (response.error_message && response.code == 500) {
                            Toast.fire({
                                icon: 'error',
                                title: response.error_message,
                            })
                        } 
                        else if (response.error_message) {
                            Toast.fire({
                                icon: 'error',
                                title: 'An error has been occured! Please Contact Administrator.'
                            })
                        } 
                        else {
                            $('#account_form')[0].reset();
                            Toast.fire({
                                icon: 'success',
                                title: 'Account has been Added Successfully!'
                            })
                            setTimeout(function(){
                                window.location.href = "{{ url('accounts') }}";
                            }, 1000);
                        }
                    }
                });
            });
            $("#code").click(function() {
                $('#table_div').hide();
            });
            $("#level2").change(function(){
                $('#table_div').removeClass('d-none');
                $('#table_div').show();
                var level2 = $(this).val();
                dataTable = $('#dataTable').DataTable({
                    bDestroy: true,
                    processing: true,
                    serverSide: true,
                    responsive: true,
                    ajax:{
                        url : "{{ route('accounts.index') }}",
                        data: {level_2_id : level2}
                    },
                    columns: [
                        {
                            data: 'responsive_id',
                            searchable: false,
                            orderable:false
                        },
                        {
                            data: 'DT_RowIndex', name: 'DT_RowIndex', orderable: false, searchable: false 
                        },
                        {
                            data: 'code',
                            name: 'code',
                        },
                        {
                            data: 'level2_code',
                            name: 'level2_code',
                        },
                        {
                            data: 'gl_type',
                            name: 'gl_type',
                        },
                        {
                            data: 'description',
                            name: 'description',
                        },
                        
                    ],
                    "columnDefs": [{
                            // For Responsive
                            className: 'control',
                            orderable: false,
                            searchable: false,
                            targets: 0
                        },
                    
                        {
                            "defaultContent": "-",
                            "targets": "_all"
                        }
                    ],
                    "order": [
                        [0, 'asc']
                    ],
                    dom: '<"card-header border-bottom p-1"<"head-label"><"dt-action-buttons text-end"B>><"d-flex justify-content-between align-items-center mx-0 row"<"col-sm-12 col-md-6"l><"col-sm-12 col-md-6"f>>t<"d-flex justify-content-between mx-0 row"<"col-sm-12 col-md-6"i><"col-sm-12 col-md-6"p>>',
                    displayLength: 10,
                    lengthMenu: [10, 25, 50, 75, 100],
                    buttons: [{
                            extend: 'collection',
                            className: 'btn btn-outline-secondary dropdown-toggle me-2',
                            text: feather.icons['share'].toSvg({
                                class: 'font-small-4 me-50'
                            }) + 'Export',
                            buttons: [{
                                    extend: 'print',
                                    text: feather.icons['printer'].toSvg({
                                        class: 'font-small-4 me-50'
                                    }) + 'Print',
                                    className: 'dropdown-item',
                                    exportOptions: {
                                        columns: ':not(.not_include)'
                                    }
                                },
                                {
                                    extend: 'csv',
                                    text: feather.icons['file-text'].toSvg({
                                        class: 'font-small-4 me-50'
                                    }) + 'Csv',
                                    className: 'dropdown-item',
                                    exportOptions: {
                                        columns: ':not(.not_include)'
                                    }
                                },
                                {
                                    extend: 'excel',
                                    text: feather.icons['file'].toSvg({
                                        class: 'font-small-4 me-50'
                                    }) + 'Excel',
                                    className: 'dropdown-item',
                                    exportOptions: {
                                        columns: ':not(.not_include)'
                                    }
                                },
                                {
                                    extend: 'pdf',
                                    text: feather.icons['clipboard'].toSvg({
                                        class: 'font-small-4 me-50'
                                    }) + 'Pdf',
                                    className: 'dropdown-item',
                                    exportOptions: {
                                        columns: ':not(.not_include)'
                                    }
                                },
                                {
                                    extend: 'copy',
                                    text: feather.icons['copy'].toSvg({
                                        class: 'font-small-4 me-50'
                                    }) + 'Copy',
                                    className: 'dropdown-item',
                                    exportOptions: {
                                        columns: ':not(.not_include)'
                                    }
                                }
                            ],
                            init: function(api, node, config) {
                                $(node).removeClass('btn-secondary');
                                $(node).parent().removeClass('btn-group');
                                setTimeout(function() {
                                    $(node).closest('.dt-buttons').removeClass('btn-group')
                                        .addClass('d-inline-flex');
                                }, 50);
                            }
                        }
                    ],
                    responsive: {
                        details: {
                            display: $.fn.dataTable.Responsive.display.childRowImmediate,
                            type: 'column',
                        }
                    },
                    language: {
                        paginate: {
                            // remove previous & next text from pagination
                            previous: '&nbsp;',
                            next: '&nbsp;'
                        }
                    }
                });
                $('div.head-label').html('<h6 class="mb-0">List of Accounts</h6>');
            });
        });
    </script>
@endsection
