<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\Traits\LogsActivity;
use Spatie\Activitylog\LogOptions;
use Spatie\Activitylog\Models\Activity;

class AccountDetail extends Model
{
    use HasFactory,LogsActivity;
    protected $appends = ['account'];

    public function getActivitylogOptions(): LogOptions
    {
        $login_user = \Auth::user()->name;
        return LogOptions::defaults()
        ->logOnly(['account_id','customer_id','supplier_id','exemption_certificate_number','from_date','expire_date','certificate_image'])
        ->useLogName('Account Certificates')
        // ->logOnlyDirty()
        ->setDescriptionForEvent(fn(string $eventName) => "{$login_user} have {$eventName} Account Certificates");
    }

    // public function tapActivity(Activity $activity, string $eventName)
    // {
    //     $activity->properties = $activity->properties->put('account_code', $this->account->code);
    //     $activity->properties = $activity->properties->put('gl_type', $this->account->gl_type);
    //     $activity->properties = $activity->properties->put('account_name', $this->account->name);
    // }

    public function account()
    {
        return $this->belongsTo(Account::class,'account_id');
    }
}
