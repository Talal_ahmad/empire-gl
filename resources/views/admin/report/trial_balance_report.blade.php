<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <style>
      table {
            border: 1px solid black;
            border-collapse: collapse;
            width:100%;
        }
        tr, td, th {
            border: 1px solid black;
            padding:4px;
        }

    </style>
</head>

<body>
    <div class="on_print" style="margin-bottom: 1rem">
        <div style="float: right; text-align:left; padding-right: 40px">
            <h3 style="margin-top: 0px;">Trial Balance Report</h3>
            <p><b>Print Date:</b> &nbsp;&nbsp;&nbsp; {{date('Y-m-d H:i')}}</p>
        </div>
        <div>
            <h3 style="margin-top: 0px;">{{env('COMPANY')}}</h3>
            <p><b>Lahore</b></p>
            {{-- <p>Season: {{$season}}</p> --}}
        </div>
        <div style="clear:both"></div>
    </div>
    <div class="table-responsive">
        <table class="table table-bordered table-striped" id="dataTable">
            <thead>
                <tr>
                    <th rowspan="2" >Account</th>
                    <th rowspan="2">Group</th>
                    @if (request('summary') != 1)
                    <th colspan="2" class="text-center" style="text-align: center">Opening</th>
                    <th colspan="2" class="text-center" style="text-align: center">Transaction</th>
                    @endif
                    <th colspan="2" class="text-center" style="text-align: center">Closing Balance</th>
                </tr>
                <tr>
                    @if (request('summary') != 1)
                    <th style="padding: 0; text-align:center;">Debit</th>s
                    <th style="padding: 0; text-align:center;">Credit</th>                                           
                    <th style="padding: 0; text-align:center;">Debit</th>
                    <th style="padding: 0; text-align:center;">Credit</th>
                    @endif
                    <th style="padding: 0; text-align:center;">Debit</th>
                    <th style="padding: 0; text-align:center;">Credit</th>
                </tr>
            </thead>
            <tbody>
                @php
                    $totalOpeningDebitBalance = 0;
                    $totalOpeningCreditsBalance = 0;
                    $totalClosingDebitBalance = 0;
                    $totalClosingCreditBalance = 0;
                    $totalTranscationDebit = 0;
                    $totalTranscationCredit = 0;
                @endphp
                @forelse($levels as $level)
                    @if (count($level->level2) > 0)
                    @php
                    $totalLevel1OpeningDebitBalance = 0;
                    $totalLevel1OpeningCreditsBalance = 0;
                    $totalLevel1ClosingDebitBalance = 0;
                    $totalLevel1ClosingCreditBalance = 0;
                    $totalLevel1TranscationDebit = 0;
                    $totalLevel1TranscationCredit = 0;
                @endphp
                        <tr>
                            <th>{{ $level->code }}</th>
                            <th colspan="7">{{ $level->description }}</th>
                        </tr>
                        @foreach ($level->level2 as $level2)
                            @if (count($level2->accounts) > 0)
                                <tr>
                                    <th>{{ $level2->code }}</th>
                                    <th colspan="7">{{ $level2->description }}</th>
                                </tr>
                                @php
                                    $level2OpeningDebitBalance = 0;
                                    $level2OpeningCreditsBalance = 0;
                                    $level2ClosingDebitBalance = 0;
                                    $level2ClosingCreditBalance = 0;
                                    $level2TranscationDebit = 0;
                                    $level2TranscationCredit = 0;
                                @endphp
                                @foreach ($level2->accounts as $account)
                                    @php
                                        $check = false;
                                        if(!empty($account->voucherdetails[0])){
                                            $check = request('zero') == 1 && $account->voucherdetails[0]['openingDebitBalance'] == 0 && $account->voucherdetails[0]['openingCreditsBalance'] == 0 && $account->voucherdetails[0]['closingDebitBalance'] == 0 && $account->voucherdetails[0]['closingCreditBalance'] == 0 && $account->voucherdetails[0]['transcationDebit'] == 0 && $account->voucherdetails[0]['transcationCredit'] == 0;
                                        }
                                    @endphp
                                    @if ($check == false && count($account->voucherdetails) > 0)
                                        <tr>
                                            <td>{{ $account->code }}</td>
                                            <td>{{ $account->description }}</td>
                                            @php
                                                                            $openingBalance = 0;
                                                $accountClass = checkAccountClass($level->account_class_id);
                                                if ($accountClass->name == 'Expenses' || $accountClass->name == 'Cost' || $accountClass->name == 'Income') {
                                                    $openingBalance = openingBalance($account->id, request('fromDate'), true);
                                                    $closingBalance = $openingBalance + $account->voucherdetails[0]['transcationDebit'] - abs($account->voucherdetails[0]['transcationCredit']);
                                                } else {
                                                    $openingBalance = $account->voucherdetails[0]['openingDebitBalance'] + $account->voucherdetails[0]['openingCreditsBalance'];
                                                    $closingBalance = $openingBalance + $account->voucherdetails[0]['transcationDebit'] - abs($account->voucherdetails[0]['transcationCredit']);
                                                }
                                                if ($openingBalance < 0) {
                                                    $level2OpeningCreditsBalance += abs($openingBalance);
                                                    $totalLevel1OpeningCreditsBalance += abs($openingBalance);
                                                    $totalOpeningCreditsBalance += abs($openingBalance);
                                                } else {
                                                    $level2OpeningDebitBalance += $openingBalance;
                                                    $totalLevel1OpeningDebitBalance += $openingBalance;
                                                    $totalOpeningDebitBalance += $openingBalance;
                                                }
                                                if ($closingBalance < 0) {
                                                    $totalClosingCreditBalance += abs($closingBalance);
                                                    $totalLevel1ClosingCreditBalance += abs($closingBalance);
                                                    $level2ClosingCreditBalance += abs($closingBalance);
                                                } else {
                                                    $totalClosingDebitBalance += $closingBalance;
                                                    $totalLevel1ClosingDebitBalance += $closingBalance;
                                                    $level2ClosingDebitBalance += $closingBalance;
                                                }
                                                // $totalClosingDebitBalance += $account->voucherdetails[0]['closingDebitBalance'];
                                                // $totalClosingCreditBalance += abs($account->voucherdetails[0]['closingCreditBalance']);
                                                $totalTranscationDebit += $account->voucherdetails[0]['transcationDebit'];
                                                $totalTranscationCredit += abs($account->voucherdetails[0]['transcationCredit']);
                                                $level2TranscationDebit += abs($account->voucherdetails[0]['transcationDebit']);
                                                $level2TranscationCredit += abs($account->voucherdetails[0]['transcationCredit']);
                                                $totalLevel1TranscationDebit += abs($account->voucherdetails[0]['transcationDebit']);
                                                $totalLevel1TranscationCredit += abs($account->voucherdetails[0]['transcationCredit']);
                                            @endphp
                                            @if (request('summary') != 1)
                                                <td style="text-align: right">
                                                    {{ $openingBalance > 0 ? number_format($openingBalance, 2) : 0 }}
                                                </td>
                                                <td style="text-align: right">
                                                    {{ $openingBalance < 0 ? number_format(abs($openingBalance), 2) : 0 }}
                                                </td>
                                                <td style="text-align: right">
                                                    {{ number_format($account->voucherdetails[0]['transcationDebit'], 2) }}
                                                </td>
                                                <td style="text-align: right">
                                                    {{ number_format(abs($account->voucherdetails[0]['transcationCredit']), 2) }}
                                                </td>
                                            @endif
                                            <td style="text-align: right">
                                                {{ $closingBalance > 0 ? number_format($closingBalance, 2) : 0 }}
                                            </td>
                                            <td style="text-align: right">
                                                {{ $closingBalance < 0 ? number_format(abs($closingBalance), 2) : 0 }}
                                            </td>
                                        </tr>
                                    @else
                                        @if (request('zero') != 1)
                                            <tr>
                                                <td>{{ $account->code }}</td>
                                                <td>{{ $account->description }}</td>
                                                @if (request('summary') != 1)
                                                <td style="text-align: right">0</td>
                                                <td style="text-align: right">0</td>
                                                <td style="text-align: right">0</td>
                                                <td style="text-align: right">0</td>
                                                @endif
                                                <td style="text-align: right">0</td>
                                                <td style="text-align: right">0</td>
                                            </tr>
                                        @endif
                                    @endif
                                @endforeach
                                <tr>
                                    <td style="font-size: 15px" colspan="2"><b>Sub
                                            Total</b></td>
                                    @if (request('summary') != 1)
                                        <td style="text-align: right">
                                            <b>{{ number_format($level2OpeningDebitBalance, 2) }}</b>
                                        </td>
                                        <td style="text-align: right">
                                            <b>{{ number_format($level2OpeningCreditsBalance, 2) }}</b>
                                        </td>
                                        <td style="text-align: right">
                                            <b>{{ number_format($level2TranscationDebit, 2) }}</b>
                                        </td>
                                        <td style="text-align: right">
                                            <b>{{ number_format($level2TranscationCredit, 2) }}</b>
                                        </td>
                                    @endif
                                    <td style="text-align: right">
                                        <b>{{ number_format($level2ClosingDebitBalance, 2) }}</b>
                                    </td>
                                    <td style="text-align: right">
                                        <b>{{ number_format($level2ClosingCreditBalance, 2) }}</b>
                                    </td>
                                </tr>
                            @endif
                        @endforeach
                        <tr>
                            <td style="font-size: 15px" colspan="2"><b>Main Code Total:</b></td>
                            @if (request('summary') != 1)
                                <td style="text-align: right">
                                    <b>{{ number_format($totalLevel1OpeningDebitBalance, 2) }}</b>
                                </td>
                                <td style="text-align: right">
                                    <b>{{ number_format($totalLevel1OpeningCreditsBalance, 2) }}</b>
                                </td>
                                <td style="text-align: right">
                                    <b>{{ number_format($totalLevel1TranscationDebit, 2) }}</b>
                                </td>
                                <td style="text-align: right">
                                    <b>{{ number_format($totalLevel1TranscationCredit, 2) }}</b>
                                </td>
                            @endif
                            <td style="text-align: right">
                                <b>{{ number_format($totalLevel1ClosingDebitBalance, 2) }}</b>
                            </td>
                            <td style="text-align: right">
                                <b>{{ number_format($totalLevel1ClosingCreditBalance, 2) }}</b>
                            </td>
                        </tr>
                    @endif
                @empty
                    <tr class="text-center">
                        <td colspan="8">
                            <h5>No Record Found!</h5>
                        </td>
                    </tr>
                @endforelse
                @if (isset($levels) && count($levels) > 0)
                    <tr>
                        <td style="font-size: 15px" colspan="2"><b>Total</b></td>
                        @if (request('summary') != 1)
                            <td style="text-align: right">
                                <b>{{ number_format($totalOpeningDebitBalance, 2) }}</b></td>
                            <td style="text-align: right">
                                <b>{{ number_format($totalOpeningCreditsBalance, 2) }}</b></td>
                            <td style="text-align: right">
                                <b>{{ number_format($totalTranscationDebit, 2) }}</b></td>
                            <td style="text-align: right">
                                <b>{{ number_format($totalTranscationCredit, 2) }}</b></td>
                        @endif
                        <td style="text-align: right">
                            <b>{{ number_format($totalClosingDebitBalance, 2) }}</b></td>
                        <td style="text-align: right">
                            <b>{{ number_format($totalClosingCreditBalance, 2) }}</b></td>
                    </tr>
                @endif
            </tbody>
        </table>
    </div>
</body>
</html>
