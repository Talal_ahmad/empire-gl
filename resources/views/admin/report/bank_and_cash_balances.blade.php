@extends('admin.layouts.master')
@section('title', 'Bank & Cash Balances Report')
@section('style')
    <style>
        table {
            font-family: arial, sans-serif;
            /* border-collapse: collapse; */
            /* width: 100%; */
        }

        th,
        tr,
        td {
            border: 1px solid black;
            text-align: left;
            padding: 8px;
        }

        tr:nth-child(even) {
            /* background-color: #545050; */
        }
    </style>
@endsection
@section('content')
    <section class="content-wrapper container-xxl p-0">
        <div class="content-header row">
            <div class="content-header-left col-md-9 col-12 mb-2">
                <div class="row breadcrumbs-top">
                    <div class="col-12">
                        <h2 class="content-header-title float-start mb-0">Bank & Cash Balances Report</h2>
                        <div class="breadcrumb-wrapper">
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="{{ url('/') }}">Dashboard</a>
                                </li>
                                <li class="breadcrumb-item"><a href="#">Reports</a>
                                </li>
                                <li class="breadcrumb-item active">Bank & Cash Balances Report
                                </li>
                            </ol>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="content-body">
            <div class="row">
                <div class="col-12">
                    <div class="card mb-1">
                        <div class="card-body">
                            <form id="search_form" action="">
                                <div class="row">
                                    <div class="col-md-4">
                                        <label class="form-label" for="date">From</label>
                                        <input type="date" id="date" class="form-control" name="fromDate"
                                            value="{{ request('fromDate') }}" />
                                    </div>
                                    <div class="col-md-4">
                                        <label class="form-label" for="date">To</label>
                                        <input type="date" id="date" class="form-control" name="toDate"
                                            value="{{ !empty(request('toDate')) ? request('toDate') : date('Y-m-d') }}" />
                                    </div>
                                    <div class="col-md-4 col-12 text-end mt-1">
                                        <a href="{{ url('bank_and_cash_balances') }}" type="button"
                                            class="btn btn-danger mt-1">Reset</a>
                                        <button type="submit" class="btn btn-primary mt-1">Apply</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                    <div id="div_to_print">
                        <div class="card p-2 pb-0">
                            <div class="row">
                                <div class="col-12">
                                    <h2 class="text-center fw-bolder text-uppercase">UE - GL</h2>
                                    <p class="text-center fw-bolder text-uppercase">Bank & Cash Balances Report </p>
                                    <p class="text-center fw-bolder text-uppercase ">From
                                        {{ request('fromDate') ? request('fromDate') : '-' }} To {{ request('toDate') ? request('toDate') : date('Y-m-d') }} </p>
                                </div>
                            </div>
                            <table class="table table-striped mt-3" id="dataTable">
                                @php
                                    $all_banks_opening_grand_sum = 0;
                                    $all_banks_during_period_grand_sum = 0;
                                    $all_banks_closing_grand_sum = 0;
                                @endphp
                                <thead>
                                    <tr>
                                        <th scope="col" class="text-center">Code</th>
                                        <th scope="col" class="text-center">Title</th>
                                        <th scope="col" class="text-center">Debit</th>
                                        <th scope="col" class="text-center">Credit</th>
                                        <th scope="col" class="text-center">Debit</th>
                                        <th scope="col" class="text-center">Credit</th>
                                        <th scope="col" class="text-center">Debit</th>
                                        <th scope="col" class="text-center">Credit</th>
                                    </tr>
                                </thead>
                                {{-- Loop accounts --}}
                                @foreach ($accounts as $key => $account)
                                    {{-- Table header --}}
                                    <tr>
                                        <td colspan="2" class="fw-bolder fs-4">
                                            {{ $key }}
                                        </td>
                                        <td class=" d-none "></td>
                                        <td colspan="2" class="text-center fw-bolder">Opening Balance</td>
                                        <td class="d-none"></td>
                                        <td colspan="2" class="text-center fw-bolder">During The Period</td>
                                        <td class="d-none"></td>
                                        <td colspan="2" class="text-center fw-bolder ">Closing Balance</td>
                                        <td class="d-none"></td>
                                    </tr>
                                    <tr>
                                        <td colspan="2" class="text-center border-end-0 border-black fw-bolder fs-5">
                                            Bank Accounts</td>
                                        <td class="d-none"></td>
                                        <td colspan="6" class="border-start-0 border-black text-end "></td>
                                        <td class="d-none"></td>
                                        <td class="d-none"></td>
                                        <td class="d-none"></td>
                                        <td class="d-none"></td>
                                        <td class="d-none"></td>
                                    </tr>
                                    {{-- End of table header --}}

                                    {{-- Table body --}}
                                    @php
                                        $all_banks_opening_sum = 0;
                                        $all_banks_closing_sum = 0;
                                        $all_banks_during_period_sum = 0;
                                    @endphp

                                    {{-- Loop account details --}}
                                    @foreach ($account as $key => $account_details)
                                        @php
                                            $opening_balance =
                                                $account_details['opening_balance']->debit_amount +
                                                $account_details['opening_balance']->credit_amount;

                                            $during_the_period_balance =
                                                $account_details['total_debit_amount'] +
                                                $account_details['total_credit_amount'];

                                            $closing_balance =
                                                $account_details['closing_balance']->debit_amount +
                                                $account_details['closing_balance']->credit_amount;
                                        @endphp

                                        {{-- Table rows for account details --}}
                                        <tr>
                                            <td>{{ $account_details['accounts_code'] }}</td>
                                            <td>{{ $key }}</td>
                                            {{-- Opening balance --}}
                                            @if ($opening_balance > 0)
                                                <td class="text-center">{{ abs($opening_balance) }}</td>
                                                <td class="text-center">-</td>
                                            @elseif ($opening_balance < 0)
                                                <td class="text-center">-</td>
                                                <td class="text-center">{{ abs($opening_balance) }}</td>
                                            @else
                                                <td class="text-center">0</td>
                                                <td class="text-center">0</td>
                                            @endif
                                            {{-- During the period balance --}}
                                            @if ($during_the_period_balance > 0)
                                                <td class="text-center">{{ abs($during_the_period_balance) }}</td>
                                                <td class="text-center">-</td>
                                            @elseif ($during_the_period_balance < 0)
                                                <td class="text-center">-</td>
                                                <td class="text-center">{{ abs($during_the_period_balance) }}</td>
                                            @else
                                                <td class="text-center">0</td>
                                                <td class="text-center">0</td>
                                            @endif
                                            {{-- Closing balance --}}
                                            @if ($closing_balance > 0)
                                                <td class="text-center">{{ abs($closing_balance) }}</td>
                                                <td class="text-center">-</td>
                                            @elseif ($closing_balance < 0)
                                                <td class="text-center">-</td>
                                                <td class="text-center">{{ abs($closing_balance) }}</td>
                                            @else
                                                <td class="text-center">0</td>
                                                <td class="text-center">0</td>
                                            @endif
                                        </tr>
                                        {{-- End of table rows for account details --}}
                                        @php
                                            $all_banks_opening_sum += $opening_balance;
                                            $all_banks_during_period_sum += $during_the_period_balance;
                                            $all_banks_closing_sum += $closing_balance;
                                            $all_banks_opening_grand_sum += $opening_balance;
                                            $all_banks_during_period_grand_sum += $during_the_period_balance;
                                            $all_banks_closing_grand_sum += $closing_balance;
                                        @endphp
                                    @endforeach
                                    {{-- End of loop account details --}}

                                    {{-- Table row for sub total --}}
                                    <tr>
                                        <td colspan="2" class="text-center fw-bolder">Sub Total</td>
                                        <td class=" d-none "></td>
                                        <td class="text-center fw-bolder ">
                                            {{ $all_banks_opening_sum > 0 ? $all_banks_opening_sum : '-' }}</td>
                                        <td class="text-center fw-bolder ">
                                            {{ $all_banks_opening_sum < 0 ? $all_banks_opening_sum : '-' }}</td>
                                        <td class="text-center fw-bolder ">
                                            {{ $all_banks_during_period_sum > 0 ? $all_banks_during_period_sum : '-' }}
                                        </td>
                                        <td class="text-center fw-bolder ">
                                            {{ $all_banks_during_period_sum < 0 ? $all_banks_during_period_sum : '-' }}
                                        </td>
                                        <td class="text-center fw-bolder ">
                                            {{ $all_banks_closing_sum > 0 ? $all_banks_closing_sum : '-' }}</td>
                                        <td class="text-center fw-bolder ">
                                            {{ $all_banks_closing_sum < 0 ? $all_banks_closing_sum : '-' }}</td>
                                    </tr>
                                    {{-- End of table row for sub total --}}
                                @endforeach
                                {{-- End of loop accounts --}}

                                {{-- Table footer --}}
                                {{-- @dd($accounts) --}}
                                @if ($all_banks_opening_grand_sum != 0 && $all_banks_during_period_grand_sum != 0 && $all_banks_closing_grand_sum != 0)
                                    <tfoot>
                                        <tr>
                                            <td colspan="2"><span class="fs-4 fw-bolder">Grand Total:</span></td>
                                            <td class=" d-none"></td>
                                            <td class="text-center">
                                                {{ $all_banks_opening_grand_sum > 0 ? $all_banks_opening_grand_sum : '-' }}
                                            </td>
                                            <td class="text-center">
                                                {{ $all_banks_opening_grand_sum < 0 ? $all_banks_opening_grand_sum : '-' }}
                                            </td>
                                            <td class="text-center">
                                                {{ $all_banks_during_period_grand_sum > 0 ? $all_banks_during_period_grand_sum : '-' }}
                                            </td>
                                            <td class="text-center">
                                                {{ $all_banks_during_period_grand_sum < 0 ? $all_banks_during_period_grand_sum : '-' }}
                                            </td>
                                            <td class="text-center">
                                                {{ $all_banks_closing_grand_sum > 0 ? $all_banks_closing_grand_sum : '-' }}
                                            </td>
                                            <td class="text-center">
                                                {{ $all_banks_closing_grand_sum < 0 ? $all_banks_closing_grand_sum : '-' }}
                                            </td>
                                        </tr>
                                @endif
                                </tfoot>
                                {{-- End of table footer --}}
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        </div>
    </section>
@endsection
@section('scripts')
    <script src="{{ asset('assets/js/scripts.js') }}"></script>
    <script>
        $(document).ready(function() {
            $('#dataTable').DataTable({
                processing: true,
                responsive: true,
                ordering: false,
                // scrollX: true,
                "columnDefs": [{
                        className: 'control',
                        searchable: false,
                        orderable: false
                    },
                    {
                        "defaultContent": "-",
                        "targets": "_all"
                    }
                ],
                "order": [
                    [0, 'asc']
                ],
                dom: '<"card-header border-bottom p-1"<"head-label"><"dt-action-buttons text-end"B>><"d-flex justify-content-between align-items-center mx-0 row"<"col-sm-12 col-md-6"l><"col-sm-12 col-md-6"f>>t<"d-flex justify-content-between mx-0 row"<"col-sm-12 col-md-6"i><"col-sm-12 col-md-6"p>>',
                displayLength: 10,
                lengthMenu: [10, 25, 50, 75, 100],
                buttons: [{
                    extend: 'collection',
                    className: 'btn btn-outline-secondary dropdown-toggle app me-2',
                    text: feather.icons['share'].toSvg({
                        class: 'font-small-4 me-50'
                    }) + 'Export',
                    buttons: [{
                            extend: 'csv',
                            text: feather.icons['file-text'].toSvg({
                                class: 'font-small-4 me-50'
                            }) + 'Csv',
                            className: 'dropdown-item',
                            exportOptions: {
                                columns: ':not(.not_include)'
                            }
                        },
                        {
                            extend: 'excel',
                            text: feather.icons['file'].toSvg({
                                class: 'font-small-4 me-50'
                            }) + 'Excel',
                            className: 'dropdown-item',
                            action: function(e, dt, button, config) {
                                var table = $('#dataTable').DataTable();
                                var header = [];
                                $(table.table().header()).find('th').each(function() {
                                    header.push($(this).text().trim());
                                });
                                var data = [];
                                table.rows().every(function(rowIdx, tableLoop, rowLoop) {
                                    var rowData = this.data();
                                    data.push(rowData);
                                });
                                var excelData = [header].concat(data);
                                var excelContent = excelData.map(function(row) {
                                    return row.join('\t');
                                }).join('\n');
                                var blob = new Blob([excelContent], {
                                    type: "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=utf-8"
                                });
                                saveAs(blob, 'Bank_Cash_Balances_Report.xls');
                            },
                            exportOptions: {
                                columns: ':not(.not_include)'
                            }
                        },
                        {
                            extend: 'pdf',
                            text: feather.icons['clipboard'].toSvg({
                                class: 'font-small-4 me-50'
                            }) + 'Pdf',
                            className: 'dropdown-item',
                            customize: function(doc) {
                                doc.content[1].table.body[0][0].forEach(function(cell) {
                                    cell.bold = true; // Set the first row to bold
                                });
                            },
                            exportOptions: {
                                columns: ':not(.not_include)'
                            }
                        },
                        {
                            extend: 'copy',
                            text: feather.icons['copy'].toSvg({
                                class: 'font-small-4 me-50'
                            }) + 'Copy',
                            className: 'dropdown-item',
                            exportOptions: {
                                columns: ':not(.not_include)'
                            }
                        }
                    ],
                    init: function(api, node, config) {
                        $(node).removeClass('btn-secondary');
                        $(node).parent().removeClass('btn-group');
                        setTimeout(function() {
                            $(node).closest('.dt-buttons').removeClass('btn-group')
                                .addClass('d-inline-flex');
                        }, 50);
                    }
                }],
                responsive: {
                    details: {
                        display: $.fn.dataTable.Responsive.display.childRowImmediate,
                        type: 'column',
                    }
                },
                language: {
                    paginate: {
                        previous: '&nbsp;',
                        next: '&nbsp;'
                    }
                },
            });

            $('div.head-label').html('<h3 class="mb-0">Bank & Cash Balances Report</h3>');
            $('.app')
                .attr('id', 'app-export');

        });
    </script>
@endsection
