@extends('admin.layouts.master')
@section('title', 'Edit User')
@section('content')
    <section class="content-wrapper container-xxl p-0">
        <div class="content-header row">
            <div class="content-header-left col-md-9 col-12 mb-2">
                <div class="row breadcrumbs-top">
                    <div class="col-12">
                        <h2 class="content-header-title float-start mb-0">User</h2>
                        <div class="breadcrumb-wrapper">
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="{{ url('/dashboard') }}">Dashboard</a>
                                </li>
                                <li class="breadcrumb-item active">Edit User
                                </li>
                            </ol>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="content-body">
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-body">
                            <form action="{{route('users.update',$user->id)}}" class="form" method="POST" enctype="multipart/form-data">
                                @csrf
                                @method('PUT')
                                <div class="row">
                                    <div class="col-md-6 col-12">
                                        <div class="mb-1">
                                            <label class="form-label" for="name">Name</label>
                                            <input type="text" name="name" id="name" class="form-control"
                                                placeholder="Full Name" value="{{$user->name}}" required />
                                            @error('name')
                                                <div class="text-danger">{{ $message }}</div>
                                            @enderror
                                        </div>
                                    </div>
                                    <div class="col-md-6 col-12">
                                        <div class="mb-1">
                                            <label class="form-label" for="email">E-Mail</label>
                                            <input type="email" name="email" id="email" class="form-control"
                                                placeholder="E-Mail" value="{{$user->email}}" required />
                                                @error('email')
                                                    <div class="text-danger">{{ $message }}</div>
                                                @enderror
                                        </div>
                                    </div>
                                    <div class="col-md-6 col-12">
                                        <div class="mb-1">
                                            <label for="profile_pic" class="form-label">Profile Photo</label>
                                            <input class="form-control" name="profile_pic" type="file" id="profile_pic">
                                        </div>
                                    </div>
                                    <div class="col-md-6 col-12">
                                        <div class="mb-1">
                                            <label class="form-label" for="role">Role</label>
                                            <select name="role" id="role" class="select2 form-select"
                                                data-placeholder="Select Role" required>
                                                <option value=""></option>
                                                @foreach ($roles as $role)
                                                    <option value="{{ $role->id }}" {{$role->id == $user_role->role_id ? 'selected' : ''}}>{{ $role->name }}</option>
                                                @endforeach
                                            </select>
                                            @error('role')
                                                <div class="text-danger">{{ $message }}</div>
                                            @enderror
                                        </div>
                                    </div>
                                    <div class="col-md-6 col-12">
                                        <div class="mb-1">
                                            <label class="form-label" for="fiscal_year">Financial Year</label>
                                            <select name="fiscal_years[]" id="fiscal_year" class="select2 form-select"
                                                data-placeholder="Select Financial Year" multiple required>
                                                <option value=""></option>
                                                @foreach ($financialyear as $year)
                                                    <option value="{{ $year->id }}" {{!empty($user->fiscal_years) ? (in_array($year->id,json_decode($user->fiscal_years)) ? 'selected' : '') : ''}}>{{ $year->fiscal_year }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-6 col-12">
                                        <div class="mb-1">
                                            <label class="form-label" for="location">Location</label>
                                            <select name="locations[]" id="location" class="select2 form-select"
                                                data-placeholder="Select Location" multiple required>
                                                <option value=""></option>
                                                @foreach ($location as $loc)
                                                    <option value="{{ $loc->id }}" {{!empty($user->locations) ? (in_array($loc->id,json_decode($user->locations)) ? 'selected' : '') : ''}}>{{ $loc->location_name }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                    @role('Super Admin')
                                    <div class="col-md-6 col-12">
                                        <div class="mb-1">
                                            <label class="form-label" for="edit_password">Password</label>
                                            <input type="password" id="edit_password" class="form-control @error('password') is-invalid @enderror" placeholder="Password" name="password" />
                                            <span class="text text-danger">Don't write, if you don't want to change</span>
                                        </div>
                                        @error('password')
                                            <div class="text-danger">{{ $message }}</div>
                                        @enderror
                                    </div>
                                    <div class="col-md-6 col-12">
                                        <div class="mb-1">
                                            <label class="form-label" for="password_confirmation">Confirm Password</label>
                                            <input type="password" id="password_confirmation" class="form-control @error('password_confirmation') is-invalid @enderror" placeholder="Confirm Password" name="password_confirmation" />
                                            <span class="text text-danger">Don't write, if you don't want to change</span>
                                        </div>
                                    </div>
                                    @endrole
                                    <div class="col-md-12">
                                        <hr>
                                        <h4>Voucher Types</h4>
                                        <hr>
                                        <div class="voucher_type" id="voucher_type">
                                            <div data-repeater-list="voucherTypes">
                                                @if (count($UserVoucherTypes) > 0)
                                                    @foreach ($UserVoucherTypes as $userType)
                                                    <div data-repeater-item>
                                                        <div class="row d-flex align-items-end">
                                                            <div class="col-md-3 col-12">
                                                                <div class="mb-1">
                                                                    <label class="form-label" for="voucher_type_id">Voucher
                                                                        Types</label>
                                                                    <select name="voucher_type" class="form-select select2"
                                                                        data-placeholder="Select Voucher Type" required>
                                                                        <option value=""></option>
                                                                        @foreach ($VoucherTypes as $type)
                                                                            <option value="{{ $type->id }}" {{$type->id == $userType->voucher_type_id ? 'selected' : ''}}>
                                                                                {{ $type->voucher_type }}</option>
                                                                        @endforeach
                                                                    </select>
                                                                </div>
                                                            </div>
                                                            <div class="col-md-2 col-12">
                                                                <div class="mb-1">
                                                                    <label class="form-label" for="type_limit">Limit</label>
                                                                    <input type="number" name="type_limit" class="form-control"
                                                                        placeholder="Limit" value="{{$userType->type_limit}}" required />
                                                                </div>
                                                            </div>
                                                            <div class="col-md-7 col-12">
                                                                <div class="mb-2">
                                                                    <div class="form-check-inline">
                                                                        <input class="form-check-input" type="checkbox"
                                                                            id="check1" name="permissions" value="Add" {{!empty($userType->permissions) ? (in_array('Add',json_decode($userType->permissions)) ? 'checked' : '') : ''}}>
                                                                        <label class="form-check-label">Add</label>
                                                                    </div>
                                                                    <div class="form-check-inline">
                                                                        <input class="form-check-input" type="checkbox"
                                                                            id="check1" name="permissions" value="Edit" {{!empty($userType->permissions) ? (in_array('Edit',json_decode($userType->permissions)) ? 'checked' : '') : ''}}>
                                                                        <label class="form-check-label">Edit</label>
                                                                    </div>
                                                                    <div class="form-check-inline">
                                                                        <input class="form-check-input" type="checkbox"
                                                                            id="check1" name="permissions" value="View" {{!empty($userType->permissions) ? (in_array('View',json_decode($userType->permissions)) ? 'checked' : '') : ''}}>
                                                                        <label class="form-check-label">View</label>
                                                                    </div>
                                                                    <div class="form-check-inline">
                                                                        <input class="form-check-input" type="checkbox"
                                                                            id="check1" name="permissions" value="Check" {{!empty($userType->permissions) ? (in_array('Check',json_decode($userType->permissions)) ? 'checked' : '') : ''}}>
                                                                        <label class="form-check-label">Check</label>
                                                                    </div>
                                                                    <div class="form-check-inline">
                                                                        <input class="form-check-input" type="checkbox"
                                                                            id="check1" name="permissions" value="Verify" {{!empty($userType->permissions) ? (in_array('Verify',json_decode($userType->permissions)) ? 'checked' : '') : ''}}>
                                                                        <label class="form-check-label">Verify</label>
                                                                    </div>
                                                                    <div class="form-check-inline">
                                                                        <input class="form-check-input" type="checkbox"
                                                                            id="check1" name="permissions" value="Approve" {{!empty($userType->permissions) ? (in_array('Approve',json_decode($userType->permissions)) ? 'checked' : '') : ''}}>
                                                                        <label class="form-check-label">Approve</label>
                                                                    </div>
                                                                    <div class="form-check-inline">
                                                                        <input class="form-check-input" type="checkbox"
                                                                            id="check1" name="permissions" value="Delete" {{!empty($userType->permissions) ? (in_array('Delete',json_decode($userType->permissions)) ? 'checked' : '') : ''}}>
                                                                        <label class="form-check-label">Delete</label>
                                                                    </div>
                                                                    <div class="form-check-inline pb-1">
                                                                        <input class="form-check-input" type="checkbox"
                                                                            id="check1" name="permissions" value="Print" {{!empty($userType->permissions) ? (in_array('Print',json_decode($userType->permissions)) ? 'checked' : '') : ''}}>
                                                                        <label class="form-check-label">Print</label>
                                                                    </div>
                                                                    <div class="form-check-inline">
                                                                        <input class="form-check-input" type="checkbox"
                                                                            id="check1" name="permissions" value="Tag" {{!empty($userType->permissions) ? (in_array('Tag',json_decode($userType->permissions)) ? 'checked' : '') : ''}}>
                                                                        <label class="form-check-label">Tag</label>
                                                                    </div>
                                                                    <div class="form-check-inline">
                                                                        <input class="form-check-input" type="checkbox"
                                                                            id="check1" name="permissions" value="qty&rate" {{!empty($userType->permissions) ? (in_array('qty&rate',json_decode($userType->permissions)) ? 'checked' : '') : ''}}>
                                                                        <label class="form-check-label">Qty & Rate</label>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="col-md-2 col-12 mb-51">
                                                                <div class="mb-1">
                                                                    <button class="btn btn-outline-danger text-nowrap px-1"
                                                                        data-repeater-delete type="button">
                                                                        <i data-feather="x" class="me-25"></i>
                                                                        <span>Delete</span>
                                                                    </button>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <hr />
                                                    </div>
                                                    @endforeach
                                                @else
                                                <div data-repeater-item>
                                                    <div class="row d-flex align-items-end">
                                                        <div class="col-md-4 col-12">
                                                            <div class="mb-1">
                                                                <label class="form-label" for="voucher_type_id">Voucher
                                                                    Types</label>
                                                                <select name="voucher_type" class="form-select select2"
                                                                    data-placeholder="Select Voucher Type" required>
                                                                    <option value=""></option>
                                                                    @foreach ($VoucherTypes as $type)
                                                                        <option value="{{ $type->id }}">
                                                                            {{ $type->voucher_type }}</option>
                                                                    @endforeach
                                                                </select>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-3 col-12">
                                                            <div class="mb-1">
                                                                <label class="form-label" for="type_limit">Limit</label>
                                                                <input type="number" name="type_limit" class="form-control"
                                                                    placeholder="Limit" required />
                                                            </div>
                                                        </div>
                                                        <div class="col-md-5 col-12">
                                                            <div class="mb-2">
                                                                <div class="form-check-inline">
                                                                    <input class="form-check-input" type="checkbox"
                                                                        id="check1" name="permissions" value="Add">
                                                                    <label class="form-check-label">Add</label>
                                                                </div>
                                                                <div class="form-check-inline">
                                                                    <input class="form-check-input" type="checkbox"
                                                                        id="check1" name="permissions" value="Edit">
                                                                    <label class="form-check-label">Edit</label>
                                                                </div>
                                                                <div class="form-check-inline">
                                                                    <input class="form-check-input" type="checkbox"
                                                                        id="check1" name="permissions" value="View">
                                                                    <label class="form-check-label">View</label>
                                                                </div>
                                                                <div class="form-check-inline">
                                                                    <input class="form-check-input" type="checkbox" name="permissions" value="Check">
                                                                    <label class="form-check-label">Check</label>
                                                                </div>
                                                                <div class="form-check-inline">
                                                                    <input class="form-check-input" type="checkbox"
                                                                        id="check1" name="permissions" value="Verify">
                                                                    <label class="form-check-label">Verify</label>
                                                                </div>
                                                                <div class="form-check-inline">
                                                                    <input class="form-check-input" type="checkbox" name="permissions" value="Approve">
                                                                    <label class="form-check-label">Approve</label>
                                                                </div>
                                                                <div class="form-check-inline">
                                                                    <input class="form-check-input" type="checkbox"
                                                                        id="check1" name="permissions" value="Delete">
                                                                    <label class="form-check-label">Delete</label>
                                                                </div>
                                                                <div class="form-check-inline pb-1">
                                                                    <input class="form-check-input" type="checkbox"
                                                                        id="check1" name="permissions" value="Print">
                                                                    <label class="form-check-label">Print</label>
                                                                </div>
                                                                <div class="form-check-inline">
                                                                    <input class="form-check-input" type="checkbox"
                                                                        id="check1" name="permissions" value="Tag">
                                                                    <label class="form-check-label">Tag</label>
                                                                </div>
                                                                <div class="form-check-inline">
                                                                    <input class="form-check-input" type="checkbox" name="permissions" value="qty&rate">
                                                                    <label class="form-check-label">Qty & Rate</label>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-2 col-12 mb-51">
                                                            <div class="mb-1">
                                                                <button class="btn btn-outline-danger text-nowrap px-1"
                                                                    data-repeater-delete type="button">
                                                                    <i data-feather="x" class="me-25"></i>
                                                                    <span>Delete</span>
                                                                </button>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <hr />
                                                </div>
                                                @endif
                                            </div>
                                            <div class="row">
                                                <div class="col-12">
                                                    <button class="btn btn-icon btn-primary" type="button"
                                                        data-repeater-create>
                                                        <i data-feather="plus" class="me-25"></i>
                                                        <span>Add New</span>
                                                    </button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row mb-1 p-2">
                                        <hr>
                                        <h4>Account Codes</h4>
                                        <hr>
                                        <div class="col-md-12 form-check mb-1">
                                            <input type="checkbox" class="form-check-input" name="check_all" id="check_all" value="check_all" />
                                            <label class="custom-control-label" for="check_all">Check all</label>
                                        </div>
                                        @foreach ($AccountCodes as $account_code)
                                            <div class="col-md-4 form-check mb-1 codes">
                                                <input type="checkbox" class="form-check-input" name="account_codes[]" id="{{$account_code->code}}" value="{{$account_code->id}}" {{!empty($user->account_codes) ? (in_array($account_code->id,json_decode($user->account_codes)) ? 'checked' : '') : ''}}/>
                                                <label class="custom-control-label" for="{{$account_code->code}}">{{$account_code->code}}</label>
                                            </div>
                                        @endforeach
                                    </div>
                                    <div class="d-flex justify-content-end">
                                        <a href="{{ route('users.index') }}" class="btn btn-outline-danger">Cancel</a>
                                        <button type="submit" class="form_save btn btn-primary ms-1">Update</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
@section('scripts')
    <script>
        $(document).ready(function() {
            var calculation_process = $('#voucher_type');
            $('.voucher_type').repeater({
                isFirstItemUndeletable: true,
                // initEmpty: true,
                show: function() {
                    $(this).slideDown();
                    calculation_process.find('select').next('.select2-container').remove();
                    calculation_process.find('select').select2();
                    // Feather Icons
                    if (feather) {
                        feather.replace({
                            width: 14,
                            height: 14
                        });
                    }
                },
                hide: function(deleteElement) {
                    $(this).slideUp(deleteElement);
                }
            });

            $("#check_all").on("change", function(){
                if($(this).is(":checked")){
                    $(".codes input[type='checkbox']").prop('checked', true);  
                }
                else{
                    $(".codes input[type='checkbox']").prop('checked', false);
                }
            });
        })
    </script>
@endsection
