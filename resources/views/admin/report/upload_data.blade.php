@extends('admin.layouts.master')
@section('title', 'Upload File')
@section('content')
<section class="content-wrapper container-xxl p-0">
    <div class="content-header row">
        <div class="content-header-left col-md-9 col-12 mb-2">
            <div class="row breadcrumbs-top">
                <div class="col-12">
                    <h2 class="content-header-title float-start mb-0">Upload Filer</h2>
                    <div class="breadcrumb-wrapper">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="{{ url('/dashboard') }}">Dashboard</a>
                            </li>
                            <li class="breadcrumb-item"><a href="{{ url('/vouchers') }}">Upload File</a>
                            </li>
                            <li class="breadcrumb-item active"> Upload File
                            </li>
                        </ol>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="content-body">
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-header">
                        <h4 class="card-title">Upload File</h4>
                    </div>
                    <div class="card-body">
                        <form class="form"  action="{{url('file_upload')}}" method="POST" id="add_voucher" enctype="multipart/form-data">
                            @csrf
                            <div class="row">
                                <div class="col-md-4 col-12">
                                    <div class="mb-1">
                                        <label class="form-label" for="voucher_type">Upload File</label>
                                        <input type="file" name="file" id="file" class="form-control" >
                                    </div>
                                </div>
                                    <div class="d-flex justify-content-end">
                                        <a href="{{ route('vouchers.index') }}" class="btn btn-outline-danger">Cancel</a>
                                        <button type="submit" class="form_save btn btn-primary ms-1"
                                            id="save">Save</button>
                                    </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-header">
                        <h4 class="card-title">Upload For Account Table</h4>
                    </div>
                    <div class="card-body">
                        <form class="form"  action="{{url('file_upload_for_accounts')}}" method="POST" id="add_voucher" enctype="multipart/form-data">
                            @csrf
                            <div class="row">
                                <div class="col-md-4 col-12">
                                    <div class="mb-1">
                                        <label class="form-label" for="voucher_type">Upload File</label>
                                        <input type="file" name="file" id="file" class="form-control" >
                                    </div>
                                </div>
                                    <div class="d-flex justify-content-end">
                                        <a href="{{ route('vouchers.index') }}" class="btn btn-outline-danger">Cancel</a>
                                        <button type="submit" class="form_save btn btn-primary ms-1"
                                            id="save">Save</button>
                                    </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection