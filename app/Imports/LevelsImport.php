<?php

namespace App\Imports;

use App\Models\Level1;
use App\Models\Level2;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithHeadingRow;

class LevelsImport implements ToModel,WithHeadingRow
{
    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */
    public  $privious=0;
    public function model(array $row)
    {
        if(!empty($row['code'])){
            if( $this->privious != $row['code']){
              $this->privious = $row['code'];
           $code=explode('/',$row['code']);
           $levl2code=$code[0];
            $id=Level1::where('code', $levl2code)->first();
            if(!empty($id->id)){
                $level_2 = new Level2();
                $level_2->code =$code[0]."-".$code[1];
                $level_2->description = $row['description'];
                $level_2->level_1_code=$code[0];
                $level_2->level_1_id=$id->id;
                $level_2->save();
                return $level_2;
            }
        // else{
            // if($row['level1']){
            //     if( $this->privious != $row['level1']){
            //         $this->privious = $row['level1'];
            //     $level1 = new Level1();
            //     $level1->code = $row['level1'];
            //     $level1->description = $row['description'];
            //     $level1->save();
            //     return $level1;

            // }
         
     

}
}
}
}