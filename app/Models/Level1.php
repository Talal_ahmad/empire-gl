<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\Traits\LogsActivity;
use Spatie\Activitylog\LogOptions;

class Level1 extends Model
{
    use HasFactory,LogsActivity;
    protected $table = 'level_1';

    public function getActivitylogOptions(): LogOptions
    {
        $login_user = \Auth::user()->name;
        return LogOptions::defaults()
        ->logOnly(['code','description'])
        ->useLogName('Level1')
        ->logOnlyDirty()
        ->setDescriptionForEvent(fn(string $eventName) => "{$login_user} have {$eventName} Level1");
    }

    public function level2()
    {
        return $this->hasMany(Level2::class,'level_1_id');
    }

    public function accountclass()
    {
        return $this->belongsTo(AccountClass::class,'account_class_id');
    }
}
