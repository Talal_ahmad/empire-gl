<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\Traits\LogsActivity;
use Spatie\Activitylog\LogOptions;

class VoucherDetail extends Model
{
    use HasFactory,LogsActivity;
    protected $table = 'gl_trans_details';

    public function getActivitylogOptions(): LogOptions
    {
        $login_user = \Auth::user()->name;
        return LogOptions::defaults()
        ->logOnly(['gl_trans_id','voucher_no','ent','date','tag','account.code','account.name','account.gl_type','qty','rate','amount','line_remarks'])
        ->useLogName('Voucher Entry')
        ->logOnlyDirty()
        ->setDescriptionForEvent(fn(string $eventName) => "{$login_user} have {$eventName} Voucher Entry");
    }

    public function account()
    {
        return $this->belongsTo(Account::class)->select(['id', 'code','name','gl_type','level_2_id','description']);
    }

    public function voucher()
    {
        return $this->belongsTo(Voucher::class,'gl_trans_id');
    }

    public function vouchertype()
    {
        return $this->belongsTo(Vouchertype::class,'voucher_type')->select(['id', 'voucher_type']);
    }

    public function location()
    {
        return $this->belongsTo(Location::class)->select(['id', 'location_name','short_name']);
    }
}
