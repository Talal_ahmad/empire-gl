<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\Traits\LogsActivity;
use Spatie\Activitylog\LogOptions;

class Voucher extends Model
{
    use HasFactory,LogsActivity;
    protected $table = 'gl_trans';

    public function getActivitylogOptions(): LogOptions
    {
        $login_user = \Auth::user()->name;
        return LogOptions::defaults()
        ->logOnly(['voucher_no','vouchertype.voucher_type','bill_no','date','status','check_remarks','varify_remarks','approve_remarks','financialyear.fiscal_year','location.location_name','type_limit','debit_amount','credit_amount','igp_number','ogp_number','igp_ogp_date','cheque_no','cheque_date','attachments','general_remarks'])
        ->useLogName('Voucher')
        // ->logOnlyDirty()
        ->setDescriptionForEvent(fn(string $eventName) => "{$login_user} have {$eventName} Voucher");
    }

    // public function tapActivity(Activity $activity, string $eventName)
    // {
    //     $activity->properties = $activity->properties->put('voucher_type', $this->accountdetails);
    // }

    public function voucherdetails()
    {
        return $this->hasMany(VoucherDetail::class,'gl_trans_id');
    }

    public function vouchertype()
    {
        return $this->belongsTo(Vouchertype::class,'voucher_type')->select(['id', 'voucher_type']);
    }

    public function location()
    {
        return $this->belongsTo(Location::class)->select(['id', 'location_name']);
    }

    public function bankcash()
    {
        return $this->belongsTo(BankCash::class,'bank_cash_id')->select(['id', 'name']);
    }

    public function financialyear()
    {
        return $this->belongsTo(Financialyear::class,'fiscal_year')->select(['id', 'fiscal_year']);
    }

    public function customer()
    {
        return $this->belongsTo(Customer::class)->select(['id', 'name']);
    }

    public function supplier()
    {
        return $this->belongsTo(Supplier::class)->select(['id', 'name']);
    }
}
