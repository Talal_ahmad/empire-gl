<?php

namespace App\Http\Controllers\Api;

use App\Models\Account;
use App\Models\Supplier;
use App\Models\Customer;
use App\Models\BankCash;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ApiController extends Controller
{
    public function updateAccountCode()
    {
        $accounts = Account::get(['id','code','level2_code','account_code','gl_type','name','description']);
        foreach ($accounts as $key => $account) {
            $codeParts = explode('-',$account->code);
            if(array_key_exists(2,$codeParts) && !empty($codeParts[2])){
                $account->account_code = $codeParts[2];
            }
            if($account->gl_type == 'supplier' || $account->gl_type == 'Supplier'){
                $account->gl_type = 'Supplier';
                $accountSupplier = Supplier::where('account_id', $account->id)->first();
                if(empty($accountSupplier)){
                    $account->name = $account->description;
                    $supplier = new Supplier();
                    $supplier->account_id = $account->id;
                    $supplier->name = $account->description;
                    $supplier->save();
                }
            }
            if ($account->gl_type == 'customer' || $account->gl_type == 'Customers') {
                $account->gl_type = 'Customer';
                $accountCustomer = Customer::where('account_id', $account->id)->first();
                if(empty($accountCustomer)){
                    $account->name = $account->description;
                    $customer = new Customer();
                    $customer->account_id = $account->id;
                    $customer->name = $account->description;
                    $customer->save();
                }
            }
            if($account->gl_type == 'Bank' || $account->gl_type == 'bank' || $account->gl_type == 'Cash/Bank'){
                $account->gl_type = 'Bank';
                $BankCash = BankCash::where('account_id', $account->id)->first();
                if(empty($BankCash)){
                    $account->name = $account->description;
                    $bank = new BankCash();
                    $bank->account_id = $account->id;
                    $bank->name = $account->description;
                    $bank->save();
                }
            }
            if($account->gl_type == 'Cost_of_sales' || $account->gl_type == 'Cost of Sales'){
                $account->gl_type = 'Cost of Sales';
            }
            $account->update();
        }
        return ['code' => 200, 'message' => 'Success'];
    }
}
