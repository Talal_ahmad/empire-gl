<?php

namespace App\Http\Controllers;

use Auth;
use DataTables;
use Carbon\Carbon;
use App\Models\Voucher;
use App\Models\Location;
use App\Models\Vouchertype;
use App\Models\Account;
use App\Models\Customer;
use App\Models\Supplier;
use App\Models\BankCash;
use App\Models\VoucherDetail;
use App\Models\Financialyear;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Spatie\Activitylog\Facades\LogBatch;
use Spatie\Activitylog\Models\Activity;
use Illuminate\Validation\ValidationException;

class VoucherController extends Controller
{
    public function __construct(){
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if($request->ajax()){
            $data = DB::table('gl_trans')->join('vouhertypes','vouhertypes.id', '=','gl_trans.voucher_type')
            ->join('locations','locations.id', '=','gl_trans.location_id');
            if(!Auth::user()->hasRole('Super Admin')){
                $data->whereIn('gl_trans.voucher_type', loginUserVoucherTypes());
            }
            $data = $data->select('gl_trans.id','gl_trans.voucher_type','gl_trans.voucher_no','gl_trans.bill_no','gl_trans.date','gl_trans.status','gl_trans.debit_amount','gl_trans.general_remarks','gl_trans.check_remarks','gl_trans.varify_remarks','gl_trans.approve_remarks','vouhertypes.voucher_type as vocherType','locations.location_name as location');
            return DataTables::of($data)
            ->addIndexColumn()
            ->addColumn('action', function($row){
                $btn = '';
                if(in_array("View",UserVoucherTypePermissions($row->voucher_type)['permissions'])){
                    $btn .= '<a href="vouchers/'.$row->id.'" class="text-info font-medium-3 me-75" title="View Voucher"><i class="fas fa-eye"></i></a>';
                }
                if($row->status < 4){
                    if(in_array("Edit",UserVoucherTypePermissions($row->voucher_type)['permissions'])){
                        $btn .= '<a href="vouchers/'.$row->id.'/edit" class="item-edit font-medium-3 me-75" title="Edit Voucher"><i class="far fa-edit"></i></a>';
                    }
                    if (in_array("Delete", UserVoucherTypePermissions($row->voucher_type)['permissions'])) {
                        $btn .= '<a href="javascript:;" class="text-danger font-medium-3 me-75" title="Delete Voucher" onclick="deleteVoucher('.$row->id.')"><i class="fas fa-trash"></i></a>';
                    }
                    if(in_array("Check",UserVoucherTypePermissions($row->voucher_type)['permissions']) && $row->status == 1){
                        $btn .= '<a href="vouchers/'.$row->id.'/edit?type=Check" class="text-secondary font-medium-3 me-75" title="Check Voucher"><i class="fas fa-check"></i></a>';
                    }
                    if(in_array("Verify",UserVoucherTypePermissions($row->voucher_type)['permissions']) && $row->status == 2){
                        $btn .= '<a href="vouchers/'.$row->id.'/edit?type=Verify" class="item-edit font-medium-3 me-75" title="Verify Voucher"><i class="fas fa-check-circle"></i></a>';
                    }
                    if(in_array("Approve",UserVoucherTypePermissions($row->voucher_type)['permissions']) && $row->status == 3){
                        $btn .= '<a href="vouchers/'.$row->id.'/edit?type=Approve" class="text-success font-medium-3" title="Approve Voucher"><i class="fas fa-user-check"></i></a>';
                    }
                }
                return $btn;
            })
            ->rawColumns(['action'])
            ->make(true);
        }
        return view('admin.add_voucher.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $Vouchertypes = Vouchertype::join('user_voucher_types', 'user_voucher_types.voucher_type_id', '=','vouhertypes.id')->where('user_id', loginUserId())->whereJsonContains('user_voucher_types.permissions', ['Add'])->select('vouhertypes.id', 'vouhertypes.voucher_type')->get();
        $locations = Location::whereIn('id', loginUserLocations())->get();
        $customers = Customer::get(['id', 'name']);
        $suppliers = Supplier::get(['id', 'name']);
        $bankcash = BankCash::get(['id', 'name']);
        return view('admin.add_voucher.create',get_defined_vars());
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // dd($request);
        try{
            $this->validate($request, [
                'voucher_type' => 'required',
                'date' => 'required',
                'location' => 'required',
                'attachments.*' => 'mimes:jpeg,png,jpg,pdf',
                ],
                ['attachments.*.mimes' => 'The attachments must be a image or pdf.']
            );
            if(!Auth::user()->can('Date validation')){
                if($request->date < date('Y-m-d H:i:s')){
                return ['code'=>'400','customMessage'=>'Date could not be less then current date!'];     }
            }
            if($request->date > date('Y-m-d H:i:s')){
                return ['code'=>'400','customMessage'=>'Date is greater than current date!'];    
             }
            $lastVoucherMonth = Voucher::where('voucher_type', $request->voucher_type)->whereYear('date', date('Y',strtotime($request->date)))->whereMonth('date', date('m',strtotime($request->date)))->orderBy('date','DESC')->first(['id', 'date']);
            if( !empty($lastVoucherMonth) && ($request->date <  $lastVoucherMonth->date)){
                return ['code'=>'400','customMessage'=>'Date is less than last voucher date!'];   

            }
            // if(!empty($lastVoucherMonth) && ($request->date < $lastVoucherMonth->date)){
            //     return ['code'=>'300','customMessage'=>'Voucher Date is Already exist!'];
            // }
            // dd($lastVoucherMonth);
            if(round($request->debit_amount,2) != round($request->credit_amount,2)){
                return ['code'=>'500','customMessage'=>'Sum of Debit and Credit amount must be equal!'];
            }
            $voucherTypeLimit = DB::table('user_voucher_types')->where('user_id',loginUserId())->where('voucher_type_id',$request->voucher_type)->value('type_limit');
            if(round($request->debit_amount,2) > $voucherTypeLimit){
                return ['code'=>'500','customMessage'=>'Your voucher limit excedeed!'];
            }
            $fiscal_year = Financialyear::where([
                    ['open_close','=','Open'],
                    ['status','=','Active'],
                    ['start_date','<=',$request->date],
                    ['end_date','>=',$request->date],
                ])->first(['id']);
            if(empty($fiscal_year)){
                return ['code'=>'500','customMessage'=>'Date does not match with fiscal year!'];
            }
            DB::transaction(function () use ($request,$fiscal_year) {
                $voucherNo = Voucher::where('voucher_type',$request->voucher_type)->whereYear('date',date('Y',strtotime($request->date)))->whereMonth('date',date('m',strtotime($request->date)))->orderBy('id', 'DESC')->first(['id','voucher_no']);
                // dd($voucherNo)
                if(!empty($voucherNo)){
                    $voucher_no = $voucherNo->voucher_no+1;
                }
                else{
                    $voucher_no = 1;
                }
                LogBatch::withinBatch(function(string $uuid) use ($request,$fiscal_year,$voucher_no) {
                    $voucher = new Voucher();
                    $voucher->voucher_type = $request->voucher_type;
                    $voucher->voucher_no = $voucher_no;
                    $voucher->date = $request->date;
                    $voucher->bill_no = $request->bill_no;
                    $voucher->status = 1;
                    $voucher->fiscal_year = $fiscal_year->id;
                    $voucher->location_id = $request->location;
                    if(isset($request->customer)){
                        $voucher->customer_id = $request->customer;
                    }
                    if(isset($request->supplier)){
                        $voucher->supplier_id = $request->supplier;
                    }
                    if(isset($request->bank)){
                        $voucher->bank_cash_id = $request->bank;
                        $voucher->cheque_no = $request->cheque_no;
                        $voucher->cheque_date = $request->cheque_date;
                    }
                    if(isset($request->attachments)){
                        $attachments = InsertMultipleFiles($request->attachments,'/voucher_attachments');
                        $voucher->attachments = json_encode($attachments);
                    }
                    $voucher->type_limit = $request->type_limit;
                    $voucher->debit_amount = $request->debit_amount;
                    $voucher->credit_amount = $request->credit_amount;
                    $voucher->igp_number = $request->igp_number;
                    $voucher->ogp_number	 = $request->ogp_number;
                    $voucher->igp_ogp_date = $request->igp_ogp_date;
                    $voucher->general_remarks = $request->general_remarks;
                    $voucher->created_by = Auth::user()->id;
                    $voucher->save();
                    $total_amount = 0;
                    $total_debit_amount = 0;
                    $total_credit_amount = 0;
                    $tag = 0;
                    foreach ($request->voucherEntry as $key => $entry) {
                        $tagAccount = Account::where('id',$entry['account_code'])->where('tag',1)->first(['id']);
                        if(!empty($tagAccount)){
                            $tag = 1;
                        }
                        $voucherDetail = new VoucherDetail();
                        $voucherDetail->gl_trans_id = $voucher->id;
                        $voucherDetail->voucher_type = $request->voucher_type;
                        $voucherDetail->voucher_no = $voucher_no;
                        $voucherDetail->date = $request->date;
                        $voucherDetail->ent = $entry['ent'];
                        $voucherDetail->account_id = $entry['account_code'];
                        $voucherDetail->qty = isset($entry['qty']) ? $entry['qty'] : NULL;
                        $voucherDetail->rate = isset($entry['rate']) ? $entry['rate'] : NULL;
                        $voucherDetail->tag = $tag;
                        if(!empty(cleanAmount($entry['credit']))){
                            $amount = -cleanAmount($entry['credit']);
                        }
                        else{
                            $amount = cleanAmount($entry['debit']);
                        }
                        if(isset($request->customer)){
                            $voucherDetail->customer_id = $request->customer;
                            $CustomerAccount = Account::where('id',$entry['account_code'])->where('gl_type', 'Customer')->first(['id']);
                            if(!empty($CustomerAccount)){
                                if($amount > 0){
                                    $total_debit_amount += $amount;
                                }
                                else{
                                    $total_credit_amount += abs($amount);
                                }
                                $total_amount += $amount;
                            }
                        }
                        if(isset($request->supplier)){
                            $voucherDetail->supplier_id = $request->supplier;
                            $SupplierAccount = Account::where('id',$entry['account_code'])->where('gl_type', 'Supplier')->first(['id']);
                            if(!empty($SupplierAccount)){
                                if($amount > 0){
                                    $total_debit_amount += $amount;
                                }
                                else{
                                    $total_credit_amount += abs($amount);
                                }
                                $total_amount += $amount;
                            }
                        }
                        if(isset($request->bank)){
                            $voucherDetail->bank_cash_id = $request->bank;
                            $bankAccount = Account::where('id',$entry['account_code'])->where('gl_type', 'Bank')->first(['id']);
                            if(!empty($bankAccount)){
                                if($amount > 0){
                                    $total_debit_amount += $amount;
                                }
                                else{
                                    $total_credit_amount += abs($amount);
                                }
                                $total_amount += $amount;
                            }
                        }
                        $voucherDetail->amount = $amount;
                        $voucherDetail->location_id = $request->location;
                        $voucherDetail->line_remarks = $entry['description'];
                        $voucherDetail->save();
                    }
                    if($tag == 1){
                        DB::table('gl_trans_details')->where('gl_trans_id', $voucher->id)->update(['tag' => $tag]);
                    }
                    if(isset($request->bank)){
                        DB::table('bank_trans')->insert([
                            'gl_trans_id' => $voucher->id,
                            'bank_id' => $request->bank,
                            'voucher_type' => $voucher->voucher_type,
                            'voucher_no' => $voucher->voucher_no,
                            'bill_no' => $voucher->bill_no,
                            'date' => $voucher->date,
                            'cheque_no' => $request->cheque_no,
                            'cheque_date' => $request->cheque_date,
                            'total_amount' => $total_amount,
                            'debit_amount' => $total_debit_amount,
                            'credit_amount' => $total_credit_amount
                        ]);
                    }
                    if(isset($request->customer)){
                        DB::table('debtor_trans')->insert([
                            'gl_trans_id' => $voucher->id,
                            'customer_id' => $request->customer,
                            'voucher_type' => $voucher->voucher_type,
                            'voucher_no' => $voucher->voucher_no,
                            'bill_no' => $voucher->bill_no,
                            'date' => $voucher->date,
                            'total_amount' => $total_amount,
                            'debit_amount' => $total_debit_amount,
                            'credit_amount' => $total_credit_amount
                        ]);
                    }
                    if(isset($request->supplier)){
                        DB::table('supplier_trans')->insert([
                            'gl_trans_id' => $voucher->id,
                            'supplier_id' => $request->supplier,
                            'voucher_type' => $voucher->voucher_type,
                            'voucher_no' => $voucher->voucher_no,
                            'bill_no' => $voucher->bill_no,
                            'date' => $voucher->date,
                            'total_amount' => $total_amount,
                            'debit_amount' => $total_debit_amount,
                            'credit_amount' => $total_credit_amount
                        ]);
                    }
                });
            });
            return ['code'=>'200','message'=>'success'];
        }
        catch(\Exception | ValidationException $e){
            if($e instanceof ValidationException){
                return ['code'=>'422','errors' => $e->errors()];
            }
            else{
                return ['code'=>'500','error_message'=>$e->getMessage()];
            }
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $voucher = Voucher::with(['vouchertype','location','bankcash','financialyear','customer','supplier'])->find($id);
        $voucher_details = VoucherDetail::with('account')->where('gl_trans_id', $id)->get()->map(function($detail,$key) {
            if ($detail->amount < 0) {
                $detail->credit = abs($detail->amount);
            }
            else{
                $detail->debit = $detail->amount;
            }
            return $detail;
        });
        return view('admin.add_voucher.view',compact('voucher','voucher_details'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    { 
        // dd($id);
        $voucher = Voucher::find($id);
        $voucher_details = VoucherDetail::with('account')->where('gl_trans_id', $id)->get()->map(function($detail,$key) {
            if ($detail->amount < 0) {
                $detail->credit = abs($detail->amount);
            }
            else{
                $detail->debit = $detail->amount;
            }
            return $detail;
        });
        $Vouchertypes = Vouchertype::join('user_voucher_types', 'user_voucher_types.voucher_type_id', '=','vouhertypes.id')->where('user_id', loginUserId())->whereJsonContains('user_voucher_types.permissions', ['Add'])->select('vouhertypes.id', 'vouhertypes.voucher_type')->get();
        // $accounts = Account::whereIn('level_2_id',loginUserAccountCodes())->get(['id','code','description','name','gl_type']);
        $locations = Location::whereIn('id', loginUserLocations())->get();
        $customers = Customer::get(['id', 'name']);
        $suppliers = Supplier::get(['id', 'name']);
        $bankcash = BankCash::get(['id', 'name']);
        return view('admin.add_voucher.edit',get_defined_vars());
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        try{
            $this->validate($request, [
                'date' => 'required',
                'location' => 'required',
                'attachments.*' => 'mimes:jpeg,png,jpg,pdf',
                ],
                ['attachments.*.mimes' => 'The attachments must be a image or pdf.']
            );
            if(round($request->debit_amount,2) != round($request->credit_amount,2)){
                return ['code'=>'500','customMessage'=>'Sum of Debit and Credit amount must be equal!'];
            }
            $voucherTypeLimit = DB::table('user_voucher_types')->where('user_id',loginUserId())->where('voucher_type_id',$request->voucher_type)->value('type_limit');
            if(round($request->debit_amount,2) > $voucherTypeLimit){
                return ['code'=>'500','customMessage'=>'Your voucher limit excedeed!'];
            }
            $fiscal_year = Financialyear::where([
                    ['open_close','=','Open'],
                    ['status','=','Active'],
                    ['start_date','<=',$request->date],
                    ['end_date','>=',$request->date],
                ])->first(['id']);
            if(empty($fiscal_year)){
                return ['code'=>'500','customMessage'=>'Date does not match with fiscal year!'];
            }
            DB::transaction(function () use ($request,$id,$fiscal_year) {
                $voucher = Voucher::find($id);
                if($voucher->date != $request->date){
                    $voucherNo = Voucher::where('voucher_type',$request->voucher_type)->whereYear('date',date('Y',strtotime($request->date)))->whereMonth('date',date('m',strtotime($request->date)))->orderBy('id', 'DESC')->first(['id','voucher_no']);
                    if(!empty($voucherNo)){
                        $voucher_no = $voucher->voucher_no;
                        if($voucher->voucher_no != $voucherNo->voucher_no){
                            $voucher_no = $voucherNo->voucher_no+1;
                        }
                    }
                    else{
                        $voucher_no = 1;
                    }
                    $voucher->voucher_no = $voucher_no;
                }
                LogBatch::withinBatch(function(string $uuid) use ($request,$id,$fiscal_year,$voucher) {
                    $voucher->date = $request->date;
                    $voucher->bill_no = $request->bill_no;
                    if(isset($request->type) && $request->type == 'Check'){
                        $voucher->status = 2;
                        $voucher->check_remarks = $request->remarks;
                    }
                    if(isset($request->type) && $request->type == 'Verify'){
                        $voucher->status = 3;
                        $voucher->varify_remarks = $request->remarks;
                    }
                    if(isset($request->type) && $request->type == 'Approve'){
                        $voucher->status = 4;
                        $voucher->approve_remarks = $request->remarks;
                    }
                    $voucher->fiscal_year = $fiscal_year->id;
                    $voucher->location_id = $request->location;
                    if(isset($request->attachments)){
                        if(!empty($voucher->attachments)){
                            foreach (json_decode($voucher->attachments) as $key => $attachment) {
                                unlink(public_path().$attachment);
                            }
                        }
                        $attachments = InsertMultipleFiles($request->attachments,'/voucher_attachments');
                        $voucher->attachments = json_encode($attachments);
                    }
                    if(isset($request->bank)){
                        $voucher->cheque_no = $request->cheque_no;
                        $voucher->cheque_date = $request->cheque_date;
                    }
                    $voucher->type_limit = $request->type_limit;
                    $voucher->debit_amount = $request->debit_amount;
                    $voucher->credit_amount = $request->credit_amount;
                    $voucher->igp_number = $request->igp_number;
                    $voucher->ogp_number	 = $request->ogp_number;
                    $voucher->igp_ogp_date = $request->igp_ogp_date;
                    $voucher->general_remarks = $request->general_remarks;
                    $voucher->edit_by = Auth::user()->id;
                    $voucher->edited_at = Carbon::now();
                    $voucher->update();
                    $total_amount = 0;
                    $total_debit_amount = 0;
                    $total_credit_amount = 0;
                    $tag = 0;
                    if(isset($request->voucherEntry) && count($request->voucherEntry) > 0){
                        $VoucherDetails = VoucherDetail::where('gl_trans_id', $id)->get();
                        // $VoucherDetails = VoucherDetail::where('gl_trans_id', $id)->delete();
                        foreach ($VoucherDetails as $key => $detail) {
                            $detail->delete();
                        }
                        foreach ($request->voucherEntry as $key => $entry) {
                            $tagAccount = Account::where('id',$entry['account_code'])->where('tag',1)->first(['id']);
                            if(!empty($tagAccount)){
                                $tag = 1;
                            }
                            $voucherDetail = new VoucherDetail();
                            $voucherDetail->gl_trans_id = $id;
                            $voucherDetail->voucher_type = $voucher->voucher_type;
                            $voucherDetail->voucher_no = $voucher->voucher_no;
                            $voucherDetail->date = $request->date;
                            $voucherDetail->ent = $entry['ent'];
                            $voucherDetail->account_id = $entry['account_code'];
                            $voucherDetail->qty = isset($entry['qty']) ? cleanAmount($entry['qty']) : NULL;
                            $voucherDetail->rate = isset($entry['rate']) ? cleanAmount($entry['rate']) : NULL;
                            if(cleanAmount($entry['credit']) > 0){
                                $amount = -cleanAmount($entry['credit']);
                            }
                            else{
                                $amount = cleanAmount($entry['debit']);
                            }
                            if(isset($request->customer)){
                                $voucherDetail->customer_id = $request->customer;
                                $CustomerAccount = Account::where('id',$entry['account_code'])->where('gl_type', 'Customer')->first(['id']);
                                if(!empty($CustomerAccount)){
                                    if($amount > 0){
                                        $total_debit_amount += $amount;
                                    }
                                    else{
                                        $total_credit_amount += abs($amount);
                                    }
                                    $total_amount += $amount;
                                }
                            }
                            if(isset($request->supplier)){
                                $voucherDetail->supplier_id = $request->supplier;
                                $SupplierAccount = Account::where('id',$entry['account_code'])->where('gl_type', 'Supplier')->first(['id']);
                                if(!empty($SupplierAccount)){
                                    if($amount > 0){
                                        $total_debit_amount += $amount;
                                    }
                                    else{
                                        $total_credit_amount += abs($amount);
                                    }
                                    $total_amount += $amount;
                                }
                            }
                            if(isset($request->bank)){
                                $voucherDetail->bank_cash_id = $request->bank;
                                $bankAccount = Account::where('id',$entry['account_code'])->where('gl_type', 'Bank')->first(['id']);
                                if(!empty($bankAccount)){
                                    if($amount > 0){
                                        $total_debit_amount += $amount;
                                    }
                                    else{
                                        $total_credit_amount += abs($amount);
                                    }
                                    $total_amount += $amount;
                                }
                            }
                            $voucherDetail->amount = $amount;
                            $voucherDetail->location_id = $request->location;
                            $voucherDetail->line_remarks = $entry['description'];
                            $voucherDetail->save();
                        }
                    }
                    if($tag == 1){
                        DB::table('gl_trans_details')->where('gl_trans_id', $voucher->id)->update(['tag' => $tag]);
                    }
                    if(isset($request->bank)){
                        DB::table('bank_trans')->where('bank_id', $request->bank)->update([
                            'bill_no' => $voucher->bill_no,
                            'voucher_no' => $voucher->voucher_no,
                            'date' => $voucher->date,
                            'cheque_no' => $request->cheque_no,
                            'cheque_date' => $request->cheque_date,
                            'total_amount' => $total_amount,
                            'debit_amount' => $total_debit_amount,
                            'credit_amount' => $total_credit_amount
                        ]);
                    }
                    if(isset($request->customer)){
                        DB::table('debtor_trans')->where('customer_id', $request->customer)->update([
                            'bill_no' => $voucher->bill_no,
                            'voucher_no' => $voucher->voucher_no,
                            'date' => $voucher->date,
                            'total_amount' => $total_amount,
                            'debit_amount' => $total_debit_amount,
                            'credit_amount' => $total_credit_amount
                        ]);
                    }
                    if(isset($request->supplier)){
                        DB::table('supplier_trans')->where('supplier_id', $request->supplier)->update([
                            'bill_no' => $voucher->bill_no,
                            'voucher_no' => $voucher->voucher_no,
                            'date' => $voucher->date,
                            'total_amount' => $total_amount,
                            'debit_amount' => $total_debit_amount,
                            'credit_amount' => $total_credit_amount
                        ]);
                    }
                });
            });
            return ['code'=>'200','message'=>'success'];
        }
        catch(\Exception | ValidationException $e){
            if($e instanceof ValidationException){
                return ['code'=>'422','errors' => $e->errors()];
            }
            else{
                return ['code'=>'500','error_message'=>$e->getMessage()];
            }
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        DB::table('bank_trans')->where('gl_trans_id',$id)->delete();
        DB::table('supplier_trans')->where('gl_trans_id',$id)->delete();
        LogBatch::withinBatch(function(string $uuid) use ($id) {
            Voucher::find($id)->delete();
            $VoucherDetails = VoucherDetail::where('gl_trans_id', $id)->get();
            foreach ($VoucherDetails as $key => $detail) {
                $detail->delete();
            }
        });
    }
    public function VoucherStatus(Request $request,$id)
    {
        try{
            $voucher = Voucher::find($id);
            if($request->status == 'Checked'){
                $voucher->status = 2;
                $voucher->checked_by = Auth::user()->id;
                $voucher->checked_at = Carbon::now();
            }
            elseif ($request->status == 'Verified') {
                $voucher->status = 3;
                $voucher->verified_by = Auth::user()->id;
                $voucher->verified_at = Carbon::now();
            }
            else{
                $voucher->status = 4;
                $voucher->approved_by = Auth::user()->id;
                $voucher->approved_at = Carbon::now();
            }
            $voucher->update();
            return ['code'=>'200','message'=>'success'];
        }
        catch(\Exception | ValidationException $e){
            return ['code'=>'500','error_message'=>$e->getMessage()];
        }
    }

    public function voucherLogs()
    {
        if(request()->ajax()){
            return DataTables::of(Activity::inLog('Voucher')->latest('created_at'))
            ->addIndexColumn()
            ->addColumn('date', function($row){
                return date('Y-m-d',strtotime($row->created_at));
            })
            ->addColumn('voucherType', function($row){
                if ($row->event == 'created' || $row->event == 'updated') {
                    return $row->properties['attributes']['vouchertype.voucher_type'];
                }
                else{
                    return $row->properties['old']['vouchertype.voucher_type'];
                }
            })
            ->addColumn('performedBy', function($row){
                return userName($row->causer_id);
            })
            ->addColumn('action', function($row){
                $btn = '';
                $btn .= '<a href="histryDetail/'.$row->id.'" class="text-info font-medium-3 me-75" title="View Voucher"><i class="fas fa-eye"></i></a>';
                return $btn;
            })
            ->rawColumns(['date','voucherType','performedBy','action'])
            ->make(true);
        }
        return view('admin.add_voucher.history');
    }

    public function histryDetail($id)
    {
        $log = Activity::where('id',$id)->first();
        $properties = $log->properties;
        return view('admin.add_voucher.histryDetail',compact('log','properties'));
    }
}