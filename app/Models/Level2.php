<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\Traits\LogsActivity;
use Spatie\Activitylog\LogOptions;

class Level2 extends Model
{
    use HasFactory,LogsActivity;
    protected $table = 'level_2';

    public function getActivitylogOptions(): LogOptions
    {
        $login_user = \Auth::user()->name;
        return LogOptions::defaults()
        ->logOnly(['code','description','level1.code'])
        ->useLogName('Level2')
        ->logOnlyDirty()
        ->setDescriptionForEvent(fn(string $eventName) => "{$login_user} have {$eventName} Level2");
    }

    public function level1()
    {
        return $this->belongsTo(Level1::class,'id')->select(['id','code','description']);
    }

    public function accounts()
    {
        return $this->hasMany(Account::class,'level_2_id');
    }
}
