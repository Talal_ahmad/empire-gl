<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\Traits\LogsActivity;
use Spatie\Activitylog\LogOptions;

class Location extends Model
{
    use HasFactory,LogsActivity;

    public function getActivitylogOptions(): LogOptions
    {
        $login_user = \Auth::user()->name;
        return LogOptions::defaults()
        ->logOnly(['location_name','description'])
        ->useLogName('Location')
        ->logOnlyDirty()
        ->setDescriptionForEvent(fn(string $eventName) => "{$login_user} have {$eventName} Location");
    }

    public function vouchers()
    {
        return $this->hasMany(Voucher::class);
    }

    public function voucherdetails()
    {
        return $this->hasMany(VoucherDetail::class);
    }
}
