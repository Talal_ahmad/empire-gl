<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.3.1/dist/css/bootstrap.min.css"
        integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <title>Genral Ladger</title>
    <style>
        table,
        th,
        td {
            border: 1px solid black;
            border-collapse: collapse;
        }
    </style>
</head>

<body>
    <div class="container-fluid">
        <div class="row">
            <div id="div_to_print">
                @if (isset($levels) && count($levels) > 0)
                    @foreach ($levels as $level1)
                        @if (count($level1->level2) > 0)
                            @foreach ($level1->level2 as $level2)
                                @if (count($level2->accounts) > 0)
                                    @foreach ($level2->accounts as $data)
                                        {{-- @if (count($data->voucherdetails) > 0) --}}
                                            @php
                                                $openingBalance = openingBalance($data->id, request('fromDate'));
                                            @endphp
                                            <div class="card ">
                                                <div class="row  m-0">
                                                    <div class="col-6">
                                                        <h6 style="font-weight: bold;margin: 10px 0px 0px 0px;">
                                                            {{ env('COMPANY') }}<br>
                                                        </h6>
                                                    </div>
                                                    <div class="col-6 top_btn">
                                                        <p> <b>General Ledger</b></p>
                                                    </div>
                                                    <div class="col-2 col3">
                                                        <p class="para"><b>Control A/c :</b></p>
                                                    </div>
                                                    <div class="col-5 col2">
                                                        <p class="para" style="white-space: nowrap;">
                                                            <b>{{ $data->code }}
                                                                {{ $data->description }} </b>
                                                        </p>
                                                    </div>
                                                    <div class="col-5 col4">
                                                        <p class="para"><b>From:</b> {{ request('fromDate') }}
                                                            <b>To:</b>
                                                            {{ request('toDate') }}
                                                        </p>
                                                        <p class="para"> </p>
                                                    </div>
                                                </div>
                                                <!-- <div class="table-responsive"> -->
                                                <table class="table" id="dataTable">
                                                    <thead class="dataTable">
                                                        <tr>
                                                            <th>Date</th>
                                                            <th>V.Tape/No.</th>
                                                            <th>Description</th>
                                                            <th style="text-align: right !important">Debit(Rs.)</th>
                                                            <th style="text-align: right !important">Credit(Rs.)</th>
                                                            <th style="text-align: right !important">Balance(Rs.)</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        <tr>
                                                            <td colspan="2"></td>
                                                            <td style="white-space: nowrap;"> <b>Opening Balance:</b>
                                                            </td>
                                                            <td colspan="2"></td>
                                                            @if ($openingBalance > 0)
                                                                <td style="text-align: right;white-space: nowrap;"> <b>
                                                                        {{ number_format($openingBalance, 2) }} DR</b>
                                                                </td>
                                                            @else
                                                                <td style="text-align: right;white-space: nowrap;"> <b>
                                                                        {{ number_format(abs($openingBalance), 2) }}
                                                                        CR</b></td>
                                                            @endif
                                                            @php
                                                                $blance = 0;
                                                                $dbtotel = 0;
                                                                $crtotel = 0;
                                                                $blance = $blance + $openingBalance;
                                                            @endphp
                                                        </tr>
                                                        @foreach ($data->voucherdetails as $detail)
                                                            <tr style="border-top: 2px solid #000;">
                                                                <td style="white-space: nowrap;">
                                                                    {{ date('j M y', strtotime($detail->date)) }}</td>
                                                                <td style="white-space: nowrap;">
                                                                    {{ $detail->vouchertype->voucher_type . '-' . $detail->location->short_name }}
                                                                    {{ $detail->voucher_no }}</td>
                                                                <td>{{ $detail->line_remarks }} -
                                                                    {{ !empty($detail->voucher->igp_number) ? $detail->voucher->igp_number : $detail->voucher->ogp_number }}
                                                                </td>
                                                                @if ($detail->amount >= 0)
                                                                    <?php $dbtotel = $detail->amount + $dbtotel; ?>
                                                                    <td style="text-align: right">
                                                                        {{ number_format(abs($detail->amount), 2) }}
                                                                    </td>
                                                                    <td style="text-align: right">-</td>
                                                                @else
                                                                    <td style="text-align: right">-</td>
                                                                    <?php $crtotel = $detail->amount + $crtotel; ?>
                                                                    <td style="text-align: right">
                                                                        {{ number_format(abs($detail->amount), 2) }}
                                                                    </td>
                                                                @endif
                                                                <?php
                                                                $blance = $blance + $detail->amount;
                                                                ?>
                                                                @if ($blance < 0)
                                                                    <td style="text-align: right">
                                                                        {{ '(' . number_format(abs($blance), 2) . ')' }}
                                                                    </td>
                                                                @else
                                                                    <td style="text-align: right">
                                                                        {{ number_format(abs($blance), 2) }}</td>
                                                                @endif
                                                            </tr>
                                                        @endforeach
                                                        <tr style="border-top: 2px solid #000;">
                                                        </tr>
                                                        <tr>
                                                            <td></td>
                                                            <td></td>
                                                            <td style="white-space: nowrap;"><b> Closing Balance:</b>
                                                            </td>
                                                            <td style="text-align: right">
                                                                <b>{{ number_format(abs($dbtotel), 2) }}</b> </td>
                                                            <td style="text-align: right">
                                                                <b>{{ number_format(abs($crtotel), 2) }}</b> </td>
                                                            @if ($blance >= 0)
                                                                <td style="text-align: right">
                                                                    <b>{{ number_format(abs($blance), 2) }}</b> </td>
                                                            @else
                                                                <td style="text-align: right">
                                                                    <b>{{ '(' . number_format(abs($blance), 2) . ')' }}</b>
                                                                </td>
                                                            @endif


                                                        </tr>
                                                    </tbody>
                                                </table>
                                            </div>
                                        {{-- @endif --}}
                                    @endforeach
                                @endif
                            @endforeach
                        @endif
                    @endforeach
                @endif
            </div>
        </div>
    </div>
</body>

</html>
