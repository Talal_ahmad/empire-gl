@extends('admin.layouts.master')

@section('title', 'Dashboard')
@section('style')
    <style>
        .rounded-custom {
            border-radius: 1.40rem;
        }

        .bg-indigo {
            background-color: #283046;
        }

        .table_hover tbody tr,
        .table_hover tfoot tr {
            transition: transform 0.3s ease, background-color 0.3s ease !important;
        }

        .table_hover tbody tr:hover td:hover,
        .table_hover tfoot tr:hover td:hover {
            transform: scale(1.1);
            background-color: rgba(226, 241, 14, 0.959) !important;
            color: #000 !important;
            font-weight: bolder;
        }

        .table_hover tbody tr:hover td:not(:hover),
        .table_hover tfoot tr:hover td:not(:hover) {
            transform: scale(0.9);
        }
    </style>
@endsection
@section('content')
    <section class="content-wrapper container-xxl p-0">
        {{-- <div class="content-header row"></div> --}}
        <div class="content-body mx-2">
            <h1 class="m-0 p-0">Dashboard</h1>
            <div class="content-body row row-cols-1 row-cols-xl-3 row-cols-lg-3 row-cols-md-2 ">
                <div class="px-1 mt-2">
                    <div class="card m-0 rounded-custom">
                        <div class="card-body">
                            <div class="row">
                                <div class="">
                                    <div class="d-flex align-items-center ps-1">
                                        <i class="w-25 pe-1"><img src="{{ asset('assets/Group 879.png') }}"
                                                class="w-100"></img></i>
                                        <div class="">
                                            <a href="{{ asset('/Chart_report') }}">
                                                <p class="card-text fs-4">Chart Of Accounts Report</p>
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="px-1 mt-2">
                    <div class="card m-0 rounded-custom">
                        <div class="card-body">
                            <div class="row">
                                <div class="">
                                    <div class="d-flex align-items-center ps-1">
                                        <i class="w-25 pe-1"><img src="{{ asset('assets/Group 880.png') }}"
                                                class="w-100"></img></i>
                                        <div class="">
                                            <a href="{{ asset('/TrailBalance') }}">
                                                <p class="card-text fs-4">Trail Balance Report</p>
                                            </a>
                                            {{-- <h4 class="fw-bolder">{{ $total_customers }}</h4> --}}
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="px-1 mt-2">
                    <div class="card m-0 rounded-custom">
                        <div class="card-body">
                            <div class="row">
                                <div class="">
                                    <div class="d-flex align-items-center ps-1">
                                        <i class="w-25 pe-1"><img src="{{ asset('assets/Group 881.png') }}"
                                                class="w-100"></img></i>
                                        <div class="">
                                            <a href="{{ asset('/generalLedger') }}">
                                                <p class="card-text fs-4">General Ledger Report</p>
                                            </a>
                                            {{-- <h4 class="fw-bolder">{{ $total_vouchers }}</h4> --}}
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="px-1 mt-2">
                    <div class="card m-0 rounded-custom">
                        <div class="card-body">
                            <div class="row">
                                <div class="">
                                    <div class="d-flex align-items-center ps-1">
                                        <i class="w-25 pe-1"><img src="{{ asset('assets/Group 882.png') }}"
                                                class="w-100"></img></i>
                                        <div class="">
                                            <a href="{{ asset('/editList') }}">
                                                <p class="card-text fs-4">Edit List Report</p>
                                            </a>
                                            {{-- <h4 class="fw-bolder">1,332</h4> --}}
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                @can('Statement Of Daily Affairs')
                    <div class="px-1 mt-2">
                        <div class="card m-0 rounded-custom">
                            <div class="card-body">
                                <div class="row">
                                    <div class="">
                                        <div class="d-flex align-items-center ps-1">
                                            <i class="w-25 pe-1"><img src="{{ asset('assets/Group 305.png') }}"
                                                    class="w-100"></img></i>
                                            <div class="">
                                                <a href="{{ asset('/statement_of_daily_affairs') }}">
                                                    <p class="card-text fs-4">Statement Of Daily Affairs</p>
                                                </a>
                                                {{-- <h4 class="fw-bolder">{{ $total_voucher_types }}</h4> --}}
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                @endcan
                @can('Bank & Cash Balances Report')
                    <div class="px-1 mt-2">
                        <div class="card m-0 rounded-custom">
                            <div class="card-body">
                                <div class="row">
                                    <div class="">
                                        <div class="d-flex align-items-center ps-1">
                                            <i class="w-25 pe-1"><img src="{{ asset('assets/Group 303.png') }}"
                                                    class="w-100"></img></i>
                                            <div class="">
                                                <a href="{{ asset('/bank_and_cash_balances') }}">
                                                    <p class="card-text fs-4">Bank & Cash Balances</p>
                                                </a>
                                                {{-- <h4 class="fw-bolder">150,000</h4> --}}
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                @endcan
            </div>
            <table class="table table-striped mt-3 table-bordered table_hover " data-mdb-animation-start="onHover">
                @php
                    $all_banks_grand_sum = 0;
                @endphp
                <thead>
                    <tr>
                        <th scope="col" class="text-center">Code</th>
                        <th scope="col" class="text-center">Account</th>
                        <th scope="col" class="text-center">Total Amount</th>
                    </tr>
                </thead>
                @foreach ($accounts as $key => $account)
                    @php
                        $all_banks_sum = 0;
                    @endphp

                    <tr>
                        <td colspan="2" class="fs-4">
                            <span class=" fw-bolder">Account Title: </span>{{ $key }}
                        </td>
                        <td></td>
                    </tr>
                    @foreach ($account as $key => $account_details)
                        {{-- @dd($account_details) --}}
                        {{-- @php
                            $opening_balance =
                                $account_details['opening_balance']->debit_amount +
                                $account_details['opening_balance']->credit_amount;

                            $during_the_period_balance =
                                $account_details['total_debit_amount'] + $account_details['total_credit_amount'];

                            $closing_balance =
                                $account_details['closing_balance']->debit_amount +
                                $account_details['closing_balance']->credit_amount;
                        @endphp --}}

                        {{-- Table rows for account details --}}
                        <tr>
                            <td>{{ $account_details['accounts_code'] }}</td>
                            <td>{{ $key }}</td>
                            <td>PKR {{ $account_details['total_amount'] }}</td>
                        </tr>
                        {{-- End of table rows for account details --}}
                        @php
                            $all_banks_sum += $account_details['total_amount'];
                            $all_banks_grand_sum += $account_details['total_amount'];
                        @endphp
                    @endforeach
                    {{-- End of loop account details --}}

                    {{-- Table row for sub total --}}
                    <tr>
                        <td colspan="2" class="text-center fw-bolder">Sub Total</td>
                        <td class=" d-none "></td>
                        <td class=" fw-bolder ">
                            PKR {{ $all_banks_sum }}</td>
                    </tr>
                    {{-- End of table row for sub total --}}
                @endforeach
                {{-- End of loop accounts --}}

                {{-- Table footer --}}
                {{-- @dd($accounts) --}}
                {{-- @empty($accounts) --}}
                <tfoot>
                    <tr>
                        <td colspan="2"><span class="fs-4 fw-bolder">Grand Total:</span></td>
                        <td class=" d-none"></td>
                        <td class=" fw-bolder ">
                            PKR {{ $all_banks_grand_sum }}
                        </td>
                    </tr>
                    {{-- @endempty --}}
                </tfoot>
                {{-- End of table footer --}}
            </table>
        </div>
    </section>
@endsection
@section('scripts')
    <script>
        setTimeout(function() {
            toastr['success'](
                'You have successfully logged in.',
                '👋 Welcome {{ Auth::user()->name }}!', {
                    closeButton: true,
                    tapToDismiss: false
                }
            );
        }, 2000);
    </script>
@endsection
