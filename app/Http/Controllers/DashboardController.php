<?php

namespace App\Http\Controllers;

use App\Models\BankCash;
use App\Models\Customer;
use App\Models\User;
use App\Models\Voucher;
use App\Models\VoucherDetail;
use App\Models\Vouchertype;
use Illuminate\Http\Request;

class DashboardController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $total_users = User::count();
        $total_customers = Customer::count();
        $total_vouchers = Voucher::count();
        $total_voucher_types = Vouchertype::count();
        $accounts = VoucherDetail::join('accounts', 'gl_trans_details.account_id', '=', 'accounts.id')
            ->select(
                'accounts.id',
                'accounts.gl_type',
                'accounts.code as accounts_code',
                'accounts.account_title as account_title',
                'accounts.name as name',
                'gl_trans_details.id as gl_trans_details_id',
                'gl_trans_details.amount',
                'gl_trans_details.created_at as gl_trans_details_created_at',
            )
            // ->where('accounts.account_title','=','CASH IN HAND - UEP')
            ->where('accounts.account_title','!=',null)
            ->get();

        // dd($accounts);
        // $all_bank_accounts_sum = $accounts->where('name', '!=', 'Cash')->sum('debit_amount') + $accounts->where('name', '!=', 'Cash')->sum('credit_amount');
        // dd($all_bank_accounts);
        $accounts = $accounts->groupBy('account_title');
        // dd($accounts);
        $accounts = $accounts->map(function ($accounts_by_name) {
            $accounts_by_name = $accounts_by_name->groupBy('name');
            // dd($accounts_by_name);
            $accounts_by_name = $accounts_by_name->map(function ($bank_accounts) {
                $accounts_code = $bank_accounts->first();
                $total_amount = $bank_accounts->sum('amount');
                return [
                    'accounts_code' => $accounts_code->accounts_code,
                    'total_amount' => $total_amount,
                ];
            });

            return $accounts_by_name;
        });
        // dd($accounts);
        return view('admin.index', get_defined_vars());
    }
}
