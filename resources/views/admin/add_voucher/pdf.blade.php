<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <style>
        table {
            border-collapse: collapse;
        }
        tr,td,th {
            padding: 10px;
            border: 1px solid;
        }
    </style>
</head>
<body>
    <div style="padding: 10px 30px">
        <h1 style="text-align: center;">ST GROUP</h1>
        <h3 style="text-align: center;"><span style="border-bottom:3px solid black">BANK RECIPT VOUCHER</span></h3>
        <div>
            <div style="float: right;">
                <p><b>Status</b>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:&nbsp;&nbsp;&nbsp;&nbsp;Created</p>
                <p><b>Voucher No</b>&nbsp;&nbsp;&nbsp;:&nbsp;&nbsp;&nbsp; 20</p>
                <p><b>Voucher Date:</b>&nbsp;&nbsp;&nbsp; 11/11/2023</p>
            </div>
            <div style="float: left;">
                <p>&nbsp;</p>
                <p>&nbsp;</p>
                <p><b>Location :</b> Lorem ipsum, dolor sit amet consectetur adipisicing.</p>
            </div>
            <div style="clear: both;"></div>
        </div>
        <table style="width: 100%;">
            <thead>
                <tr style="text-align: left;">
                    <th>Account Code</th>
                    <th>Account Title & Entry Narration</th>
                    <th>Debit (Rs.)</th>
                    <th>Credit (Rs.)</th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td rowspan="2" style="vertical-align: top;">16-10-1234</td>
                    <td style="padding-bottom: 40px;">Lorem ipsum dolor sit amet consectetur adipisicing.</td>
                    <td rowspan="2" style="text-align: right;vertical-align: bottom;">12234.32</td>
                    <td rowspan="2" style="text-align: right;vertical-align: bottom;">12234.32</td>
                </tr>
                <tr>
                    <td>Lorem ipsum dolor sit amet consectetur adipisicing elit. Sint, aliquam? lor</td>
                </tr>
                <tr>
                    <td rowspan="2" style="vertical-align: top;">16-10-1234</td>
                    <td style="padding-bottom: 40px;">Lorem ipsum dolor sit amet consectetur adipisicing.</td>
                    <td rowspan="2" style="text-align: right;vertical-align: bottom;">12234.32</td>
                    <td rowspan="2" style="text-align: right;vertical-align: bottom;">12234.32</td>
                </tr>
                <tr>
                    <td>Lorem ipsum dolor sit amet consectetur adipisicing elit. Sint, aliquam? lor</td>
                </tr>
                <tr>
                    <td rowspan="2" style="vertical-align: top;">16-10-1234</td>
                    <td style="padding-bottom: 40px;">Lorem ipsum dolor sit amet consectetur adipisicing.</td>
                    <td rowspan="2" style="text-align: right;vertical-align: bottom;">12234.32</td>
                    <td rowspan="2" style="text-align: right;vertical-align: bottom;">12234.32</td>
                </tr>
                <tr>
                    <td>Lorem ipsum dolor sit amet consectetur adipisicing elit. Sint, aliquam? lor</td>
                </tr>
            </tbody>
            <thead>
                <tr style="border: none;">
                    <td colspan="4" style="border: none;"></td>
                </tr>
                <tr style="border: none;">
                    {{-- <td style="border: none;"><b>Rupees: </b>&nbsp;&nbsp;&nbsp; Ten Thousand Fifty Rupees Only</td> --}}
                    <td colspan="2" style="text-align: right;border: none;"><b>Total</b></td>
                    <td style="text-align: right;">54334</td>
                    <td style="text-align: right;">554340000</td>
                </tr>
            </thead>
        </table>
        <div style="width: 35%;">
            <p><b>Rupees : </b> &nbsp;&nbsp;&nbsp; Ten Thousand Fifty Rupees Only</p>
        </div>
        <div style="width: 75%;">
            <p><b>Remarks : </b> Lorem, ipsum dolor sit amet consectetur adipisicing elit. Cumque suscipit velit labore, rerum itaque ducimus, nesciunt eos autem eligendi, iste reprehenderit harum provident. Aperiam qui nesciunt similique iure quasi quae.</p>
        </div>
        <div style="width: 100%;display: flex;justify-content: space-between; margin-top: 60px;">
            <div style="width: 24%; border-top: 3px solid; text-align: center;">
                <span><b>Prepared By</b></span>
            </div>
            <div style="width: 24%; border-top: 3px solid; text-align: center;">
                <span><b>Checked By</b></span>
            </div>
            <div style="width: 24%; border-top: 3px solid; text-align: center;">
                <span><b>Manager Accounts</b></span>
            </div>
            <div style="width: 24%; border-top: 3px solid; text-align: center;">
                <span><b>Approved By</b></span>
            </div>
        </div>
    </div>
</body>
</html>