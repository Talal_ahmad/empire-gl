@extends('admin.layouts.master')
@section('title', 'Edit List ')
@section('style')
    <style>
        .top_btn {
            border-right: 1px solid #000;
            border-bottom: 1px solid #000;
            border-top: 1px solid #000;
            border-left: 1px solid #000;
            box-shadow: 3px 5px;
            text-align: center;
            padding-top: 5px;

        }

        .col3 {
            width: 25%;
        }

        .col4 {
            width: 30%;
        }

        .col2 {
            width: 20%;
        }

        .col8 {
            width: 65%;
        }

        .para {
            padding-top: 10px;
        }

        .dataTable {
            border-right: 1px solid #000;
            border-bottom: 2px solid #000;
            border-top: 1px solid #000;
            border-left: 1px solid #000;
            box-shadow: 3px 5px;
            text-align: center;
            /* padding-top: 8px; */
        }

        tr td {
            font-size: 10px !important;
        }
    </style>
@endsection
@section('content')
    <section class="content-wrapper container-xxl p-0">
        <div class="content-header row">
            <div class="content-header-left col-md-9 col-12 mb-2">
                <div class="row breadcrumbs-top">
                    <div class="col-12">
                        <h2 class="content-header-title float-start mb-0">Edit List</h2>
                        <div class="breadcrumb-wrapper">
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="{{ url('/') }}">Dashboard</a>
                                </li>
                                <li class="breadcrumb-item"><a href="#">Reports</a>
                                </li>
                                <li class="breadcrumb-item active">Edit List
                                </li>
                            </ol>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="content-body">
            <div class="row">
                <div class="col-12">
                    <div class="card mb-1">
                        <div class="card-body">
                            <form id="search_form" action="{{ url('editList') }}" method="GET">
                                <div class="row">
                                    <div class="col-4">
                                        <label class="form-label" for="voucher_type_id">Voucher
                                            Types</label>
                                        <select name="voucher_type" class="form-select select2"
                                            data-placeholder="Select Voucher Type" required>
                                            <option value=""></option>
                                            @foreach ($VoucherTypes as $type)
                                                <option value="{{ $type->id }}">
                                                    {{ $type->voucher_type }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="col-4">
                                        <label class="form-label" for="date">From</label>
                                        <input type="date" id="date" class="form-control" name="fromDate"
                                            value="{{ !empty(request('fromDate')) ? request('fromDate') : date('Y-m-d') }}"
                                            required />
                                    </div>
                                    <div class="col-4">
                                        <label class="form-label" for="date">To</label>
                                        <input type="date" id="date" class="form-control" name="toDate"
                                            value="{{ !empty(request('toDate')) ? request('toDate') : date('Y-m-d') }}"
                                            required />
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-6 col-12">
                                        <button type="button" onclick="divToPrint()"
                                            class="btn btn-primary mt-1">Print</button>
                                    </div>
                                    <div class="col-md-6 col-12 text-end">
                                        <a href="{{ url('generalLedger') }}" type="button"
                                            class="btn btn-danger mt-1">Reset</a>
                                        <button type="submit" class="btn btn-primary mt-1">Apply</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                    <div class="mb-1">
                        <div class="dt-action-buttons text-end">
                            <div class="dt-buttons d-inline-flex">
                                <a href="{{ url('editList') }}?type=excel&fromDate={{ request('fromDate') }}&toDate={{ request('toDate') }}&voucher_type={{ request('voucher_type') }}"
                                    class="btn btn-success">Download Excel</a>
                                <a href="{{ url('editList') }}?type=pdf&fromDate={{ request('fromDate') }}&toDate={{ request('toDate') }}&voucher_type={{ request('voucher_type') }}"
                                    class="btn btn-danger ms-1">Download Pdf</a>
                            </div>
                        </div>
                    </div>
                </div>
                <div  class="table-responsive" id="div_to_print">
                    <div class="p-1">
                        <div class="row" >
                            <table class="table" id="dataTable">
                                <thead class="dataTable">
                                    <tr>
                                        <th colspan="7" style="font-size: 10px;text-align:left">
                                            {{ date('d-m-Y  h:i:sa') }}
                                        </th>
                                    </tr>
                                    <tr>
                                        <th colspan="7"
                                            style="text-align: center;text-decoration-line: underline;text-transform: uppercase;font-size: 15px !important">
                                            {{ env('COMPANY') }}</th>
                                    </tr>
                                    <tr>
                                        <th colspan="7" style="text-align: center;font-size: 12px !important">Edit List
                                            {{ date('d-m-Y', strtotime(request('fromDate'))) }} To
                                            {{ date('d-m-Y', strtotime(request('toDate'))) }}</th>
                                    </tr>
                                    <tr class="left-row">
                                        <th>Date</th>
                                        <th>V.Type/No.</th>
                                        <th>Code</th>
                                        <th>Description</th>
                                        <th>Voucher Naration</th>
                                        <th style="text-align: right !important">Debit</th>
                                        <th style="text-align: right !important">Credit</th>
                                    </tr>
                                </thead>
                                {{-- @php
                                    dd($vouchers);
                                @endphp --}}
                                <tbody>
                                    @if (count($vouchers) > 0)
                                    @php
                                        $granddebit = 0;
                                        $grandcredit = 0;
                                    @endphp
                                        @foreach ($vouchers as $voucher)
                                            @php
                                                $totaldebit = 0;
                                                $totalcredit = 0;
                                            @endphp
                                            @foreach ($voucher->voucherdetails as $details)
                                                @php
                                                    if ($details->amount > 0) {
                                                        $totaldebit = $totaldebit + $details->amount;
                                                        $granddebit = $granddebit + $details->amount;
                                                    } else {
                                                        $totalcredit = $totalcredit + $details->amount;
                                                        $grandcredit = $grandcredit + $details->amount;
                                                    }
                                                @endphp
                                                <tr>
                                                    <td style="white-space: nowrap;">{{ $details->date }}</td>
                                                    <td>{{ $voucher->vouchertype->voucher_type . '-' . $voucher->voucher_no }}</td>
                                                    <td>{{ $details->account->code }}</td>
                                                    <td>{{ $details->account->description }}</td>
                                                    <td>{{ $details->line_remarks }}</td>
                                                    @if ($details->amount > 0)
                                                        <td style="text-align: right">
                                                            {{ number_format($details->amount, 2) }}</td>
                                                        <td style="text-align: right"></td>
                                                    @else
                                                        <td style="text-align: right"></td>

                                                        <td style="text-align: right">
                                                            {{ number_format(abs($details->amount), 2) }}</td>
                                                    @endif
                                                </tr>
                                            @endforeach
                                            <tr>
                                                <td colspan="5" style="text-align: end"><b>Total</b></td>
                                                <td style="text-align: right; border-top: 2px solid black !important; border-bottom: 2px solid black !important"><b>{{ number_format($totaldebit, 2) }}</b>
                                                </td>
                                                <td style="text-align: right; border-top: 2px solid black !important; border-bottom: 2px solid black !important"><b>{{ number_format(abs($totalcredit), 2) }}</b>
                                                </td>
                                            </tr>

                                        @endforeach
                                        <tr>
                                            <td colspan="5" style="text-align: end"><b>Grand Total</b></td>
                                            <td style="text-align: right; border-top: 2px solid black !important; border-bottom: 2px solid black !important"><b>{{ number_format($granddebit, 2) }}</b>
                                            </td>
                                            <td style="text-align: right; border-top: 2px solid black !important; border-bottom: 2px solid black !important"><b>{{ number_format(abs($grandcredit), 2) }}</b>
                                            </td>
                                        </tr>
                                    @else
                                    @endif
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
    </section>
@endsection
@section('scripts')
    <script>
        function divToPrint() {
            var divToPrint = document.getElementById("div_to_print");
            const WinPrint = window.open("");
            WinPrint.document.open();
            WinPrint.document.write(
                '<html><head><meta name="viewport" content="width=device-width,initial-scale=1.0,user-scalable=0,minimal-ui"><link rel="stylesheet" type="text/css" href="{{ asset('app-assets/css/bootstrap.css') }}"><link rel="stylesheet" type="text/css" href="{{ asset('app-assets/css/bootstrap-extended.css') }}"><style>table {width: 100%;}#dataTable th, #dataTable td {padding: 0.20rem !important;}#test{display:none}table {border-collapse: collapse;}table { page-break-inside:auto; }tr{ page-break-inside:avoid; page-break-after:auto; }.top_btn {border-right: 1px solid #000;border-bottom: 1px solid #000;border-top: 1px solid #000;border-left: 1px solid #000;box-shadow: 3px 5px;text-align: center;padding-top: 5px;}.para {padding-top: 10px;}.dataTable {border: none;box-shadow: 0px 0px;text-align: center;}.dataTable thead,.dataTable tbody{border:none !important}tr td, th{ font-size:12px !important; border:none !important;} .left-row th {text-align:left !important; text-transform: capitalize;} @page {margin-top: 30px;} body {padding-top: 10px;}</style><body   onload="window.print()">' +
                divToPrint.innerHTML + '</body></html>');
            WinPrint.document.close();
            WinPrint.focus();
            WinPrint.print();
            WinPrint.close();
        }

        $(document).ready(function() {
            $("#level1").change(function() {
                var level_1_id = $(this).val();
                $.ajax({
                    type: "GET",
                    url: 'get_levels/' + level_1_id,
                    data: {
                        id: level_1_id
                    },
                    success: function(response) {
                        $('#level2').html('<option value="">Select Level 2</option>');
                        $.each(response, function(index, value) {
                            $('#level2').append(
                                $('<option></option>').val(value.id).html(value
                                    .code + " " + value.description)
                            );
                        });
                    },
                });
            });

            $("#level2").change(function() {
                var level_2_id = $(this).val();
                $.ajax({
                    type: "GET",
                    url: 'get_accounts/' + level_2_id,
                    data: {
                        id: level_2_id
                    },
                    success: function(response) {
                        $('#accounts').html('<option value="">Select Level 3</option>');
                        $.each(response, function(index, value) {
                            $('#accounts').append(
                                $('<option></option>').val(value.id).html(value
                                    .code + " " + value.description)
                            );
                        });
                    },
                });
            });


        })
    </script>
@endsection
