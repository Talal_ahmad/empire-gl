@extends('admin.layouts.master')
@section('title', 'View Voucher')
@section('style')
<style>
    #print {
        display: none;
    }
    #print table {
        border-collapse: collapse;
    }
    #print tr,#print td,#print th {
        padding: 10px;
        border: 1px solid;
    }
</style>
@endsection
@section('content')
<section class="content-wrapper container-xxl p-0">
    <div class="content-header row">
        <div class="content-header-left col-md-9 col-12 mb-2">
            <div class="row breadcrumbs-top">
                <div class="col-12">
                    <h2 class="content-header-title float-start mb-0">Voucher</h2>
                    <div class="breadcrumb-wrapper">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="{{ url('/dashboard') }}">Dashboard</a>
                            </li>
                            <li class="breadcrumb-item"><a href="{{ url('/vouchers') }}">Vouchers</a>
                            </li>
                            <li class="breadcrumb-item active"> View Voucher
                            </li>
                        </ol>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="content-body">
        <div class="card p-2">
            <div class="row">
                <div class="col-md-3 col-6">
                    <h5>Voucher #:</h5>
                    <p>{{ $voucher->voucher_no }}</p>
                </div>
                <div class="col-md-3 col-6">
                    <h5>Voucher Date:</h5>
                    <p>{{ date('d-m-Y',strtotime($voucher->date)) }}</p>
                </div>
                <div class="col-md-3 col-6">
                    <h5>Bill No:</h5>
                    <p>{{ !empty($voucher->bill_no) ? $voucher->bill_no : '-' }}</p>
                </div>
                <div class="col-md-3 col-6">
                    <h5>Status:</h5>
                    @if ($voucher->status==1)
                    <p><span class="badge bg-primary">Created</span></p>
                    @endif
                    @if ($voucher->status==2)
                    <p><span class="badge bg-secondary">Checked</span></p>
                    @endif
                    @if ($voucher->status==3)
                    <p><span class="badge bg-info">Verified</span></p>
                    @endif
                    @if ($voucher->status==4)
                    <p><span class="badge bg-success">Approved</span></p>
                    @endif
                </div>
                <div class="col-md-3 col-6">
                    <h5>Voucher Type:</h5>
                    <p>{{ $voucher->vouchertype->voucher_type }}</p>
                </div>
                <div class="col-md-3 col-6">
                    <h5>Voucher Type Limit:</h5>
                    <p>{{ number_format($voucher->type_limit) }}</p>
                </div>
                <div class="col-md-3 col-6">
                    <h5>Fiscal Year:</h5>
                    <p>{{ $voucher->financialyear->fiscal_year }}</p>
                </div>
                <div class="col-md-3 col-6">
                    <h5>Location:</h5>
                    <p>{{ $voucher->location->location_name }}</p>
                </div>
                @if (!empty($voucher->customer_id))
                <div class="col-md-3 col-6">
                    <h5>Customer:</h5>
                    <p>{{ $voucher->customer->name }}</p>
                </div>
                @endif
                @if (!empty($voucher->supplier_id))
                <div class="col-md-3 col-6">
                    <h5>Supplier:</h5>
                    <p>{{ $voucher->supplier->name }}</p>
                </div>
                @endif
                @if (!empty($voucher->bank_cash_id))
                <div class="col-md-3 col-6">
                    <h5>Bank:</h5>
                    <p>{{ $voucher->bankcash->name }}</p>
                </div>
                @endif
                <div class="col-md-3 col-6">
                    <h5>Gate Inward Pass #:</h5>
                    <p>{{ !empty($voucher->igp_number) ? $voucher->igp_number : '-' }}</p>
                </div>
                <div class="col-md-3 col-6">
                    <h5>Gate Outward Pass #:</h5>
                    <p>{{ !empty($voucher->ogp_number) ? $voucher->ogp_number : '-' }}</p>
                </div>
                <div class="col-md-3 col-6">
                    <h5>Gate Passes Date:</h5>
                    <p>{{ $voucher->igp_ogp_date }}</p>
                </div>
                <div class="col-md-3 col-6">
                    <h5>Created By:</h5>
                    <p>{{ userName($voucher->created_by) }}</p>
                </div>
                <div class="col-md-3 col-6">
                    <h5>Created at:</h5>
                    <p>{{ $voucher->created_at }}</p>
                </div>
                <div class="col-md-3 col-6">
                    <h5>Checked By:</h5>
                    <p>{{ !empty($voucher->checked_by) ? userName($voucher->checked_by) : '-' }}</p>
                </div>
                <div class="col-md-3 col-6">
                    <h5>Checked at:</h5>
                    <p>{{ !empty($voucher->checked_at) ? $voucher->checked_at : '-' }}</p>
                </div>
                <div class="col-md-3 col-6">
                    <h5>Verified By:</h5>
                    <p>{{ !empty($voucher->verified_at) ? userName($voucher->verified_by) : '-' }}</p>
                </div>
                <div class="col-md-3 col-6">
                    <h5>Verified at:</h5>
                    <p>{{ !empty($voucher->verified_at) ? $voucher->verified_at : '-' }}</p>
                </div>
                <div class="col-md-3 col-6">
                    <h5>Approved By:</h5>
                    <p>{{ !empty($voucher->approved_by) ? userName($voucher->approved_by) : '-' }}</p>
                </div>
                <div class="col-md-3 col-6">
                    <h5>Approved at:</h5>
                    <p>{{ !empty($voucher->approved_at) ? $voucher->approved_at : '-' }}</p>
                </div>
                <div class="col-12">
                    <h5>General Remarks:</h5>
                    <p>{{ !empty($voucher->general_remarks) ? $voucher->general_remarks : '-' }}</p>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-12">
                <div class="card pb-2">
                    <table class="table" id="dataTable">
                        <thead>
                            <tr>
                                <th class="not_include"></th>
                                <th>Sr.No</th>
                                <th>ENT</th>
                                <th>Account</th>
                                <th>Qty</th>
                                <th>Rate</th>
                                <th>Debit</th>
                                <th>Credit</th>
                                <th>Remarks</th>
                            </tr>
                        </thead>
                        <tbody>
                            @if (count($voucher_details) > 0)
                                @foreach ($voucher_details as $key => $detail)
                                    <tr>
                                        <td></td>
                                        <td>{{ $key + 1 }}</td>
                                        <td>{{$detail->ent}}</td>
                                        <td>{{$detail->account->code}} ({{$detail->account->description}})</td>
                                        <td>{{!empty($detail->qty) ? $detail->qty : '-'}}</td>
                                        <td>{{!empty($detail->rate) ? $detail->rate : '-'}}</td>
                                        <td>{{$detail->debit > 0 ? number_format($detail->debit,2) : '-'}}</td>
                                        <td>{{$detail->credit > 0 ? number_format($detail->credit,2) : '-'}}</td>
                                        <td>{{!empty($detail->line_remarks) ? $detail->line_remarks : '-'}}</td>
                                    </tr>
                                @endforeach
                            @endif
                        </tbody>
                        <tfoot>
                            <th></th>
                            <th></th>
                            <th></th>
                            <th></th>
                            <th></th>
                            <th></th>
                            <th>{{ number_format($voucher->debit_amount,2) }}</th>
                            <th>{{ number_format($voucher->credit_amount,2) }}</th>
                            <th></th>
                        </tfoot>
                    </table>
                </div>
            </div>
        </div>
        <div class="card p-1">
            <div class="row">
                @if (!empty($voucher->attachments))
                    <h4>Attachments</h4>
                    @foreach (json_decode($voucher->attachments) as $attachment)
                        @php
                            $ext = pathinfo($attachment, PATHINFO_EXTENSION);
                        @endphp
                        @if ($ext == 'pdf')
                        <div class="col-sm-6 col-md-3 col-lg-2">
                            <a href="{{ asset($attachment) }}">
                                <img src="{{ asset('assets/Asset 24.png') }}" class="img-fluid rounded zoom" style="width:72%;border: 7px solid #F8F8F8">
                            </a>
                        </div>
                        @else
                        <div class="col-sm-4 col-md-3 col-lg-2">
                            <a class="elem" href="{{ asset($attachment) }}" target="_blank" title="Attachment">
                                <img src="{{ asset($attachment) }}" class="img-fluid rounded zoom" style="width:100%;border: 7px solid #F8F8F8">
                            </a>
                        </div>
                        @endif
                    @endforeach
                @endif
            </div>
        </div>
    </div>

    <div id="print" style="position:relative">
        <div>
            <div id ="hide">
            <h1 style="text-align: center;">{{env('COMPANY')}}</h1>
            <h3 style="text-align: center;"><span style="border-bottom:3px solid black">{{ strtoupper($voucher->vouchertype->voucher_type) }} VOUCHER</span></h3>
            <div>
                <div style="float: right;">
                    @if ($voucher->status==1)
                    <p><b>Status</b>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:&nbsp;&nbsp;&nbsp;&nbsp;Created</p>
                    @endif
                    @if ($voucher->status==2)
                    <p><b>Status</b>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:&nbsp;&nbsp;&nbsp;&nbsp;Checked</p>
                    @endif
                    @if ($voucher->status==3)
                    <p><b>Status</b>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:&nbsp;&nbsp;&nbsp;&nbsp;Verified</p>
                    @endif
                    @if ($voucher->status==4)
                    <p><b>Status</b>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:&nbsp;&nbsp;&nbsp;&nbsp;Approved</p>
                    @endif
                    <p><b>Voucher No</b>&nbsp;&nbsp;&nbsp;:&nbsp;&nbsp;&nbsp; {{$voucher->voucher_no}}</p>
                    <p><b>Voucher Date:</b>&nbsp;&nbsp;&nbsp; {{date('d-m-Y',strtotime($voucher->date))}}</p>
                </div>
                <div style="float: left;">
                    <p>&nbsp;</p>
                    <p><b>Print date :</b>{{ date('d-m-Y  h:i:sa') }}</p>
                    <p><b>Location :</b> {{ $voucher->location->location_name }}</p>
                </div>
                <div style="clear: both;"></div>
            </div>
            </div>
            <table style="width: 100%;" id="printTable">
                <thead>
                    <tr>
                        <th colspan="5" style="font-size: 10px;text-align:right">
                            {{ date('d-m-Y  h:i:sa') }}
                        </th>
                    </tr>
                    <tr>
                        <th colspan="5" style="text-align: center;text-decoration-line: underline;text-transform: uppercase;font-size: 15px !important">{{ env("COMPANY") }}</th>
                    </tr>
                    <tr>
                        <th colspan="5" style="text-align: center;font-size: 15px !important"><span style="border-bottom:3px solid black">{{ strtoupper($voucher->vouchertype->voucher_type) }} VOUCHER</span></th>
                    </tr>
                    @if ($voucher->status==1)
                    <tr>
                        <th colspan="5" style="text-align: right;font-size: 15px !important">Status:<span> Created</span></th>
                    </tr>
                    @endif
                    @if($voucher->status==2)
                    <tr>
                        <th colspan="5" style="text-align: right;font-size: 15px !important"><span style="border-bottom:3px solid black"> Checked</span></th> 
                    </tr>
                    @endif
                    @if($voucher->status==3)
                    <tr>
                        <th colspan="5" style="text-align: right;font-size: 15px !important"><span style="border-bottom:3px solid black"> Verified</span></th> 
                    </tr>
                    @endif
                    @if($voucher->status==4)
                    <tr>
                        <th colspan="5" style="text-align: right;font-size: 15px !important"><span style="border-bottom:3px solid black"> Approved</span></th> 
                    </tr>
                    @endif
                    <tr>
                        <th colspan="2" style="text-align: left;font-size: 15px !important"><b>Location :</b>{{$voucher->location->location_name}}</th>
                        <th colspan="3" style="text-align: right;font-size: 15px !important"><b>Voucher Date:</b> {{date('d-m-Y',strtotime($voucher->date))}}</th>
                    </tr>
                    <tr>
                        {{-- <th colspan="3" style="text-align: left;font-size: 15px !important"><b>Print date :</b>{{ date('d-m-Y  h:i:sa') }}</th> --}}
                        <th colspan="5" style="text-align: right;font-size: 15px !important"><b>Voucher No</b> {{ $voucher->voucher_no }}</th>
                    </tr>
                    {{-- <tr>
                        <th colspan="5" style="text-align: right;"><b>Voucher Date:</b> {{date('d-m-Y',strtotime($voucher->date))}}</th>
                    </tr> --}}
                    <tr style="text-align: left;">
                        <th>Ent</th>
                        <th>Account Code</th>
                        <th>Account Title & Entry Narration</th>
                        <th>Debit (Rs.)</th>
                        <th>Credit (Rs.)</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($voucher_details as $key => $detail)
                        <tr>
                            <td rowspan="2" style="vertical-align: top;">{{$detail->ent}}</td>
                            <td rowspan="2" style="vertical-align: top;">{{$detail->account->code}}</td>
                            <td ><b>{{ $detail->account->description}}</b></td>
                            <td rowspan="2" style="text-align: right;vertical-align: top;font-weight:20px;">{{$detail->debit > 0 ? number_format($detail->debit,2) : '-'}}</td>
                            <td rowspan="2" style="text-align: right;vertical-align: top;font-weight:20px;">{{$detail->credit > 0 ? number_format($detail->credit,2) : '-'}}</td>
                        </tr>
                        <tr>
                            <td>{{!empty($detail->line_remarks) ? $detail->line_remarks : '-'}}</td>
                        </tr>
                    @endforeach
                </tbody>
                <thead>
                    <tr style="border: none;">
                        <td colspan="4" style="border: none;"></td>
                    </tr>
                    <tr style="border-top:1px solid #000 !important;">
                        <td colspan="2" style="text-align: right;border: none;"><b>Total</b></td>
                        <td></td>
                        <td style="text-align: right;"><b>{{number_format($voucher->debit_amount,2)}}</b></td>
                        <td style="text-align: right;"><b>{{number_format($voucher->credit_amount,2)}}</b></td>
                    </tr>
                </thead>
            </table>
            <div style="width: 65%;">
                <p style="white-space: nowrap;"><b>Rupees : </b> &nbsp;&nbsp;&nbsp; {{numberTowords($voucher->debit_amount)}} only</p>
            </div>
            <div style="width: 65%;">
                <p style="white-space: nowrap;"><b>Remarks : </b> {{ !empty($voucher->general_remarks) ? $voucher->general_remarks : '-' }}.</p>
            </div>
        </div>
        <div>
            <table style="width: 100% ;margin-top:80px">
                <tbody>
                    <tr>
                        <td style="width: 21%; border-bottom: 2px solid black;"></td>
                        <td></td>
                        <td style="width: 21%; border-bottom: 2px solid black;"></td>
                        <td></td>
                        <td style="width: 21%; border-bottom: 2px solid black;"></td>
                        <td></td>
                        <td style="width: 21%; border-bottom: 2px solid black;"></td>
                    </tr>
                    <tr>
                        <td style="text-align: center;"><b>Prepared By</b></td>
                        <td style="text-align: center;"><b></b></td>
                        <td style="text-align: center;"><b>Checked By</b></td>
                        <td style="text-align: center;"><b></b></td>
                        <td style="text-align: center;"><b>Verified By</b></td>
                        <td style="text-align: center;"><b></b></td>
                        <td style="text-align: center;"><b>Approved By</b></td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>
</section>
@endsection
@section('scripts')
    <script>
        $(document).ready(function(){
            $('html, body').animate({
                scrollTop: $('#dataTable').offset().top
            }, 0);
            dataTable = $('#dataTable').DataTable({
                ordering:false,
                "columnDefs": [
                    {
                        // For Responsive
                        className: 'control',
                        data: 'responsive_id',
                        orderable: false,
                        searchable: false,
                        targets: 0
                    },
                ],
                "order": [
                    [0, 'asc']
                ],
                dom: '<"card-header border-bottom p-1"<"head-label"><"dt-action-buttons text-end"B>><"d-flex justify-content-between align-items-center mx-0 row"<"col-sm-12 col-md-6"l><"col-sm-12 col-md-6"f>>t<"d-flex justify-content-between mx-0 row"<"col-sm-12 col-md-6"i><"col-sm-12 col-md-6"p>>',
                displayLength: 10,
                lengthMenu: [10, 25, 50, 75, 100],
                buttons: [{
                        extend: 'collection',
                        className: 'btn btn-outline-secondary dropdown-toggle me-2',
                        text: feather.icons['share'].toSvg({
                            class: 'font-small-4 me-50'
                        }) + 'Export',
                        buttons: [{
                                extend: 'print',
                                footer: true,
                                text: feather.icons['printer'].toSvg({
                                    class: 'font-small-4 me-50'
                                }) + 'Print',
                                className: 'dropdown-item',
                                exportOptions: {
                                    columns: ':not(.not_include)'
                                }
                            },
                            {
                                extend: 'csv',
                                footer: true,
                                text: feather.icons['file-text'].toSvg({
                                    class: 'font-small-4 me-50'
                                }) + 'Csv',
                                className: 'dropdown-item',
                                exportOptions: {
                                    columns: ':not(.not_include)'
                                }
                            },
                            {
                                extend: 'excel',
                                footer: true,
                                text: feather.icons['file'].toSvg({
                                    class: 'font-small-4 me-50'
                                }) + 'Excel',
                                className: 'dropdown-item',
                                exportOptions: {
                                    columns: ':not(.not_include)'
                                }
                            },
                            {
                                extend: 'pdf',
                                footer: true,
                                text: feather.icons['clipboard'].toSvg({
                                    class: 'font-small-4 me-50'
                                }) + 'Pdf',
                                className: 'dropdown-item',
                                exportOptions: {
                                    columns: ':not(.not_include)'
                                }
                            },
                            {
                                extend: 'copy',
                                footer: true,
                                text: feather.icons['copy'].toSvg({
                                    class: 'font-small-4 me-50'
                                }) + 'Copy',
                                className: 'dropdown-item',
                                exportOptions: {
                                    columns: ':not(.not_include)'
                                }
                            }
                        ],
                        init: function(api, node, config) {
                            $(node).removeClass('btn-secondary');
                            $(node).parent().removeClass('btn-group');
                            setTimeout(function() {
                                $(node).closest('.dt-buttons').removeClass('btn-group')
                                    .addClass('d-inline-flex');
                            }, 50);
                        }
                    },
                    {
                        text:'Print Voucher',
                        className: 'create-new btn btn-primary',
                        action: function(e, dt, node, config) {
                            print();
                        },
                        init: function(api, node, config) {
                            $(node).removeClass('btn-secondary');
                        }
                    }
                ],
                responsive: {
                    details: {
                        display: $.fn.dataTable.Responsive.display.childRowImmediate,
                        type: 'column',
                    }
                },
                language: {
                    paginate: {
                        // remove previous & next text from pagination
                        previous: '&nbsp;',
                        next: '&nbsp;'
                    }
                },
            });
            $('div.head-label').html('<h6 class="mb-0">Voucher Details</h6>');
        });

        function print(){
            var divToPrint = document.getElementById("print");
            const WinPrint = window.open("");
            WinPrint.document.open();
            WinPrint.document.write('<html><head><style>#printTable {font-size:11px;color:black;border-collapse:collapse;}#hide{display:none} #printTable tr,#printTable th, #printTable td {border:none !important; padding:5px; } #printTable tr{page-break-inside:avoid;page-break-after:auto;} #footer_fixed{position: fixed;left: 0;bottom: 0;width: 100%;text-align: center;}@page {margin-top: 30px;} body {padding-top: 10px;} </style><body   onload="window.print()">'+divToPrint.innerHTML+'</body></html>');
            WinPrint.document.close();
            WinPrint.focus();
            WinPrint.print();
            WinPrint.close();
        }
    </script>
@endsection