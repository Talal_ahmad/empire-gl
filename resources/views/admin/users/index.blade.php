@extends('admin.layouts.master')
@section('title', 'Users')

@section('content')
<section class="content-wrapper container-xxl p-0">
    <div class="content-header row">
        <div class="content-header-left col-md-9 col-12 mb-2">
            <div class="row breadcrumbs-top">
                <div class="col-12">
                    <h2 class="content-header-title float-start mb-0">Users</h2>
                    <div class="breadcrumb-wrapper">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="{{url('/dashboard')}}">Dashboard</a>
                            </li>
                            <li class="breadcrumb-item active">Users
                            </li>
                        </ol>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="content-header row"></div>
    <div class="content-body">
        <section id="basic-datatable">
            <div class="row">
                <div class="col-12">
                    <div class="card pb-2">
                        <table class="table" id="dataTable">
                            <thead>
                                <tr>
                                    <th class="not_include"></th>
                                    <th>Sr.No</th>
                                    <th>Name</th>
                                    <th>E-Mail</th>
                                    <th>Role</th>
                                    {{-- <th>Financial Year</th>
                                    <th>Location</th> --}}
                                    <th class="not_include">Actions</th>
                                </tr>
                            </thead>
                            <tbody>
                                
                            </tbody>
                        </table>
                    </div>
                </div>
                <!--Add Modal -->
                {{-- <div class="modal fade text-start" id="add_modal" tabindex="-1" aria-labelledby="myModalLabel17" aria-hidden="true">
                    <div class="modal-dialog modal-lg">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h4 class="modal-title" id="myModalLabel17">Add User</h4>
                                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                            </div>
                            <form class="form" id="add_form">
                                @csrf
                                <div class="modal-body">
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="mb-1">
                                                <label class="form-label" for="name">Name</label>
                                                <input type="text" name="name" id="name" class="form-control" placeholder="Full Name" required/>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="mb-1">
                                                <label class="form-label" for="email">E-Mail</label>
                                                <input type="email" name="email" id="email" class="form-control" placeholder="E-Mail" required/>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="mb-1">
                                                <label class="form-label" for="password">Password</label>
                                                <input type="password" name="password" id="password" class="form-control" placeholder="Password" required/>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="mb-1">
                                                <label class="form-label" for="password_confirmation">Confirm Password</label>
                                                <input type="password" name="password_confirmation" id="password_confirmation" class="form-control" placeholder="Confirm Password" required/>
                                            </div>
                                        </div>
                                        <div class="col-6">
                                            <div class="mb-1">
                                                <label for="profile_pic" class="form-label">Profile Photo</label>
                                                <input class="form-control" name="profile_pic" type="file" id="profile_pic">
                                            </div>
                                        </div>
                                        <div class="col-6">
                                            <div class="mb-1">
                                                <label class="form-label" for="role">Role</label>
                                                <select name="role" id="role" class="select2 form-select" data-placeholder="Select Role">
                                                    <option value=""></option>
                                                    @foreach ($roles as $role)
                                                        <option value="{{$role->id}}">{{$role->name}}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-6">
                                            <div class="mb-1">
                                                <label class="form-label" for="fiscal_year">Financial Year</label>
                                                <select name="fiscal_year" id="fiscal_year" class="select2 form-select" data-placeholder="Select Financial Year">
                                                    <option value=""></option>
                                                    @foreach ($financialyear as $year)
                                                        <option value="{{$year->id}}">{{$year->fiscal_year}}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-6">
                                            <div class="mb-1">
                                                <label class="form-label" for="location">Location</label>
                                                <select name="location" id="location" class="select2 form-select" data-placeholder="Select Location">
                                                    <option value=""></option>
                                                    @foreach ($location as $loc)
                                                        <option value="{{$loc->id}}">{{$loc->location_name}}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="modal-footer">
                                    <button type="reset" class="btn btn-outline-success">Reset</button>
                                    <button type="submit" class="btn btn-primary" id="save">Save</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div> --}}
            </div>
            <!--Edit Modal -->
            {{-- <div class="modal fade text-start" id="edit_modal" tabindex="-1" aria-labelledby="myModalLabel17" aria-hidden="true">
                <div class="modal-dialog modal-lg">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h4 class="modal-title" id="myModalLabel17">Edit User</h4>
                            <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                        </div>
                        <form class="form" id="edit_form">
                            @csrf
                            @method('PUT')
                            <div class="modal-body">
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="mb-1">
                                            <label class="form-label" for="edit_name">Name</label>
                                            <input type="text" name="name" id="edit_name" class="form-control" placeholder="Name" required/>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="mb-1">
                                            <label class="form-label" for="edit_email">E-Mail</label>
                                            <input type="email" name="email" id="edit_email" class="form-control" placeholder="E-Mail" required readonly/>
                                        </div>
                                    </div>
                                    <div class="col-6">
                                        <div class="mb-1">
                                            <label for="edit_profile_pic" class="form-label">Profile Photo</label>
                                            <input class="form-control" name="profile_pic" type="file" id="edit_profile_pic">
                                        </div>
                                    </div>
                                    <div class="col-6">
                                        <div class="mb-1">
                                            <label class="form-label" for="edit_role">Role</label>
                                            <select name="role" id="edit_role" class="select2 form-select" data-placeholder="Select Role" required>
                                                <option value=""></option>
                                                @foreach ($roles as $role)
                                                    <option value="{{$role->id}}">{{$role->name}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-6">
                                        <div class="mb-1">
                                            <label class="form-label" for="fiscal_year">Financial Year</label>
                                            <select name="edit_fiscal_year" id="edit_fiscal_year" class="select2 form-select" data-placeholder="Select Financial Year">
                                                <option value=""></option>
                                                @foreach ($financialyear as $year)
                                                    <option value="{{$year->id}}">{{$year->fiscal_year}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-6">
                                        <div class="mb-1">
                                            <label class="form-label" for="location">Location</label>
                                            <select name="edit_location" id="edit_location" class="select2 form-select" data-placeholder="Select Location">
                                                <option value=""></option>
                                                @foreach ($location as $loc)
                                                    <option value="{{$loc->id}}">{{$loc->location_name}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="modal-footer">
                                <button type="reset" class="btn btn-outline-success">Reset</button>
                                <button type="submit" class="btn btn-primary" id="update">Update</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div> --}}
        </section>
    </div>
</section>

@endsection
@section('scripts')
    @if (Session::has('success_message'))
        <script>
            Toast.fire({
                icon: "success",
                title: "{!! Session::get('success_message') !!}"
            })
        </script>
    @endif
    <script>
        var rowid;        
        $(document).ready(function() {
            dataTable = $('#dataTable').DataTable({
                processing: true,
                serverSide: true,
                responsive: true,
                ajax: "{{ route('users.index') }}",
                columns: [
                    {
                        data: 'responsive_id'
                    },
                    {
                        data : 'id',
                        name : 'users.id',
                    },
                    {
                        // data: 'name',
                        name: 'users.name',
                        render: function(data, type, full, meta) {
                            var $user_img = full['profile_photo_path'],
                                $name = full['name'];
                            var assetPath = '{{ URL::asset('/profile_pics') }}';
                            if ($user_img) {
                                // For Profile Photo
                                var $output =
                                    '<img src="' + assetPath + '/' + $user_img + '" alt="Profile Photo" width="35" height="35">';
                            } else {
                                // For Avatar badge
                                var stateNum = full['status'];
                                var states = ['success', 'danger', 'warning', 'info', 'dark', 'primary', 'secondary'];
                                var $state = states[stateNum],
                                    $name = full['name'],
                                    $initials = $name.match(/\b\w/g) || [];
                                $initials = (($initials.shift() || '') + ($initials.pop() || '')).toUpperCase();
                                $output = '<span class="avatar-content">' + $initials + '</span>';
                            }
                            var colorClass = $user_img === '' ? ' bg-light-' + $state + ' ' : '';
                            // Creates full output for row
                            var $row_output =
                                '<div class="d-flex justify-content-left align-items-center">' +
                                '<div class="avatar ' +
                                colorClass +
                                ' me-1">' +
                                $output +
                                '</div>' +
                                '<div class="d-flex flex-column">' +
                                '<span class="emp_name text-truncate fw-bold">' +
                                $name +
                                '</span>' +
                                '</div>' +
                                '</div>';
                            return $row_output;
                        }
                    },
                    {
                        data: 'email',
                        name: 'users.email',
                    },
                    {
                        name: 'roles.name',
                        render: function(data, type, full, meta) {
                            if (full['roleName'] === null) {
                                return '-';
                            }
                            return (
                                '<span class="badge rounded-pill badge-light-success">' + full['roleName']+ '</span>'
                            );
                        }
                    },
                    // {
                    //     data: 'fiscal_years', 
                    //     render: function(data,type,row){
                    //         if(row.fiscal_years){
                    //             let user_dept = JSON.parse(row.fiscal_years.replace(/&quot;/g,'"'));
                    //             let assign_fiscal_years = '';
                    //             $.each(all_dept , function(index , all_dept_value){
                    //                 $.each(user_dept , function(index , user_dept_value){
                    //                     if(user_dept_value == all_dept_value.id){
                    //                         assign_departments += `<p class="badge bg-light-info">${all_dept_value.title}</p> `;
                    //                     }
                    //                 });
                    //             });
                    //             return assign_departments;
                    //         }   
                    //     }
                    // },
                    // {
                    //     data: 'fiscal_year',
                    //     name: 'users.fiscal_years_id',
                    // },
                    // {
                    //     data: 'location_name',
                    //     name: 'locations',
                    // },
                    {
                        data: ''
                    },
                ],
                "columnDefs": [
                    {
						// For Responsive
						className: 'control',
						orderable: false,
                        searchable: false,
						targets: 0
					},
                    {
                        // Actions
                        targets: -1,
                        title: 'Actions',
                        orderable: false,
                        searchable: false,
                        render: function (data, type, full, meta) {
                            var url = '{{ route("users.edit", ":id") }}';
                            url = url.replace(':id', full.id);
                            return (
                                '<a href="'+url+'" title="Edit Calculation Method">' +
                                feather.icons['edit'].toSvg({ class: 'font-medium-4' }) +
                                '</a>'
                            );
                        }
                    },
                    {
                        "defaultContent": "-",
                        "targets": "_all"
                    }
                ],
                "order": [[0, 'asc']],
                dom: '<"card-header border-bottom p-1"<"head-label"><"dt-action-buttons text-end"B>><"d-flex justify-content-between align-items-center mx-0 row"<"col-sm-12 col-md-6"l><"col-sm-12 col-md-6"f>>t<"d-flex justify-content-between mx-0 row"<"col-sm-12 col-md-6"i><"col-sm-12 col-md-6"p>>',
                displayLength: 10,
                lengthMenu: [10, 25, 50, 75, 100],
                buttons: [
                    {
                        extend: 'collection',
                        className: 'btn btn-outline-secondary dropdown-toggle me-2',
                        text: feather.icons['share'].toSvg({ class: 'font-small-4 me-50' }) + 'Export',
                        buttons: [
                            {
                            extend: 'print',
                            text: feather.icons['printer'].toSvg({ class: 'font-small-4 me-50' }) + 'Print',
                            className: 'dropdown-item',
                            exportOptions: { columns: ':not(.not_include)' }
                            },
                            {
                            extend: 'csv',
                            text: feather.icons['file-text'].toSvg({ class: 'font-small-4 me-50' }) + 'Csv',
                            className: 'dropdown-item',
                            exportOptions: { columns: ':not(.not_include)' }
                            },
                            {
                            extend: 'excel',
                            text: feather.icons['file'].toSvg({ class: 'font-small-4 me-50' }) + 'Excel',
                            className: 'dropdown-item',
                            exportOptions: { columns: ':not(.not_include)' }
                            },
                            {
                            extend: 'pdf',
                            text: feather.icons['clipboard'].toSvg({ class: 'font-small-4 me-50' }) + 'Pdf',
                            className: 'dropdown-item',
                            exportOptions: { columns: ':not(.not_include)' }
                            },
                            {
                            extend: 'copy',
                            text: feather.icons['copy'].toSvg({ class: 'font-small-4 me-50' }) + 'Copy',
                            className: 'dropdown-item',
                            exportOptions: { columns: ':not(.not_include)' }
                            }
                        ],
                        init: function (api, node, config) {
                            $(node).removeClass('btn-secondary');
                            $(node).parent().removeClass('btn-group');
                            setTimeout(function () {
                            $(node).closest('.dt-buttons').removeClass('btn-group').addClass('d-inline-flex');
                            }, 50);
                        }
                    },
                    {
                        text: feather.icons['plus'].toSvg({ class: 'me-50 font-small-4' }) + 'Add New',
                        className: 'create-new btn btn-primary',
                        action: function(e, dt, node, config) {
                            window.location.href = '{{ route('users.create') }}';
                        },
                        init: function(api, node, config) {
                            $(node).removeClass('btn-secondary');
                        }
                    }
                ],
                responsive: {
					details: {
						display: $.fn.dataTable.Responsive.display.childRowImmediate,
						type: 'column',
					}
				},
                language: {
                    paginate: {
                    // remove previous & next text from pagination
                    previous: '&nbsp;',
                    next: '&nbsp;'
                    }
                }
            });
            $('div.head-label').html('<h6 class="mb-0">List of Users</h6>');

            $("#add_form").submit(function (e) {
                e.preventDefault();
                $.ajax({
                    url: "{{route('users.store')}}",
                    type: "POST",
                    data: new FormData(this),
                    processData: false,
                    contentType: false,
                    responsive: true,
                    success: function (response) {
                        if(response.errors){
                            $.each( response.errors, function( index, value ){
                                Toast.fire({
                                    icon: 'error',
                                    title: value
                                })
                            });
                        }
                        else if(response.error_message){
                            Toast.fire({
                                icon: 'error',
                                title: 'An error has been occured! Please Contact Administrator.'
                            })
                        }
                        else{
                            $('#add_form')[0].reset();
                            $("#add_modal").modal("hide");
                            dataTable.ajax.reload();
                            Toast.fire({
                                icon: 'success',
                                title: 'Users has been Added Successfully!'
                            })
                        }
                        
                    }
                });
            });
            $("#edit_form").submit(function (e) {
                e.preventDefault();
                $.ajax({
                    url: "{{url('users')}}" + "/" + rowid,
                    type: 'POST',
                    data: new FormData(this),
                    contentType: false,
                    processData: false, 
                    success: function (response) {
                        if(response.errors){
                            $.each( response.errors, function( index, value ){
                                Toast.fire({
                                    icon: 'error',
                                    title: value
                                })
                            });
                        }
                        else if(response.error_message){
                            Toast.fire({
                                icon: 'error',
                                title: 'An error has been occured! Please Contact Administrator.'
                            })
                        }
                        else{
                            $('#edit_form')[0].reset();
                            $("#edit_modal").modal("hide");
                            dataTable.ajax.reload();
                            Toast.fire({
                                icon: 'success',
                                title: 'User has been Updated Successfully!'
                            })
                        }
                    }
                });
            });
        });

        function user_edit(id){
            rowid = id;
            $.ajax({
                url: "{{url('users')}}" + "/" + rowid + "/edit",
                type: "GET",
                success: function(response){
                    console.log(response);
                    var role = response.role ? response.role.role_id : '';
                    $('#edit_name').val(response.user.name);
                    $('#edit_email').val(response.user.email);
                    $('#edit_role').val(role).select2();
                    $('#edit_location').val(response.location.location_id).select2();
                    $('#edit_fiscal_year').val(response.fiscal_year.fiscal_years_id).select2();
                    $('#edit_modal').modal('show');
                }
            });
        }
    </script>
@endsection