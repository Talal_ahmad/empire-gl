<?php

namespace App\Imports;


use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithHeadingRow;
use App\Models\Account;
use App\Models\Level2;

class AccountsImports implements ToModel,WithHeadingRow
{
    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */
    public  $privious=0;
    public function model(array $row)
    { 
        if(!empty($row['code'])){
            $code=explode('/',$row['code']);
            if( $this->privious != $row['code'])
            {
                $this->privious = $row['code'];
                $levl2code=$code[0]."-".$code[1];
                $id=Level2::where('code', $levl2code)->first();
                if(!empty($id->id)){
                    $accounts = new Account();
                    $accounts->code =$code[0]."-".$code[1]."-".$code[2];
                    $accounts->description = $row['description'];
                    $accounts->level2_code=$code[0]."-".$code[1];
                    $accounts->level_2_id=$id->id;
                    $accounts->account_code=$code[2];
                    $accounts->gl_type=$row['gl_type'];
                    $accounts->save();
                    return $accounts;


                }
             
           

            }
          
        }
       
    }
}
