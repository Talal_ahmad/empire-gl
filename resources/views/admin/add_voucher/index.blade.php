@extends('admin.layouts.master')
@section('title', 'Vouchers')
@section('style')
    <style>
        .pagination{
            justify-content: flex-end;
            padding-right: 10px;
        }
    </style>
@endsection
@section('content')
<section class="content-wrapper container-xxl p-0">
    <div class="content-header row">
        <div class="content-header-left col-md-9 col-12 mb-2">
            <div class="row breadcrumbs-top">
                <div class="col-12">
                    <h2 class="content-header-title float-start mb-0">Vouchers</h2>
                    <div class="breadcrumb-wrapper">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="{{url('/dashboard')}}">Dashboard</a>
                            </li>
                            <li class="breadcrumb-item active">Vouchers
                            </li>
                        </ol>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="content-body">
        <section id="basic-datatable">
            <div class="row">
                <div class="col-12">
                    <div class="card pb-2">
                        <table class="table" id="dataTable">
                            <thead>
                                <tr>
                                    <th class="not_include"></th>
                                    <th>Sr.No</th>
                                    <th>Voucher No</th>
                                    <th>Date</th>
                                    <th>Amount</th>
                                    <th>Voucher Type</th>
                                    <th>Bill No</th>
                                    <th>Location</th>
                                    <th>Status</th>
                                    {{-- <th>Remarks</th> --}}
                                    <th class="not_include">Actions</th>
                                </tr>
                            </thead>
                            <tbody>
                                
                            </tbody>
                        </table>
                        
                    </div>
                </div>
            </div>
        </section>
    </div>
</section>
@endsection

@section('scripts')
<script>
$(document).ready(function() {
    dataTable = $('#dataTable').DataTable({
        processing: true,
        serverSide: true,
        responsive: true,
        ajax: "{{ route('vouchers.index') }}",
        columns: [
            {
                data: 'responsive_id',
                searchable: false,
                orderable:false
            },
            {
                data: 'DT_RowIndex', name: 'DT_RowIndex', orderable: false, searchable: false 
            },
            {
                data: 'voucher_no',
                name: 'gl_trans.voucher_no',
            },
            {
                data: 'date',
                name: 'gl_trans.date',
            },
            {
                data: 'debit_amount',
                name: 'gl_trans.debit_amount',
                render: function(data, type, full, meta) {
                    return numberWithCommas(data)
                }
            },
            {
                data: 'vocherType',
                name: 'vouhertypes.voucher_type',
            },
            {
                data: 'bill_no',
                name: 'gl_trans.bill_no',
            },
            {
                data: 'location',
                name: 'locations.location_name',
            },
            {
                data: 'status',
                orderable: false,
                searchable: false,
                render: function(data, type, full, meta) {
                    if(full.status == 1){
                        return '<span class="badge bg-primary">Created</span>';
                    }
                    if(full.status == 2){
                        return '<span class="badge bg-secondary">Checked</span>';
                    }
                    if(full.status == 3){
                        return '<span class="badge bg-info">Verified</span>';
                    }
                    if(full.status == 4){
                        return '<span class="badge bg-success">Approved</span>';
                    }
                }
            },
            // {
            //     data: 'general_remarks',
            //     name: 'gl_trans.general_remarks',
            // },
            {
                data: 'action',
                name: 'action',
                orderable: false,
                searchable: false,
            },
        ],
        "columnDefs": [
            {
                // For Responsive
                className: 'control',
                orderable: false,
                searchable: false,
                targets: 0
            },
            {
                "defaultContent": "-",
                "targets": "_all"
            }
        ],
        "order": [
            [0, 'asc']
        ],
        dom: '<"card-header border-bottom p-1"<"head-label"><"dt-action-buttons text-end"B>><"d-flex justify-content-between align-items-center mx-0 row"<"col-sm-12 col-md-6"l><"col-sm-12 col-md-6"f>>t<"d-flex justify-content-between mx-0 row"<"col-sm-12 col-md-6"i><"col-sm-12 col-md-6"p>>',
        displayLength: 10,
        lengthMenu: [10, 25, 50, 75, 100],
        buttons: [{
                extend: 'collection',
                className: 'btn btn-outline-secondary dropdown-toggle me-2',
                text: feather.icons['share'].toSvg({
                    class: 'font-small-4 me-50'
                }) + 'Export',
                buttons: [{
                        extend: 'print',
                        text: feather.icons['printer'].toSvg({
                            class: 'font-small-4 me-50'
                        }) + 'Print',
                        className: 'dropdown-item',
                        exportOptions: {
                            columns: ':not(.not_include)'
                        }
                    },
                    {
                        extend: 'csv',
                        text: feather.icons['file-text'].toSvg({
                            class: 'font-small-4 me-50'
                        }) + 'Csv',
                        className: 'dropdown-item',
                        exportOptions: {
                            columns: ':not(.not_include)'
                        }
                    },
                    {
                        extend: 'excel',
                        text: feather.icons['file'].toSvg({
                            class: 'font-small-4 me-50'
                        }) + 'Excel',
                        className: 'dropdown-item',
                        exportOptions: {
                            columns: ':not(.not_include)'
                        }
                    },
                    {
                        extend: 'pdf',
                        text: feather.icons['clipboard'].toSvg({
                            class: 'font-small-4 me-50'
                        }) + 'Pdf',
                        className: 'dropdown-item',
                        exportOptions: {
                            columns: ':not(.not_include)'
                        }
                    },
                    {
                        extend: 'copy',
                        text: feather.icons['copy'].toSvg({
                            class: 'font-small-4 me-50'
                        }) + 'Copy',
                        className: 'dropdown-item',
                        exportOptions: {
                            columns: ':not(.not_include)'
                        }
                    }
                ],
                init: function(api, node, config) {
                    $(node).removeClass('btn-secondary');
                    $(node).parent().removeClass('btn-group');
                    setTimeout(function() {
                        $(node).closest('.dt-buttons').removeClass('btn-group')
                            .addClass('d-inline-flex');
                    }, 50);
                }
            },
            {
                text: feather.icons['plus'].toSvg({
                    class: 'me-50 font-small-4'
                }) + 'Add Voucher',
                className: 'create-new btn btn-primary',
                action: function(e, dt, node, config) {
                    window.location.href = '{{ route('vouchers.create') }}';
                },
                init: function(api, node, config) {
                    $(node).removeClass('btn-secondary');
                }
            }
        ],
        responsive: {
            details: {
                display: $.fn.dataTable.Responsive.display.childRowImmediate,
                type: 'column',
            }
        },
        language: {
            paginate: {
                // remove previous & next text from pagination
                previous: '&nbsp;',
                next: '&nbsp;'
            }
        },
    });
    $('div.head-label').html('<h6 class="mb-0">List of Vouchers</h6>');
});


function deleteVoucher(id) {
            $.confirm({
                icon: 'far fa-question-circle',
                title: 'Confirm!',
                content: 'Are you sure you want to delete!',
                type: 'orange',
                typeAnimated: true,
                buttons: {
                    Confirm: {
                        text: 'Confirm',
                        btnClass: 'btn-orange',
                        action: function() {
                            $.ajax({
                                url: "vouchers/" + id,
                                type: "DELETE",
                                data: {
                                    _token: "{{ csrf_token() }}"
                                },
                                success: function(response) {
                                    if (response.error_message) {
                                        Toast.fire({
                                            icon: 'error',
                                            title: 'An error has been occured! Please Contact Administrator.'
                                        })
                                    }else if(response.code == 300){
                                        $.alert({
                                            icon: 'far fa-times-circle',
                                            title: 'Oops!',
                                            content: response.message,
                                            type: 'red',
                                            buttons: {
                                                Okay: {
                                                    text: 'Okay',
                                                    btnClass: 'btn-red',
                                                }
                                            }
                                        });
                                    }else {
                                        dataTable.ajax.reload();
                                        Toast.fire({
                                            icon: 'success',
                                            title: 'Voucher has been Deleted Successfully!'
                                        })
                                    }
                                }
                            });
                        }
                    },
                    cancel: function() {
                        $.alert('Canceled!');
                    },
                }
            });
        }
function status(id,status) {
    $.confirm({
        icon: 'far fa-question-circle',
        title: 'Confirm!',
        content: 'Are you sure?',
        type: 'orange',
        typeAnimated: true,
        buttons: {
            Confirm: {
                text: 'Confirm',
                btnClass: 'btn-orange',
                action: function() {
                    $.ajax({
                        url: "{{url('VoucherStatus')}}" + '/' + id,
                        type: "POST",
                        data: {
                            _token: "{{ csrf_token() }}",
                            status:status
                        },
                        success: function(response) {
                            if (response.error_message) {
                                Toast.fire({
                                    icon: 'error',
                                    title: 'An error has been occured! Please Contact Administrator.'
                                })
                            }
                            else {
                                dataTable.ajax.reload();
                                Toast.fire({
                                    icon: 'success',
                                    title: 'Voucher has been '+status+' Successfully!'
                                })
                            }
                        }
                    });
                }
            },
            cancel: function() {
                $.alert('Canceled!');
            },
        }
    });
}
</script>
@endsection