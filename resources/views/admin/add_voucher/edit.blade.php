@extends('admin.layouts.master')
@section('title', 'Edit Voucher')
@section('style') 
<style>
    blink {
        padding: 5px 10px;
        font-weight: 700;
        font-size: 24px;
  animation: 1s linear infinite condemned_blink_effect;
}

@keyframes condemned_blink_effect {
  0% {
    visibility: hidden;
  }
  50% {
    visibility: hidden;
  }
  100% {
    visibility: visible;
    background-color:red;
  }
}
</style>
@endsection
@section('content')
<section class="content-wrapper container-xxl p-0">
    <div class="content-header row">
        <div class="content-header-left col-md-9 col-12 mb-2">
            <div class="row breadcrumbs-top">
                <div class="col-12">
                    <h2 class="content-header-title float-start mb-0">Voucher</h2>
                    <div class="breadcrumb-wrapper">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="{{ url('/dashboard') }}">Dashboard</a>
                            </li>
                            <li class="breadcrumb-item"><a href="{{ url('/vouchers') }}">Vouchers</a>
                            </li>
                            <li class="breadcrumb-item active"> Edit Voucher
                            </li>
                        </ol>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="content-body">
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-header">
                        <h4 class="card-title">Edit Voucher</h4>
                        <div class="col-12 p-2 text-center">
                            @if(request('type') == 'Check' && !empty($voucher->check_remarks)  )
                            <div>
                                <blink class="text-white"><strong>NOTE ! </strong>{{$voucher->check_remarks}}</blink>
                            </div>
                            @elseif(request('type') == 'Verify' && !empty($voucher->check_remarks) )
                            <div>
                                <blink class="text-white"><strong>NOTE ! </strong>{{$voucher->check_remarks}}</blink>
                            </div>
                            @elseif(request('type') == 'Approve' && !empty($voucher->varify_remarks) )
                            <div>
                                <blink class="text-white"><strong>NOTE ! </strong>{{$voucher->varify_remarks}}</blink>
                            </div>
                            @endif
                        </div>
                    </div>
                    <div class="card-body">
                        <form class="form" id="edit_voucher" enctype="multipart/form-data">
                            @csrf
                            @method('PUT')
                            <div class="row">
                                <div class="col-md-4 col-12">
                                    <div class="mb-1">
                                        <label class="form-label" for="voucher_type">Voucher Types</label>
                                        <select name="voucher_type" id="voucher_type" class="select2 form-select" data-placeholder="Select Voucher Type" required disabled>
                                            <option value=""></option>
                                            @foreach ($Vouchertypes as $Vouchertype)
                                            <option value="{{ $Vouchertype->id }}" {{$Vouchertype->id==$voucher->voucher_type ? 'selected' : ''}}>{{ $Vouchertype->voucher_type }}
                                            </option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <input type="hidden" name="voucher_type" value="{{ $voucher->voucher_type }}">
                                </div>
                                <div class="col-md-4 col-12">
                                    <div class="mb-1">
                                        <label class="form-label" for="date">Date</label>
                                        <input type="date" name="date" id="date" class="startdate_input date form-control" value="{{$voucher->date}}" readonly required>
                                    </div>
                                </div>
                                <input type="hidden" id="type" name="type" value="{{request('type')}}">
                                <div class="col-md-4 col-12">
                                    <label class="form-label" for="bill_no">Bill No</label>
                                    <input type="text" name="bill_no" id="bill_no" class="form-control" value="{{$voucher->bill_no}}" placeholder="Bill No">
                                </div>
                                <div class="col-md-4 col-12">
                                    <label class="form-label" for="igp_number">Gate Inward Pass #</label>
                                    <input type="text" name="igp_number" id="igp_number" class="form-control" value="{{$voucher->igp_number}}" placeholder="Gate Inward Pass Number">
                                </div>
                                <div class="col-md-4 col-12">
                                    <label class="form-label" for="ogp_number">Gate Outward Pass #</label>
                                    <input type="text" name="ogp_number" id="ogp_number" class="form-control" value="{{$voucher->ogp_number}}" placeholder="Gate Outward Pass Number">
                                </div>
                                <div class="col-md-4 col-12">
                                    <div class="mb-1">
                                        <label class="form-label" for="igp_ogp_date">Gate Passes Date</label>
                                        <input type="date" name="igp_ogp_date" id="igp_ogp_date" value="{{$voucher->igp_ogp_date}}" class="form-control">
                                    </div>
                                </div>
                                @if (!empty($voucher->customer_id))
                                <div class="col-12">
                                    <div class="mb-1">
                                        <label class="form-label" for="customer">Customers</label>
                                        <select name="customer" id="customer" class="select2 form-select" data-placeholder="Select Customer" disabled required>
                                            <option value=""></option>
                                            @foreach ($customers as $customer)
                                                <option value="{{ $customer->id }}" {{$customer->id==$voucher->customer_id ? 'selected' : ''}}>{{ $customer->name }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <input type="hidden" name="customer" value="{{$voucher->customer_id}}">
                                @endif
                                @if (!empty($voucher->supplier_id))
                                <div class="col-12">
                                    <div class="mb-1">
                                        <label class="form-label" for="supplier">Suppliers</label>
                                        <select name="supplier" id="supplier" class="select2 form-select" data-placeholder="Select Supplier" required disabled>
                                            <option value=""></option>
                                            @foreach ($suppliers as $supplier)
                                                <option value="{{ $supplier->id }}" {{$supplier->id==$voucher->supplier_id ? 'selected' : ''}}>{{ $supplier->name }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <input type="hidden" name="supplier" value="{{$voucher->supplier_id}}">
                                @endif
                                @if (!empty($voucher->bank_cash_id))
                                <input type="hidden" name="bank" value="{{$voucher->bank_cash_id}}">
                                <div class="col-md-4 col-12">
                                    <div class="mb-1">
                                        <label class="form-label" for="bank">Banks</label>
                                        <select name="bank" id="bank" class="select2 form-select" data-placeholder="Select Bank" required disabled>
                                            <option value=""></option>
                                            @foreach ($bankcash as $bank)
                                                <option value="{{ $bank->id }}" {{$bank->id==$voucher->bank_cash_id ? 'selected' : ''}}>{{ $bank->name }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-4 col-12">
                                    <div class="mb-1">
                                        <label class="form-label" for="cheque_no">Cheque No</label>
                                        <input type="text" name="cheque_no" id="cheque_no" class="form-control" value="{{$voucher->cheque_no}}" placeholder="Cheque No" required>
                                    </div>
                                </div>
                                <div class="col-md-4 col-12">
                                    <div class="mb-1">
                                        <label class="form-label" for="cheque_date">Cheque Date</label>
                                        <input type="date" name="cheque_date" id="cheque_date" class="form-control" value="{{$voucher->cheque_date}}" required>
                                    </div>
                                </div
                                @endif
                                <div class="col-md-6 col-12">
                                    <div class="mb-1">
                                        <label class="form-label" for="location">Locations</label>
                                        <select name="location" id="location" class="select2 form-select" data-placeholder="Select Location" required>
                                            <option value=""></option>
                                            @foreach ($locations as $location)
                                            <option value="{{ $location->id }}" {{$location->id==$voucher->location_id ? 'selected' : ''}}>{{ $location->location_name }}
                                            </option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-6 col-12" id="attchmentDiv">
                                    <label class="form-label" for="attachments">Attachments</label>
                                    <input type="file" name="attachments[]" id="attachments" class="form-control" multiple/>
                                </div>
                                <div class="col-12">
                                    <label class="form-label" for="general_remarks">General Remarks</label>
                                    <textarea name="general_remarks" id="general_remarks" cols="20" rows="2" class="form-control">{{$voucher->general_remarks}}</textarea>
                                </div>
                                <div class="row mt-2">
                                    <div class="col-12 text-center" style="background: #F3F2F7">
                                        <h4 id="entryDate" style="padding-top: 7px;">{{date('Y-m-d')}}</h4>
                                    </div>
                                    <div class="voucher_entry" id="voucher_entry">
                                        @foreach ($voucher_details as $key => $detail)
                                            @php
                                                $offset = 'offset-5';
                                                $col = 'col-md-3';
                                                $AccountCol = 'col-md-4';
                                                $EntCol = 'col-md-2';
                                            @endphp
                                            @if (in_array("qty&rate",UserVoucherTypePermissions($voucher->voucher_type)['permissions']))
                                                @php
                                                    $offset = 'offset-7';
                                                    $col = 'col-md-2';
                                                    $AccountCol = 'col-md-3';
                                                    $EntCol = 'col-md-1';
                                                @endphp
                                            @endif
                                            {{-- <input type="hidden" name="detailId" value="{{$detail->id}}"> --}}
                                            <div class="row d-flex align-items-end entryRow">
                                                <div class="{{$EntCol}} col-12">
                                                    <div class="mb-1">
                                                        <label class="form-label">ENT</label>
                                                        <input type="number" name="voucherEntry[{{$key}}][ent]" class="form-control required ent" placeholder="ENT" value="{{$detail->ent}}" required readonly/>
                                                    </div>
                                                </div>
                                                <div class="{{$AccountCol}} col-12">
                                                    <div class="mb-1">
                                                        <label class="form-label">Accounts</label>
                                                        <select name="voucherEntry[{{$key}}][account_code]" class="form-select select2 required accounts" required disabled>
                                                            <option value="">Select Account</option>
                                                            @foreach(getAccountByID($detail->account_id) as $account)
                                                            <option value="{{ $account->id }}" {{$account->id == $detail->account_id ? 'selected' : ''}}>{{ $account->code }} - {{$account->description}}</option>
                                                            @endforeach
                                                        </select>
                                                    </div>
                                                </div>
                                                @if (in_array("qty&rate",UserVoucherTypePermissions($voucher->voucher_type)['permissions']))
                                                    <div class="col-md-2 col-12">
                                                        <div class="mb-1">
                                                            <label class="form-label">Qty</label>
                                                            <input type="text" name="voucherEntry[{{$key}}][qty]" class="form-control" onkeypress="return (event.charCode !=8 && event.charCode ==0 || (event.charCode >= 48 && event.charCode <= 57))" value="{{number_format($detail->qty)}}" placeholder="Qty" readonly/>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-2 col-12">
                                                        <div class="mb-1">
                                                            <label class="form-label">Rate</label>
                                                            <input type="text" name="voucherEntry[{{$key}}][rate]" class="form-control" onkeypress="return (event.charCode !=8 && event.charCode ==0 || (event.charCode >= 48 && event.charCode <= 57))" value="{{number_format($detail->rate,2)}}" placeholder="Rate" readonly/>
                                                        </div>
                                                    </div>
                                                @endif
                                                {{-- return (event.charCode !=8 && event.charCode ==0 || (event.charCode >= 48 && event.charCode <= 57)) --}}
                                                <div class="{{$col}} col-12">
                                                    <div class="mb-1">
                                                        <label class="form-label">Debit</label>
                                                        <input type="text" name="voucherEntry[{{$key}}][debit]" class="form-control debit numberFiled required"
                                                            placeholder="Debit" oninput="blockFiled(this,'debit')" onkeypress="thousandSeparator(this)" value="{{number_format($detail->debit,2)}}" {{$detail->debit == 0 ? 'readonly' : ''}} required readonly/>
                                                    </div>
                                                </div>
                                                <div class="{{$col}} col-12">
                                                    <div class="mb-1">
                                                        <label class="form-label">Credit</label>
                                                        <input type="text" name="voucherEntry[{{$key}}][credit]" class="form-control credit numberFiled required" placeholder="Credit" oninput="blockFiled(this,'credit')" onkeypress="thousandSeparator(this)" value="{{number_format($detail->credit,2)}}" {{$detail->credit == 0 ? 'readonly' : ''}} required readonly/>
                                                    </div>
                                                </div>
                                                <div class="col-md-8 col-12">
                                                    <div class="mb-1">
                                                        <label class="form-label">Description</label>
                                                        <input type="text" name="voucherEntry[{{$key}}][description]" class="form-control" value="{{$detail->line_remarks}}" placeholder="Description" readonly/>
                                                    </div>
                                                </div>
                                                <div class="col-md-3 col-12 d-flex">
                                                    <div style="margin-bottom: 18px;" class="appendBtn">
                                                        <button class="btn btn-sm btn-outline-primary me-50 editRow" type="button" title="Edit Row" onclick="ResetRow(this)">
                                                            <i class="far fa-edit"></i>
                                                        </button>
                                                        @if ($key == 0)
                                                        <span class="deleteRow"></span>
                                                        @endif
                                                        @if (count($voucher_details)-1==$key)
                                                        <button class="btn btn-sm btn-outline-primary me-50 AddBtn" title="Add New Row" type="button">
                                                            <i data-feather="plus"></i>
                                                        </button>
                                                        @endif
                                                        @if ($key != 0)
                                                        <button class="btn btn-sm btn-outline-danger deleteRow me-50"
                                                        data-repeater-delete type="button" title="Delete" onclick="removeRow(this)">
                                                        <i data-feather="x"></i>
                                                        </button>
                                                        @endif
                                                    </div>  
                                                </div>
                                                <hr>
                                            </div>
                                        @endforeach
                                    </div>
                                    <div class="row">
                                        <div class="col-md-1 col-12">
                                            <h5 style="font-weight:bold;">Total:</h5>
                                        </div>
                                        <div class="{{$offset}} {{$col}} col-12 text-center">
                                            <span style="font-weight:bold;"id="debit">0</span>
                                        </div>
                                        <div class="{{$col}} col-12 text-center">
                                            <span style="font-weight:bold;" id="credit">0</span>
                                        </div>
                                        <hr>
                                    </div>
                                    <div class="d-flex justify-content-end">
                                        <a href="{{ route('vouchers.index') }}" class="btn btn-outline-danger">Cancel</a>
                                        @if(!empty(request('type')))
                                            <button type="button" class="form_save btn btn-primary ms-1" data-bs-toggle="modal" data-bs-target="#myModal">
                                                {{ request('type') }} Voucher
                                              </button>
                                        @else
                                            <button type="submit" class="form_save btn btn-primary ms-1"
                                            id="save">Save</button>
                                        @endif
                                    </div>
                                </div>
                            </div>
                              
                              <!-- The Modal -->
                              <div class="modal fade" id="myModal">
                                <div class="modal-dialog">
                                  <div class="modal-content">
                              
                                    <!-- Modal Header -->
                                    <div class="modal-header">
                                      <h4 class="modal-title">{{request('type')  }} Remarks</h4>
                                      <button type="button" class="btn-close" data-bs-dismiss="modal"></button>
                                    </div>
                              
                                    <!-- Modal body -->
                                    <div class="modal-body">
                                        <div class="mb-1">
                                            <label class="form-label">Remarks</label>
                                            <textarea name="remarks" class="form-control" id="remarks" cols="5" rows="3" placeholder="Enter Remarks"></textarea>
                                        </div>
                                    </div>
                              
                                    <!-- Modal footer -->
                                    <div class="modal-footer">
                                      <button type="submit" class= "form_save btn btn-primary ms-1" id="save">Submit</button>
                                    </div>
                              
                                  </div>
                                </div>
                              </div>
                              
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection
@section('scripts')
    <script src="{{asset('assets/js/scripts.js')}}"></script>
    <script>
        var typeLimit = '{!! UserVoucherTypePermissions($voucher->voucher_type)["type_limit"] !!}';
        var i = '{!! count($voucher_details) !!}';
        var voucherTypePermissions = '{!! json_encode(UserVoucherTypePermissions($voucher->voucher_type)["permissions"]) !!}';
        $(document).ready(function() {
            getAccounts();
            CalculateVoucherAmount();
            $('#date').on('change', function(){
                $('#entryDate').text($(this).val());
            });
            $("#edit_voucher").submit(function(e) {
                e.preventDefault();
                if(checkENT() != true){
                    return false;
                }
                var voucherAmount = CalculateVoucherAmount().split(',');
                // var type_limit = checkVoucherTypeLimit();
                // if(Number(voucherAmount[0]) > typeLimit){
                //     Swal.fire({
                //         icon: 'error',
                //         title: 'Oops',
                //         text: 'Your voucher limit excedeed!',
                //     });
                //     return false;
                // }
                ButtonStatus('#save',true);
                blockUI();
                $('.entryRow .select2').prop('disabled',false);
                var formData = new FormData(this);
                formData.append('debit_amount', voucherAmount[0]);
                formData.append('credit_amount', voucherAmount[1]);
                formData.append('type_limit', typeLimit);
                $.ajax({
                    url: "{{ url('vouchers') }}" + "/" + '{!! $voucher->id !!}',
                    type: "POST",
                    data: formData,
                    processData: false,
                    contentType: false,
                    responsive: true,
                    success: function(response) {
                        ButtonStatus('#save',false);
                        $.unblockUI();
                        if (response.errors) {
                            $.each(response.errors, function(index, value) {
                                Toast.fire({
                                    icon: 'error',
                                    title: value
                                })
                            });
                        }
                        else if (response.customMessage) {
                            Swal.fire({
                                icon: 'error',
                                title: 'Oops',
                                text: response.customMessage,
                            });
                        }
                        else if (response.error_message) {
                            Toast.fire({
                                icon: 'error',
                                title: 'An error has been occured! Please Contact Administrator.'
                            })
                        } 
                        else {
                            Toast.fire({
                                icon: 'success',
                                title: 'Voucher has been Updated Successfully!'
                            });
                            setTimeout(function(){
                                window.location.href = "{{ url('vouchers') }}";
                            }, 1000);
                        }
                    }
                });
            });
        })

        $(document).on('click','.AddBtn',function(){
            if(checkEmpty(this) != true){
                return false;
            }
            if(checkENT() != true){
                return false;
            }
            lockRow(this);
            appendRow(JSON.parse(voucherTypePermissions))
            $(this).closest('.appendBtn').replaceWith(`
            <div style="margin-bottom: 19px;" class="appendBtn">
                <button class="btn btn-sm btn-outline-primary me-50 editRow" type="button" title="Edit Row" onclick="ResetRow(this)">
                        <i class="far fa-edit"></i>
                </button>
                <button class="btn btn-sm btn-outline-danger deleteRow" type="button" title="Delete" onclick="removeRow(this)">
                    <i data-feather="x"></i>
                </button>
            </div>`);
            if (feather) {
                feather.replace({
                    width: 14,
                    height: 14
                });
            }
        });
    </script>
@endsection