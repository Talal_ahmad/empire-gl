@extends('admin.layouts.master')
@section('title', 'History Deatil')
@section('content')
<section class="content-wrapper container-xxl p-0">
    <div class="content-header row">
        <div class="content-header-left col-md-9 col-12 mb-2">
            <div class="row breadcrumbs-top">
                <div class="col-12">
                    <h2 class="content-header-title float-start mb-0">History Details</h2>
                    <div class="breadcrumb-wrapper">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="{{ url('/dashboard') }}">Dashboard</a>
                            </li>
                            <li class="breadcrumb-item"><a href="{{ url('/VoucherHistory') }}">Voucher History</a>
                            </li>
                            <li class="breadcrumb-item active"> History Details
                            </li>
                        </ol>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="content-body">
        <div class="row">
            <div class="col-md-6 col-6">
                <div class="card p-2">
                    <h4 style="font-weight:bold">New Values</h4>
                    <hr>
                    @if (isset($properties['attributes']) && count($properties['attributes']) > 0)
                    <div class="row">
                        @foreach ($properties['attributes'] as $key => $item)
                            @php
                                if($key=='attachments'){
                                    continue;
                                }
                                if($key=='vouchertype.voucher_type'){
                                    $key='Voucher Type';
                                }
                                if ($key=='financialyear.fiscal_year') {
                                    $key='Fiscal Year';
                                }
                                if ($key=='location.location_name'){
                                    $key='Location';
                                }
                            @endphp
                            @if ($key=='general_remarks')
                            <div class="col-12">
                                <h5>{{ucwords(str_replace('_',' ',$key))}}:</h5>
                                <p>{{ !empty($item) ? $item : '-' }}</p>
                            </div>
                            @else
                            <div class="col-md-6 col-lg-4 col-12">
                                <h5>{{ucwords(str_replace('_',' ',$key))}}:</h5>
                                @if ($key=='status' && $item==1)
                                    <p><span class="badge bg-primary">Created</span></p>
                                @elseif($key=='status' && $item==2)
                                    <p><span class="badge bg-secondary">Checked</span></p>
                                @elseif($key=='status' && $item==3)
                                    <p><span class="badge bg-info">Verified</span></p>
                                @elseif($key=='status' && $item==4)
                                    <p><span class="badge bg-success">Approved</span></p>
                                @else
                                    <p>{{ !empty($item) ? $item : '-' }}</p>
                                @endif
                            </div>
                            @endif
                        @endforeach
                    </div>
                    @else
                    <div style="text-align: center;font-size:20px">No New Values</div>
                    @endif
                </div>
            </div>

            <div class="col-md-6 col-6">
                <div class="card p-2">
                    <h4 style="font-weight:bold">Old Values</h4>
                    <hr>
                    @if (isset($properties['old']) && count($properties['old']) > 0)
                    <div class="row">
                        @foreach ($properties['old'] as $key => $item)
                            @php
                                if($key=='vouchertype.voucher_type'){
                                    $key='Voucher Type';
                                }
                                if ($key=='financialyear.fiscal_year') {
                                    $key='Fiscal Year';
                                }
                                if ($key=='location.location_name'){
                                    $key='Location';
                                }
                            @endphp
                            @if ($key=='general_remarks')
                            <div class="col-12">
                                <h5>{{ucwords(str_replace('_',' ',$key))}}:</h5>
                                <p>{{ !empty($item) ? $item : '-' }}</p>
                            </div>
                            @else
                            <div class="col-md-6 col-lg-4 col-12">
                                <h5>{{ucwords(str_replace('_',' ',$key))}}:</h5>
                                @if ($key=='status' && $item==1)
                                    <p><span class="badge bg-primary">Created</span></p>
                                @elseif($key=='status' && $item==2)
                                    <p><span class="badge bg-secondary">Checked</span></p>
                                @elseif($key=='status' && $item==3)
                                    <p><span class="badge bg-info">Verified</span></p>
                                @elseif($key=='status' && $item==4)
                                    <p><span class="badge bg-success">Approved</span></p>
                                @else
                                    <p>{{ !empty($item) ? $item : '-' }}</p>
                                @endif
                            </div>
                            @endif
                        @endforeach
                    </div>
                    @else
                    <div style="text-align: center;font-size:20px">No Old Values</div>
                    @endif
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-12">
                <div class="card pb-2">
                    <table class="table" id="dataTable">
                        <thead>
                            <tr>
                                <th class="not_include"></th>
                                <th>Sr.No</th>
                                <th>Event</th>
                                <th>ENT</th>
                                <th>Account</th>
                                <th>Qty</th>
                                <th>Rate</th>
                                <th>Debit</th>
                                <th>Credit</th>
                                <th>Remarks</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach (getLogsByUuid($log->id,$log->batch_uuid) as $key => $detail)
                                @if ($detail->event == 'deleted')
                                <tr>
                                    <td></td>
                                    <td>{{$key+1}}</td>
                                    <td><span class="badge bg-danger">{{ucfirst($detail->event)}}</span></td>
                                    <td>{{$detail->properties['old']['ent']}}</td>
                                    <td>{{$detail->properties['old']['account.code']}} ({{!empty($detail->properties['old']['account.name']) ? $detail->properties['old']['account.name'] : $detail->properties['old']['account.gl_type']}})</td>
                                    <td>{{!empty($detail->properties['old']['qty']) ? $detail->properties['old']['qty'] : '-'}}</td>
                                    <td>{{!empty($detail->properties['old']['rate']) ? $detail->properties['old']['rate'] : '-'}}</td>
                                    <td>{{$detail->properties['old']['amount'] > 0 ? number_format($detail->properties['old']['amount'],2) : '-'}}</td>
                                    <td>{{$detail->properties['old']['amount'] < 0 ? number_format(abs($detail->properties['old']['amount']),2) : '-'}}</td>
                                    <td>{{!empty($detail->properties['old']['line_remarks']) ? $detail->properties['old']['line_remarks'] : '-'}}</td>
                                </tr>
                                @else
                                <tr>
                                    <td></td>
                                    <td>{{$key+1}}</td>
                                    <td><span class="badge bg-primary">{{ucfirst($detail->event)}}</span></td>
                                    <td>{{$detail->properties['attributes']['ent']}}</td>
                                    <td>{{$detail->properties['attributes']['account.code']}} ({{!empty($detail->properties['attributes']['account.name']) ? $detail->properties['attributes']['account.name'] : $detail->properties['attributes']['account.gl_type']}})</td>
                                    <td>{{!empty($detail->properties['attributes']['qty']) ? $detail->properties['attributes']['qty'] : '-'}}</td>
                                    <td>{{!empty($detail->properties['attributes']['rate']) ? $detail->properties['attributes']['rate'] : '-'}}</td>
                                    <td>{{$detail->properties['attributes']['amount'] > 0 ? number_format($detail->properties['attributes']['amount'],2) : '-'}}</td>
                                    <td>{{$detail->properties['attributes']['amount'] < 0 ? number_format(abs($detail->properties['attributes']['amount']),2) : '-'}}</td>
                                    <td>{{!empty($detail->properties['attributes']['line_remarks']) ? $detail->properties['attributes']['line_remarks'] : '-'}}</td>
                                </tr>
                                @endif
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection
@section('scripts')
    <script>
        $(document).ready(function(){
            $('html, body').animate({
                scrollTop: $('#dataTable').offset().top
            }, 0);
            dataTable = $('#dataTable').DataTable({
                ordering:false,
                "columnDefs": [
                    {
                        // For Responsive
                        className: 'control',
                        orderable: false,
                        searchable: false,
                        targets: 0
                    },
                ],
                "order": [
                    [0, 'asc']
                ],
                dom: '<"card-header border-bottom p-1"<"head-label"><"dt-action-buttons text-end"B>><"d-flex justify-content-between align-items-center mx-0 row"<"col-sm-12 col-md-6"l><"col-sm-12 col-md-6"f>>t<"d-flex justify-content-between mx-0 row"<"col-sm-12 col-md-6"i><"col-sm-12 col-md-6"p>>',
                displayLength: 10,
                lengthMenu: [10, 25, 50, 75, 100],
                buttons: [{
                        extend: 'collection',
                        className: 'btn btn-outline-secondary dropdown-toggle me-2',
                        text: feather.icons['share'].toSvg({
                            class: 'font-small-4 me-50'
                        }) + 'Export',
                        buttons: [{
                                extend: 'print',
                                footer: true,
                                text: feather.icons['printer'].toSvg({
                                    class: 'font-small-4 me-50'
                                }) + 'Print',
                                className: 'dropdown-item',
                                exportOptions: {
                                    columns: ':not(.not_include)'
                                }
                            },
                            {
                                extend: 'csv',
                                footer: true,
                                text: feather.icons['file-text'].toSvg({
                                    class: 'font-small-4 me-50'
                                }) + 'Csv',
                                className: 'dropdown-item',
                                exportOptions: {
                                    columns: ':not(.not_include)'
                                }
                            },
                            {
                                extend: 'excel',
                                footer: true,
                                text: feather.icons['file'].toSvg({
                                    class: 'font-small-4 me-50'
                                }) + 'Excel',
                                className: 'dropdown-item',
                                exportOptions: {
                                    columns: ':not(.not_include)'
                                }
                            },
                            {
                                extend: 'pdf',
                                footer: true,
                                text: feather.icons['clipboard'].toSvg({
                                    class: 'font-small-4 me-50'
                                }) + 'Pdf',
                                className: 'dropdown-item',
                                exportOptions: {
                                    columns: ':not(.not_include)'
                                }
                            },
                            {
                                extend: 'copy',
                                footer: true,
                                text: feather.icons['copy'].toSvg({
                                    class: 'font-small-4 me-50'
                                }) + 'Copy',
                                className: 'dropdown-item',
                                exportOptions: {
                                    columns: ':not(.not_include)'
                                }
                            }
                        ],
                        init: function(api, node, config) {
                            $(node).removeClass('btn-secondary');
                            $(node).parent().removeClass('btn-group');
                            setTimeout(function() {
                                $(node).closest('.dt-buttons').removeClass('btn-group')
                                    .addClass('d-inline-flex');
                            }, 50);
                        }
                    }
                ],
                responsive: {
                    details: {
                        display: $.fn.dataTable.Responsive.display.childRowImmediate,
                        type: 'column',
                    }
                },
                language: {
                    paginate: {
                        // remove previous & next text from pagination
                        previous: '&nbsp;',
                        next: '&nbsp;'
                    }
                },
            });
            $('div.head-label').html('<h6 class="mb-0">Entry Details</h6>');
        });
    </script>
@endsection