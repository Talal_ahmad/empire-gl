<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DataTables;
use App\Models\Financialyear;
use Illuminate\Support\Facades\DB;
use Illuminate\Validation\ValidationException;

class FinancialyearController extends Controller
{
    public function __construct(){
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if($request->ajax()){
            // dd($request);
            return DataTables::eloquent(Financialyear::query())->make(true);
        }
        return view('admin.FiscalYear.index',get_defined_vars());
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // dd($request);
        try{
            $this->validate($request , [
                'fiscal_year' => 'required',
                'start_date' => 'required',
                'end_date' => 'required',
            ]);
            $financial_year = new Financialyear();
            $financial_year->fiscal_year = $request->fiscal_year;
            $financial_year->start_date = $request->start_date;
            $financial_year->end_date = $request->end_date;
            $financial_year->open_close = isset($request->open_close) ? $request->open_close : 'Close';
            $financial_year->status = isset($request->status) ? $request->status : 'Inactive';
            $financial_year->save();
            return ['code'=>'200','message'=>'success'];
        }
        catch(\Exception | ValidationException $e){
            if($e instanceof ValidationException){
                return ['code'=>'422','errors' => $e->errors()];
            }
            else{
                return ['code'=>'500','error_message'=>$e->getMessage()];
            }
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        // dd($id);
        $financial_year = Financialyear::find($id);
        return response([
            'financial_year' => $financial_year,
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        try{
            $financial_year = Financialyear::find($id);
            $financial_year->fiscal_year = $request->edit_fiscal_year;
            $financial_year->start_date = $request->edit_start_date;
            $financial_year->end_date = $request->edit_end_date;
            $financial_year->open_close = isset($request->open_close) ? $request->open_close : 'Close';
            $financial_year->status = isset($request->status) ? $request->status : 'Inactive';
            $financial_year->update();
            return ['code'=>'200','message'=>'success'];
        }
        catch(\Exception | ValidationException $e){
            if($e instanceof ValidationException){
                return ['code'=>'422','errors' => $e->errors()];
            }
            else{
                return ['code'=>'500','error_message'=>$e->getMessage()];
            }
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try{
            $financial_year = Financialyear::find($id);
            $financial_year->delete();
            return ['code'=>'200','message'=>'success'];
        }
        catch(\Exception $e){
            return ['code'=>'500','error_message'=>$e->getMessage()];
        }
    }
}
