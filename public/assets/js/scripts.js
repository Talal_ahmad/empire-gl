// (function (window, undefined) {
//   'use strict';

  /*
  NOTE:
  ------
  PLACE HERE YOUR OWN JAVASCRIPT CODE IF NEEDED
  WE WILL RELEASE FUTURE UPDATES SO IN ORDER TO NOT OVERWRITE YOUR JAVASCRIPT CODE PLEASE CONSIDER WRITING YOUR SCRIPT HERE.  */

function appendRow(voucherTypePermissions,totalColumn=''){
    var offset = 'offset-5';
    var col = 'col-md-3';
    var AccountCol = 'col-md-4';
    var EntCol = 'col-md-2';
    if(voucherTypePermissions.length > 0){
        if(voucherTypePermissions.includes("qty&rate")){
            offset = 'offset-7';
            EntCol = 'col-md-1';
            AccountCol = 'col-md-3';
            col = 'col-md-2';
        }
    }
    var html = `
        <div class="row d-flex align-items-end entryRow">
            <div class="${EntCol} col-12 pe-0">
                <div class="mb-1">
                    <label class="form-label">ENT</label>
                    <input type="number" name="voucherEntry[${i}][ent]" class="form-control required ent"
                        placeholder="ENT" required/>
                </div>
            </div>
            <div class="${AccountCol} col-12">
                <div class="mb-1">
                    <label class="form-label">Account Codes</label>
                    <select name="voucherEntry[${i}][account_code]" class="form-select select2 accounts required" required></select>
                </div>
            </div>`;
            if(voucherTypePermissions.length > 0){
                if(voucherTypePermissions.includes("qty&rate")){
                    html += `
                    <div class="col-md-2 col-12">
                        <div class="mb-1">
                            <label class="form-label">Qty</label>
                            <input type="number" name="voucherEntry[${i}][qty]" class="form-control"
                                placeholder="Qty"/>
                        </div>
                    </div>
                    <div class="col-md-2 col-12">
                        <div class="mb-1">
                            <label class="form-label">Rate</label>
                            <input type="number" name="voucherEntry[${i}][rate]" class="form-control"
                                placeholder="Rate"/>
                        </div>
                    </div>`;
                }
            }
            html += `<div class="${col} col-12">
                <div class="mb-1">
                    <label class="form-label">Debit</label>
                    <input type="text" name="voucherEntry[${i}][debit]" class="form-control debit numberFiled required"
                        placeholder="Debit" oninput="blockFiled(this,'debit')" onkeypress="thousandSeparator(this)" required/>
                </div>
            </div>
            <div class="${col} col-12">
                <div class="mb-1">
                    <label class="form-label">Credit</label>
                    <input type="text" name="voucherEntry[${i}][credit]" class="form-control credit numberFiled required"
                        placeholder="Credit" oninput="blockFiled(this,'credit')" onkeypress="thousandSeparator(this)" required/>
                </div>
            </div>
            <div class="col-md-8 col-12">
                <div class="mb-1">
                    <label class="form-label">Description</label>
                    <input type="text" name="voucherEntry[${i}][description]" class="form-control"
                        placeholder="Description"/>
                </div>
            </div>
            <div class="col-md-3 col-12 d-flex">
                <div style="margin-bottom: 19px;" class="appendBtn">
                    <button class="btn btn-sm btn-outline-success text-nowrap me-50 resetBtn" type="button" title="Reset debit & credit" onclick="resetFileds(this)">
                        <i data-feather="refresh-ccw"></i>
                    </button>
                    <button class="btn btn-sm btn-outline-primary AddBtn me-50" title="Add New Row" type="button">
                        <i data-feather="plus"></i>
                    </button>`;
                    if(i != 0){
                        html += `<button class="btn btn-sm btn-outline-danger me-50 deleteRow" type="button" title="Delete" onclick="removeRow(this)">
                                    <i data-feather="x"></i>
                                </button>`;
                    }
                    html += `
                </div>
            </div>
            <hr>
        </div>`;
    if(totalColumn == 'yes'){
        var Addhtml = `
        <div class="row">
            <div class="col-md-1 col-12">
                <h5>Total:</h5>
            </div>
            <div class="${offset} ${col} col-12 text-center">
                <span id="debit">0</span>
            </div>
            <div class="${col} col-12 text-center">
                <span id="credit">0</span>
            </div>
            <hr>
        </div>`;
        $('#appendTotalColumns').html(Addhtml);
    }
    $('#voucher_entry').append(html);
    getAccounts();
    if (feather) {
        feather.replace({
            width: 14,
            height: 14
        });
    }
    i++;
}

function getAccounts(trial = ''){
    $(".accounts").select2({
        ajax: {
            url: window.baseUrl+'/getAccounts',
            type: "get",
            delay: 250,
            data: function(params) {
                return {
                    search: params.term,
                    trial: trial
                };
            },
            processResults: function(response) {
                return {
                    results: response
                }
            },
            cache: true
        },
        placeholder: "Select Account",
        allowClear: true,
    });
}

function checkVoucherTypeLimit(){
    var typeLimit = 0;
    var voucherTypeId = $('#voucher_type').val();
    $.ajax({
        url: window.baseUrl+'/vouchertype/'+voucherTypeId,
        type: "GET",
        async:false,
        success: function(response) {
            typeLimit =  response.type_limit;
        }
    });
    return typeLimit;
}
function blockFiled(object,value){
    var name = object.name;
    CalculateVoucherAmount();
    if(value == 'debit'){
        name = name.replace('debit','credit');
    }
    else{
        name = name.replace('credit','debit');
    }
    $("input:text[name='"+name+"']").attr('readonly', true);
}

function thousandSeparator(object){
    new Cleave(object, {
        numeral: true,
        numeralPositiveOnly: true,
        numeralDecimalScale: 2,
        numeralThousandsGroupStyle: 'thousand'
    });
}

function resetFileds(object){
    var targetFields = $(object).closest(".entryRow").find('.numberFiled');
    $(targetFields).each(function(i, obj) {
        $(obj).attr('readonly', false);
        $(obj).val('');
    });
    CalculateVoucherAmount();
}

function removeRow(object){
    Swal.fire({
        title: 'Are you sure?',
        text: "You won't be able to revert this!",
        icon: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Yes, delete it!'
    }).then((result) => {
        if (result.isConfirmed) {
            $(object).closest(".entryRow").remove();
            CalculateVoucherAmount();
            $('.entryRow div').last().find('.AddBtn').remove();
            $(`<button class="btn btn-sm btn-outline-primary AddBtn me-50" title="Add New Row" type="button">
                <i data-feather="plus"></i>
            </button>`).insertBefore($('.entryRow div').last().find('.deleteRow'));
            if (feather) {
                feather.replace({
                    width: 14,
                    height: 14
                });
            }
        }
    })
}

function CalculateVoucherAmount() {
    var debit = 0.00;
    var credit = 0.00;
    $('.voucher_entry .entryRow').each(function(i, obj) {
        var creditValue = $(obj).find('.credit').val();
        var debitValue = $(obj).find('.debit').val();
        debit = parseFloat(debit) + Number(numberWithoutCommas(debitValue));
        credit = parseFloat(credit) + Number(numberWithoutCommas(creditValue));
    });
    $('#debit').text(numberWithCommas(debit.toFixed(2)));
    $('#credit').text(numberWithCommas(credit.toFixed(2)));
    return debit+','+credit;
}
// function CalculateVoucherAmount() {
//     var debit = 0.00;
//     var credit = 0.00;
//     $('.voucher_entry .entryRow').each(function(i, obj) {
//         var creditValue = $(obj).find('.credit').val();
//         var debitValue = $(obj).find('.debit').val();
//         debitValue = parseFloat(numberWithoutCommas(debitValue));
//         creditValue = parseFloat(numberWithoutCommas(creditValue));
//         debit = parseFloat(debit) + debitValue;
//         credit = parseFloat(credit) + creditValue;
//     });
//     $('#debit').text(debit.toFixed(2));
//     $('#credit').text(credit.toFixed(2));
//     return debit+','+credit;
// }

function checkEmpty(object){
    var count = 0;
    // $('.voucher_entry .required').each(function(i, obj) {
    //     var value = $(obj).val();
    //     if((value == '' || value == 0) && !$(obj).is('[readonly]')){
    //         count++;
    //     }
    // });
    $(object).closest('.entryRow').find('.required').each(function(i, obj) {
        var value = $(obj).val();
        if((value == '' || value == 0) && !$(obj).is('[readonly]')){
            count++;
        }
    });
    if(count > 0){
        return Swal.fire({
            icon: 'error',
            title: 'Oops',
            text: 'Please fill all fields!',
        });
    }
    return true;
}

function checkENT(){
    var count = 0;
    var entArray = [];
    $('.voucher_entry .ent').each(function(i, obj) {
        if(entArray.indexOf($(obj).val()) != -1){
            count++;
        }
        entArray.push($(obj).val());
    });
    if(count > 0){
        return Swal.fire({
            icon: 'error',
            title: 'Oops',
            text: 'ENT has already been taken!',
        });
    }
    return true;
}

function lockRow(object,edit=''){
    if(checkEmpty(object) != true){
        return false;
    }
    $(object).closest(".entryRow").find('input').attr('readonly','readonly');
    $(object).closest(".entryRow").find('.select2').prop('disabled',true);
    if(edit=='yes'){
        $(`<button class="btn btn-sm btn-outline-primary me-50 editRow" type="button" title="Edit Row" onclick="ResetRow(this)">
                <i class="far fa-edit"></i>
        </button>`).insertBefore($(object).closest('.appendBtn').find('.deleteRow'));
        $(object).closest('.appendBtn').find('.resetBtn').remove();
        $(object).remove();
    }
    $('.AddBtn').prop('disabled',false);
    $('.deleteRow').prop('disabled',false);
    $('.editRow').prop('disabled',false);
    $('.resetBtn').prop('disabled',false);
    if (feather) {
        feather.replace({
            width: 14,
            height: 14
        });
    }
}

function ResetRow(object){
    $(object).closest(".entryRow").find('input').attr('readonly', false);
    $(object).closest(".entryRow").find('.select2').prop('disabled',false);
    var debit = $(object).closest(".entryRow").find('.debit');
    var credit = $(object).closest(".entryRow").find('.credit');
    if(numberWithoutCommas($(debit).val()) > 0){
        credit.attr('readonly',true);
    }
    else{
        debit.attr('readonly',true);
    }
    $('.AddBtn').prop('disabled',true);
    $('.deleteRow').prop('disabled',true);
    $('.editRow').prop('disabled',true);
    $('.resetBtn').prop('disabled',true);
    $(`<button class="btn btn-sm btn-outline-success text-nowrap me-50 resetBtn" type="button" title="Reset debit & credit" onclick="resetFileds(this)">
    <i data-feather="refresh-ccw"></i>
</button>
<button class="btn btn-sm btn-outline-primary me-50" type="button" title="Update Row" onclick="lockRow(this,'yes')">
    <i class="fas fa-check"></i>
</button>`).insertBefore($(object).closest('.appendBtn').find('.deleteRow'));
    // $(object).closest('.appendBtn').replaceWith(`
    // <div style="margin-bottom: 19px;" class="appendBtn">
    //     <button class="btn btn-sm btn-outline-success text-nowrap me-50 resetBtn" type="button" title="Reset debit & credit" onclick="resetFileds(this)">
    //         <i data-feather="refresh-ccw"></i>
    //     </button>
    //     <button class="btn btn-sm btn-outline-primary me-50" type="button" title="Update Row" onclick="lockRow(this,'yes')">
    //         <i class="fas fa-check"></i>
    //     </button>
    //     <button class="btn btn-sm btn-outline-danger deleteRow disabled" type="button" title="Delete" onclick="removeRow(this)">
    //         <i data-feather="x"></i>
    //     </button>
    // </div>
    // `);
    $(object).remove();
    if (feather) {
        feather.replace({
            width: 14,
            height: 14
        });
    }
}
// })(window);
