<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\Traits\LogsActivity;
use Spatie\Activitylog\LogOptions;

class Vouchertype extends Model
{
    use HasFactory,LogsActivity;
    protected $table = 'vouhertypes';

    public function getActivitylogOptions(): LogOptions
    {
        $login_user = \Auth::user()->name;
        return LogOptions::defaults()
        ->logOnly(['voucher_type','subType','description'])
        ->useLogName('VoucherType')
        ->logOnlyDirty()
        ->setDescriptionForEvent(fn(string $eventName) => "{$login_user} have {$eventName} Voucher Type");
    }


    public function vouchers()
    {
        return $this->hasMany(Voucher::class);
    }

    public function voucherdetails()
    {
        return $this->hasMany(VoucherDetail::class);
    }
}
