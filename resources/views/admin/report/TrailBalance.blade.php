@extends('admin.layouts.master')
@section('title', ' Trial Balance Report')
@section('style')
    <style>
        tr,
        td,
        th {
            border: 1px solid black;
            padding: 2px 5px !important;
        }

        thead tr>th {
            vertical-align: middle !important;
        }
    </style>
@endsection
@section('content')
    <section class="content-wrapper container-xxl p-0">
        <div class="content-header row">
            <div class="content-header-left col-md-9 col-12 mb-2">
                <div class="row breadcrumbs-top">
                    <div class="col-12">
                        <h2 class="content-header-title float-start mb-0">Trial Balance Report</h2>
                        <div class="breadcrumb-wrapper">
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="{{ url('/') }}">Dashboard</a>
                                </li>
                                <li class="breadcrumb-item"><a href="#">Reports</a>
                                </li>
                                <li class="breadcrumb-item active">Trial Balance Report
                                </li>
                            </ol>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="content-body">
            <section>
                <div class="row">
                    <div class="col-12">
                        <div class="card mb-1">
                            <!--Search Form -->
                            <div class="card-body">
                                <form id="search_form" action="{{ url('TrailBalance') }}" method="GET">
                                    <div class="row">
                                        <div class="col-md-3">
                                            <label class="form-label" for="date">From Date</label>
                                            <input type="date" id="date" class="form-control" name="fromDate"
                                                value="{{ !empty(request('fromDate')) ? request('fromDate') : date('Y-m-d') }}"
                                                required />
                                        </div>
                                        <div class="col-md-3">
                                            <label class="form-label" for="date">To Date</label>
                                            <input type="date" id="date" class="form-control" name="toDate"
                                                value="{{ !empty(request('toDate')) ? request('toDate') : date('Y-m-d') }}"
                                                required />
                                        </div>
                                        <div class="col-md-3">
                                            <label class="form-label" for="fromRange">From Range</label>
                                            <select name="fromRange" id="fromRange" class="select2 form-select accounts"
                                                data-placeholder="Select From range" data-flag="trialBalance">
                                                <option value=""></option>
                                            </select>
                                        </div>
                                        <div class="col-md-3">
                                            <label class="form-label" for="toRange">To Range</label>
                                            <select name="toRange" id="toRange" class="select2 form-select accounts"
                                                data-placeholder="Select ToRange">
                                                <option value=""></option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-1">
                                            <div class="form-check pt-1 ">
                                                <input class="form-check-input" type="checkbox" id="summary"
                                                    name="summary" value="1"
                                                    {{ request('summary') == 1 ? 'checked' : '' }}>
                                                <label class="form-check-label">Summary</label>
                                            </div>
                                        </div>
                                        <div class="col-md-2">
                                            <div class="form-check pt-1 ">
                                                <input class="form-check-input" type="checkbox" id="zero"
                                                    name="zero" value="1"
                                                    {{ request('zero') == 1 ? 'checked' : '' }}>
                                                <label class="form-check-label">Suppres Zero</label>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-6 col-12">
                                            <button onclick="divToPrint()"
                                                class="btn btn-primary mt-1 printre">Print</button>
                                        </div>
                                        <div class="col-md-6 text-end">
                                            <a href="{{ url('TrailBalance') }}" type="button"
                                                class="btn btn-danger mt-1">Reset</a>
                                            <button type="submit" class="btn btn-primary mt-1">Apply</button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                        <div class="mb-1">
                            <div class="dt-action-buttons text-end">
                                <div class="dt-buttons d-inline-flex">
                                    <a href="{{ url('TrailBalance') }}?type=excel&fromDate={{ request('fromDate') }}&toDate={{ request('toDate') }}&fromRange={{ request('fromRange') }}&toRange={{ request('toRange') }}&summary={{ request('summary') }}&zero={{ request('zero') }}"
                                        class="btn btn-success">Download Excel</a>
                                    <a href="{{ url('TrailBalance') }}?type=pdf&fromDate={{ request('fromDate') }}&toDate={{ request('toDate') }}&fromRange={{ request('fromRange') }}&toRange={{ request('toRange') }}&summary={{ request('summary') }}&zero={{ request('zero') }}"
                                        class="btn btn-danger ms-1">Download Pdf</a>
                                </div>
                            </div>
                        </div>
                        <div class="card p-2 table-responsive" id="spot1">
                            <div id="div_to_print">
                                <div class="card">
                                    <div class="row pb-2">
                                        <div class="col-12">
                                            <h2 class="text-center fw-bolder text-decoration-underline text-uppercase">
                                                {{ env('COMPANY') }}</h2>
                                        </div>
                                        <div class="col-12">
                                            <h6 class="text-center fw-bolder text-decoration-underline text-uppercase">Trial
                                                Balance From
                                                {{ !empty(request('fromDate')) ? request('fromDate') : date('d-m-Y') }} To
                                                {{ !empty(request('toDate')) ? request('toDate') : date('d-m-Y') }}</h6>
                                        </div>
                                        <div class="col-12">
                                            <h6 class="text-center fw-bolder text-decoration-underline text-uppercase">Print Date
                                                {{ date('d-m-Y  h:i:sa') }}
                                        </div>
                                    </div>
                                    <table class="table table-bordered table-striped" id="dataTable">
                                        <thead>
                                            <tr>
                                                <th rowspan="2">Account </th>
                                                <th rowspan="2">Group</th>
                                                @if (request('summary') != 1)
                                                    <th colspan="2" class="text-center">Opening</th>
                                                    <th colspan="2" class="text-center">Transaction</th>
                                                @endif
                                                <th colspan="2" class="text-center">Closing Balance</th>
                                            </tr>
                                            <tr>
                                                @if (request('summary') != 1)
                                                    <th style="padding: 0; text-align:center;">Debit</th>
                                                    <th style="padding: 0; text-align:center;">Credit</th>
                                                    <th style="padding: 0; text-align:center;">Debit</th>
                                                    <th style="padding: 0; text-align:center;">Credit</th>
                                                @endif
                                                <th style="padding: 0; text-align:center;">Debit</th>
                                                <th style="padding: 0; text-align:center;">Credit</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @php
                                                $totalOpeningDebitBalance = 0;
                                                $totalOpeningCreditsBalance = 0;
                                                $totalClosingDebitBalance = 0;
                                                $totalClosingCreditBalance = 0;
                                                $totalTranscationDebit = 0;
                                                $totalTranscationCredit = 0;
                                            @endphp
                                            @forelse($levels as $level)
                                                @if (count($level->level2) > 0)
                                                @php
                                                    $totalLevel1OpeningDebitBalance = 0;
                                                    $totalLevel1OpeningCreditsBalance = 0;
                                                    $totalLevel1ClosingDebitBalance = 0;
                                                    $totalLevel1ClosingCreditBalance = 0;
                                                    $totalLevel1TranscationDebit = 0;
                                                    $totalLevel1TranscationCredit = 0;
                                                @endphp
                                                    <tr>
                                                        <th>{{ $level->code }}</th>
                                                        <th colspan="7">{{ $level->description }}</th>
                                                    </tr>
                                                    @foreach ($level->level2 as $level2)
                                                        @if (count($level2->accounts) > 0)
                                                            <tr>
                                                                <th>{{ $level2->code }}</th>
                                                                <th colspan="7">{{ $level2->description }}</th>
                                                            </tr>
                                                            @php
                                                                $level2OpeningDebitBalance = 0;
                                                                $level2OpeningCreditsBalance = 0;
                                                                $level2ClosingDebitBalance = 0;
                                                                $level2ClosingCreditBalance = 0;
                                                                $level2TranscationDebit = 0;
                                                                $level2TranscationCredit = 0;
                                                            @endphp
                                                            @foreach ($level2->accounts as $account)
                                                                @php
                                                                    $check = false;
                                                                    if(count($account->voucherdetails) > 0){
                                                                        // if(request('summary') == 1){
                                                                        //     $check = request('zero') == 1 && $account->voucherdetails[0]['openingDebitBalance'] == 0 && $account->voucherdetails[0]['openingCreditsBalance'] == 0 && $account->voucherdetails[0]['closingDebitBalance'] == 0 && $account->voucherdetails[0]['closingCreditBalance'] == 0 && $account->voucherdetails[0]['transcationDebit'] == 0 && $account->voucherdetails[0]['transcationCredit'] == 0;
                                                                        // }
                                                                        // else {
                                                                            $check = request('zero') == 1 && $account->voucherdetails[0]['openingDebitBalance'] == 0 && $account->voucherdetails[0]['openingCreditsBalance'] == 0 && $account->voucherdetails[0]['closingDebitBalance'] == 0 && $account->voucherdetails[0]['closingCreditBalance'] == 0 && $account->voucherdetails[0]['transcationDebit'] == 0 && $account->voucherdetails[0]['transcationCredit'] == 0;
                                                                        // }
                                                                    }
                                                                @endphp
                                                                @if ($check == false && count($account->voucherdetails) > 0)
                                                                    <tr>
                                                                        <td>{{ $account->code }}</td>
                                                                        <td>{{ $account->description }}</td>
                                                                        @php
                                                                            $openingBalance = 0;
                                                                            $accountClass = checkAccountClass($level->account_class_id);
                                                                            if ($accountClass->name == 'Expenses' || $accountClass->name == 'Cost' || $accountClass->name == 'Income') {
                                                                                $openingBalance = openingBalance($account->id, request('fromDate'), true);
                                                                                $closingBalance = $openingBalance + $account->voucherdetails[0]['transcationDebit'] - abs($account->voucherdetails[0]['transcationCredit']);
                                                                            } else {
                                                                                $openingBalance = $account->voucherdetails[0]['openingDebitBalance'] + $account->voucherdetails[0]['openingCreditsBalance'];
                                                                                $closingBalance = $openingBalance + $account->voucherdetails[0]['transcationDebit'] - abs($account->voucherdetails[0]['transcationCredit']);
                                                                            }
                                                                            if ($openingBalance < 0) {
                                                                                $level2OpeningCreditsBalance += abs($openingBalance);
                                                                                $totalLevel1OpeningCreditsBalance += abs($openingBalance);
                                                                                $totalOpeningCreditsBalance += abs($openingBalance);
                                                                            } else {
                                                                                $level2OpeningDebitBalance += $openingBalance;
                                                                                $totalLevel1OpeningDebitBalance += $openingBalance;
                                                                                $totalOpeningDebitBalance += $openingBalance;
                                                                            }
                                                                            if ($closingBalance < 0) {
                                                                                $totalClosingCreditBalance += abs($closingBalance);
                                                                                $totalLevel1ClosingCreditBalance += abs($closingBalance);
                                                                                $level2ClosingCreditBalance += abs($closingBalance);
                                                                            } else {
                                                                                $totalClosingDebitBalance += $closingBalance;
                                                                                $totalLevel1ClosingDebitBalance += $closingBalance;
                                                                                $level2ClosingDebitBalance += $closingBalance;
                                                                            }
                                                                            // $totalClosingDebitBalance += $account->voucherdetails[0]['closingDebitBalance'];
                                                                            // $totalClosingCreditBalance += abs($account->voucherdetails[0]['closingCreditBalance']);
                                                                            $totalTranscationDebit += $account->voucherdetails[0]['transcationDebit'];
                                                                            $totalTranscationCredit += abs($account->voucherdetails[0]['transcationCredit']);
                                                                            $level2TranscationDebit += abs($account->voucherdetails[0]['transcationDebit']);
                                                                            $level2TranscationCredit += abs($account->voucherdetails[0]['transcationCredit']);
                                                                            $totalLevel1TranscationDebit += abs($account->voucherdetails[0]['transcationDebit']);
                                                                            $totalLevel1TranscationCredit += abs($account->voucherdetails[0]['transcationCredit']);
                                                                        @endphp
                                                                        @if (request('summary') != 1)
                                                                            <td style="text-align: right">
                                                                                {{ $openingBalance > 0 ? number_format($openingBalance, 2) : 0 }}
                                                                            </td>
                                                                            <td style="text-align: right">
                                                                                {{ $openingBalance < 0 ? number_format(abs($openingBalance), 2) : 0 }}
                                                                            </td>
                                                                            <td style="text-align: right">
                                                                                {{ number_format($account->voucherdetails[0]['transcationDebit'], 2) }}
                                                                            </td>
                                                                            <td style="text-align: right">
                                                                                {{ number_format(abs($account->voucherdetails[0]['transcationCredit']), 2) }}
                                                                            </td>
                                                                        @endif
                                                                        <td style="text-align: right">
                                                                            {{ $closingBalance > 0 ? number_format($closingBalance, 2) : 0 }}
                                                                        </td>
                                                                        <td style="text-align: right">
                                                                            {{ $closingBalance < 0 ? number_format(abs($closingBalance), 2) : 0 }}
                                                                        </td>
                                                                    </tr>
                                                                @else
                                                                    @if (request('zero') != 1)
                                                                        <tr>
                                                                            <td>{{ $account->code }}</td>
                                                                            <td>{{ $account->description }}</td>
                                                                            @if (request('summary') != 1)
                                                                            <td style="text-align: right">0</td>
                                                                            <td style="text-align: right">0</td>
                                                                            <td style="text-align: right">0</td>
                                                                            <td style="text-align: right">0</td>
                                                                            @endif
                                                                            <td style="text-align: right">0</td>
                                                                            <td style="text-align: right">0</td>
                                                                        </tr>
                                                                    @endif
                                                                @endif
                                                            @endforeach
                                                            <tr>
                                                                <td style="font-size: 15px" colspan="2"><b>Sub
                                                                        Total</b></td>
                                                                @if (request('summary') != 1)
                                                                    <td style="text-align: right">
                                                                        <b>{{ number_format($level2OpeningDebitBalance, 2) }}</b>
                                                                    </td>
                                                                    <td style="text-align: right">
                                                                        <b>{{ number_format($level2OpeningCreditsBalance, 2) }}</b>
                                                                    </td>
                                                                    <td style="text-align: right">
                                                                        <b>{{ number_format($level2TranscationDebit, 2) }}</b>
                                                                    </td>
                                                                    <td style="text-align: right">
                                                                        <b>{{ number_format($level2TranscationCredit, 2) }}</b>
                                                                    </td>
                                                                @endif
                                                                <td style="text-align: right">
                                                                    <b>{{ number_format($level2ClosingDebitBalance, 2) }}</b>
                                                                </td>
                                                                <td style="text-align: right">
                                                                    <b>{{ number_format($level2ClosingCreditBalance, 2) }}</b>
                                                                </td>
                                                            </tr>
                                                        @endif
                                                    @endforeach
                                                    <tr>
                                                        <td style="font-size: 15px" colspan="2"><b>Main Code Total:</b></td>
                                                        @if (request('summary') != 1)
                                                            <td style="text-align: right">
                                                                <b>{{ number_format($totalLevel1OpeningDebitBalance, 2) }}</b>
                                                            </td>
                                                            <td style="text-align: right">
                                                                <b>{{ number_format($totalLevel1OpeningCreditsBalance, 2) }}</b>
                                                            </td>
                                                            <td style="text-align: right">
                                                                <b>{{ number_format($totalLevel1TranscationDebit, 2) }}</b>
                                                            </td>
                                                            <td style="text-align: right">
                                                                <b>{{ number_format($totalLevel1TranscationCredit, 2) }}</b>
                                                            </td>
                                                        @endif
                                                        <td style="text-align: right">
                                                            <b>{{ number_format($totalLevel1ClosingDebitBalance, 2) }}</b>
                                                        </td>
                                                        <td style="text-align: right">
                                                            <b>{{ number_format($totalLevel1ClosingCreditBalance, 2) }}</b>
                                                        </td>
                                                    </tr>
                                                @endif
                                            @empty
                                                <tr class="text-center">
                                                    <td colspan="8">
                                                        <h5>No Record Found!</h5>
                                                    </td>
                                                </tr>
                                            @endforelse
                                            @if (isset($levels) && count($levels) > 0)
                                                <tr>
                                                    <td style="font-size: 15px" colspan="2"><b>Total</b></td>
                                                    @if (request('summary') != 1)
                                                        <td style="text-align: right">
                                                            <b>{{ number_format($totalOpeningDebitBalance, 2) }}</b></td>
                                                        <td style="text-align: right">
                                                            <b>{{ number_format($totalOpeningCreditsBalance, 2) }}</b></td>
                                                        <td style="text-align: right">
                                                            <b>{{ number_format($totalTranscationDebit, 2) }}</b></td>
                                                        <td style="text-align: right">
                                                            <b>{{ number_format($totalTranscationCredit, 2) }}</b></td>
                                                    @endif
                                                    <td style="text-align: right">
                                                        <b>{{ number_format($totalClosingDebitBalance, 2) }}</b></td>
                                                    <td style="text-align: right">
                                                        <b>{{ number_format($totalClosingCreditBalance, 2) }}</b></td>
                                                </tr>
                                            @endif
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
            </section>
        </div>
    </section>
@endsection
@section('scripts')
    <script src="{{ asset('assets/js/scripts.js') }}"></script>
    <script>
        $(document).ready(function() {
            getAccounts('trialBalance');
            $('body').removeClass('menu-expanded').addClass('menu-collapsed');
            mask = $('.range');
            mask.each(function() {
                new Cleave($(this), {
                    delimiter: '-',
                    blocks: [2, 2, 4]
                });
            });
            $("#departmentFilter").change(function() {
                var optVal = $(this).val();
                if (optVal.length > 0) {
                    $.ajax({
                        type: "GET",
                        url: "{{ url('get_dept_employees') }}",
                        data: {
                            department_id: optVal,
                        },
                        success: function(response) {
                            $('#employeeFilter').empty();
                            $('#employeeFilter').html(
                                '<option value="">Select Employee</option>');
                            $.each(response, function(index, value) {
                                $('#employeeFilter').append(
                                    $('<option></option>').val(value.id).html(
                                        value.employee_id + '-' + value
                                        .employee_code + '-' + value.first_name +
                                        ' ' + value.middle_name + ' ' + value
                                        .last_name + ' - ' + value.designation)
                                );
                            });
                        },
                        error: function() {
                            alert('Error occured');
                        }
                    });
                } else {
                    $('#employeeFilter').empty();
                }
            });
        });
        // function divToPrint() {
        //         var divToPrint = document.getElementById("div_to_print");
        //         const WinPrint = window.open("");
        //         WinPrint.document.write(`<!DOCTYPE html>`);
        //         WinPrint.document.write(`<head>`);
        //         WinPrint.document.write(`<link rel="stylesheet" type="text/css" href="app-assets/css/bootstrap.css">`);
        //         WinPrint.document.write(`<style>
    //                     *{  
    //                         padding: 0;
    //                         color: black;
    //                     }
    //                     table tr{
    //                         border: 1px solid black;
    //                     }
    //                     `);
        //                     WinPrint.document.write(`</style>`);
        //     WinPrint.document.write(`</head><body>`);
        //     WinPrint.document.write(`${divToPrint.innerHTML}`);
        //     WinPrint.document.write(`</body></html>`);
        //     WinPrint.document.close();
        //     setTimeout(function(){
        //         WinPrint.print();
        //         WinPrint.close();
        //     },1000);
        //     WinPrint.focus();
        //     }
        // }
        function divToPrint() {
            var divToPrint = document.getElementById("div_to_print");
            const WinPrint = window.open("");
            WinPrint.document.open();
            WinPrint.document.write(
                '<html><head><meta name="viewport" content="width=device-width,initial-scale=1.0,user-scalable=0,minimal-ui"><link rel="stylesheet" type="text/css" href="{{ asset('app-assets/css/bootstrap.css') }}"><link rel="stylesheet" type="text/css" href="{{ asset('app-assets/css/bootstrap-extended.css') }}"><style>table {width: 100%;}#dataTable th, #dataTable td {padding: 0.20rem !important;}#test{display:none}table {border:none;}table { page-break-inside:auto; }tr{ page-break-inside:avoid; page-break-after:auto; }.top_btn {border-right: 1px solid #000;border-bottom: 1px solid #000;border-top: 1px solid #000;border-left: 1px solid #000;box-shadow: 3px 5px;text-align: center;padding-top: 5px;}.para {padding-top: 10px;}.dataTable {border: none;box-shadow: 0px 0px;text-align: center;}.dataTable thead,.dataTable tbody{border:none !important}tr,td,th{ font-size:12px !important; border:none !important;white-space: nowrap !important;} .left-row th {text-align:left !important; text-transform: capitalize;} @page {margin-top: 30px;} body {padding-top: 10px;}</style><body   onload="window.print()">' +
                divToPrint.innerHTML + '</body></html>');
            WinPrint.document.close();
            WinPrint.focus();
            WinPrint.print();
            WinPrint.close();
        }
        // tr, td, th {
        //         border: none;
        //         padding:2px !important;
        //         font-size:10px;
        //     }
    </script>
@endsection
