<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\PusherNotificationController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/dashboard', 'DashboardController@index');
// Add voucher
Route::resource('vouchers' , VoucherController::class);
Route::post('VoucherStatus/{id}' , 'VoucherController@VoucherStatus');
Route::get('VoucherHistory' , 'VoucherController@voucherLogs');
Route::get('histryDetail/{id}' , 'VoucherController@histryDetail');

// Role
Route::resource('roles' , RoleController::class);
// Permission
Route::resource('permissions' , PermissionController::class);
// Users
Route::resource('users' , UserController::class);
// Location
Route::resource('location' , LocationController::class);
// Voucher Type
Route::resource('vouchertype' , VouchertypeController::class);
// Fiscal Year
Route::resource('fIscalyear' , FinancialyearController::class);
// Level 1
Route::resource('level' , Level1Controller::class);
// Level 2
Route::resource('level2' , Level2Controller::class);
// Account Code
Route::get('getAccounts', 'AccountController@getAccounts');
Route::resource('accounts' , AccountController::class);
// Account Code
Route::resource('accountclass' , AccountClassController::class);


// Roports
Route::get('Chart_report', 'ReportController@ChartReport');
Route::get('file_submit', 'ReportController@uploadData');
Route::post('file_upload', 'ReportController@ImportLevels');
Route::post('file_upload_for_accounts', 'ReportController@file_upload_for_accounts');
Route::get('chart_pdf', 'ReportController@ChartPDF');
Route::get('get_levels/{id}', 'ReportController@getlevels');
Route::get('get_accounts/{id}', 'ReportController@getaccounts');
Route::get('TrailBalance', 'ReportController@TrailBalance');
Route::get('editList', 'ReportController@editList');
Route::get('generalLedger', 'ReportController@generalLedger');
Route::get('get_listof_account', 'ReportController@GetAccount');
Route::get('get_level_2', 'ReportController@get_level_2');
Route::get('statement_of_daily_affairs', 'ReportController@StatementOfDailyAffairs');
Route::get('bank_and_cash_balances', 'ReportController@BankAndCashBalancesReport');


Route::get('/notification', function () {
    return view('admin.notifications.notification');
});

require __DIR__.'/auth.php';
