<div class="navbar-header">
    <ul class="nav navbar-nav flex-row">
        <li class="nav-item me-auto">
            <a class="navbar-brand" href="{{ url('/dashboard') }}">
                {{-- <span class="brand-logo">
                    <svg viewbox="0 0 139 95" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" height="24">
                        <defs>
                            <lineargradient id="linearGradient-1" x1="100%" y1="10.5120544%" x2="50%" y2="89.4879456%">
                                <stop stop-color="#000000" offset="0%"></stop>
                                <stop stop-color="#FFFFFF" offset="100%"></stop>
                            </lineargradient>
                            <lineargradient id="linearGradient-2" x1="64.0437835%" y1="46.3276743%" x2="37.373316%" y2="100%">
                                <stop stop-color="#EEEEEE" stop-opacity="0" offset="0%"></stop>
                                <stop stop-color="#FFFFFF" offset="100%"></stop>
                            </lineargradient>
                        </defs>
                        <g id="Page-1" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                            <g id="Artboard" transform="translate(-400.000000, -178.000000)">
                                <g id="Group" transform="translate(400.000000, 178.000000)">
                                    <path
                                        class="text-primary"
                                        id="Path"
                                        d="M-5.68434189e-14,2.84217094e-14 L39.1816085,2.84217094e-14 L69.3453773,32.2519224 L101.428699,2.84217094e-14 L138.784583,2.84217094e-14 L138.784199,29.8015838 C137.958931,37.3510206 135.784352,42.5567762 132.260463,45.4188507 C128.736573,48.2809251 112.33867,64.5239941 83.0667527,94.1480575 L56.2750821,94.1480575 L6.71554594,44.4188507 C2.46876683,39.9813776 0.345377275,35.1089553 0.345377275,29.8015838 C0.345377275,24.4942122 0.230251516,14.560351 -5.68434189e-14,2.84217094e-14 Z"
                                        style="fill: currentColor;"
                                    ></path>
                                    <path
                                        id="Path1"
                                        d="M69.3453773,32.2519224 L101.428699,1.42108547e-14 L138.784583,1.42108547e-14 L138.784199,29.8015838 C137.958931,37.3510206 135.784352,42.5567762 132.260463,45.4188507 C128.736573,48.2809251 112.33867,64.5239941 83.0667527,94.1480575 L56.2750821,94.1480575 L32.8435758,70.5039241 L69.3453773,32.2519224 Z"
                                        fill="url(#linearGradient-1)"
                                        opacity="0.2"
                                    ></path>
                                    <polygon id="Path-2" fill="#000000" opacity="0.049999997" points="69.3922914 32.4202615 32.8435758 70.5039241 54.0490008 16.1851325"></polygon>
                                    <polygon id="Path-21" fill="#000000" opacity="0.099999994" points="69.3922914 32.4202615 32.8435758 70.5039241 58.3683556 20.7402338"></polygon>
                                    <polygon id="Path-3" fill="url(#linearGradient-2)" opacity="0.099999994" points="101.428699 0 83.0667527 94.1480575 130.378721 47.0740288"></polygon>
                                </g>
                            </g>
                        </g>
                    </svg>
                </span> --}}
                <h2 class="brand-text ps-0" data-bs-toggle="tooltip" data-bs-placement="bottom"
                    title="{{ env('COMPANY') }}">{{ Str::limit(env('COMPANY'), '12') }}</h2>
            </a>
        </li>
        <li class="nav-item nav-toggle">
            <a class="nav-link modern-nav-toggle pe-0" data-bs-toggle="collapse">
                <i class="d-block d-xl-none text-primary toggle-icon font-medium-4" data-feather="x"></i><i
                    class="d-none d-xl-block collapse-toggle-icon font-medium-4 text-primary" data-feather="disc"
                    data-ticon="disc"></i>
            </a>
        </li>
    </ul>
</div>
<div class="shadow-bottom"></div>
<div class="main-menu-content">
    <ul class="navigation navigation-main" id="main-menu-navigation" data-menu="menu-navigation">
        @can('Dashboard')
            <li class="nav-item {{ \Route::getFacadeRoot()->current()->uri() == 'dashboard' ? 'active' : '' }}">
                <a class="d-flex align-items-center" href="{{ url('/dashboard') }}">
                    <i data-feather="home"></i><span class="menu-title text-truncate"
                        data-i18n="Dashboards">Dashboard</span>
                </a>
            </li>
        @endcan
        @can('Reports')
            <li class="nav-item">
                <a class="d-flex align-items-center" href="#"><i class="fas fa-chart-bar"></i><span
                        class="menu-title text-truncate" data-i18n="Invoice">Reports </span></a>
                <ul class="menu-content">
                    @can('Chart of Account')
                        <li class="nav-item {{ request()->segment(1) == 'Chart_report' ? 'active' : '' }}">
                            <a class="d-flex align-items-center" href="{{ url('/Chart_report') }}"><i
                                    data-feather="circle"></i><span class="menu-item text-truncate" data-i18n="Preview">Chart of
                                    Account</span></a>
                        </li>
                    @endcan
                    {{-- <li class="nav-item {{ request()->segment(1) == 'file_submit' ? 'active' : ''}}">
                    <a class="d-flex align-items-center" href="{{url('/file_submit')}}"><i data-feather="circle"></i><span class="menu-item text-truncate" data-i18n="Preview">File Submit</span></a>
                </li> --}}
                    @can('Trail Balance')
                        <li class="nav-item {{ request()->segment(1) == 'TrailBalance' ? 'active' : '' }}">
                            <a class="d-flex align-items-center" href="{{ url('/TrailBalance') }}"><i
                                    data-feather="circle"></i><span class="menu-item text-truncate" data-i18n="Preview">Trial
                                    Balance</span></a>
                        </li>
                    @endcan
                    @can('Genral Ladger')
                        <li class="nav-item {{ request()->segment(1) == 'generalLedger' ? 'active' : '' }}">
                            <a class="d-flex align-items-center" href="{{ url('/generalLedger') }}"><i
                                    data-feather="circle"></i><span class="menu-item text-truncate" data-i18n="Preview">General
                                    Ledger</span></a>
                        </li>
                    @endcan
                    {{-- @can('Edit List') --}}
                    <li class="nav-item {{ request()->segment(1) == 'editList' ? 'active' : '' }}">
                        <a class="d-flex align-items-center" href="{{ url('/editList') }}"><i
                                data-feather="circle"></i><span class="menu-item text-truncate" data-i18n="Preview">Edit
                                List</span></a>
                    </li>
                    {{-- @endcan --}}
                    {{-- @can('') --}}
                    @can('Statement Of Daily Affairs')
                    <li class="nav-item {{ request()->segment(1) == 'statement_of_daily_affairs' ? 'active' : '' }}">
                        <a class="d-flex align-items-center" href="{{ url('/statement_of_daily_affairs') }}"><i
                                data-feather="circle"></i><span class="menu-item text-truncate"
                                data-i18n="Preview">Statement Of Daily Affairs</span></a>
                    </li>
                    @endcan
                    {{-- @endcan --}}
                    @can('Bank & Cash Balances Report')
                    <li class="nav-item {{ request()->segment(1) == 'bank_and_cash_balances' ? 'active' : '' }}">
                        <a class="d-flex align-items-center" href="{{ url('/bank_and_cash_balances') }}"><i
                                data-feather="circle"></i><span class="menu-item text-truncate"
                                data-i18n="Preview">Bank & Cash Balances Report</span></a>
                    </li>
                    @endcan
                </ul>
            </li>
        @endcan
        @can('Voucher Management')
            <li class="navigation-header"><span data-i18n="Attendance">Voucher Management</span><i
                    data-feather="more-horizontal"></i></li>
            @can('Locations')
                <li class="nav-item {{ request()->segment(1) == 'location' ? 'active' : '' }}">
                    <a class="d-flex align-items-center" href="{{ url('/location') }}"><i
                            class="fas fa-map-marker-alt"></i><span class="menu-item text-truncate"
                            data-i18n="Preview">Locations</span></a>
                </li>
            @endcan
            @can('Vouchers')
                <li class="nav-item {{ request()->segment(1) == 'vouchers' ? 'active' : '' }}">
                    <a class="d-flex align-items-center" href="{{ url('/vouchers') }}"><i class="fas fa-file-invoice"></i><span
                            class="menu-item text-truncate" data-i18n="Preview">Vouchers</span></a>
                </li>
            @endcan
            @can('Vouchers History')
                <li
                    class="nav-item {{ request()->segment(1) == 'VoucherHistory' || request()->segment(1) == 'histryDetail' ? 'active' : '' }}">
                    <a class="d-flex align-items-center" href="{{ url('/VoucherHistory') }}"><i
                            class="fas fa-history"></i><span class="menu-item text-truncate" data-i18n="Preview">Vouchers
                            History</span></a>
                </li>
            @endcan
            @can('Setup')
                <li class="nav-item">
                    <a class="d-flex align-items-center" href="#"><i data-feather='sliders'></i><span
                            class="menu-title text-truncate" data-i18n="Invoice">Setup</span></a>
                    <ul class="menu-content">
                        @can('Voucher Types')
                            <li class="nav-item {{ request()->segment(1) == 'vouchertype' ? 'active' : '' }}">
                                <a class="d-flex align-items-center" href="{{ url('/vouchertype') }}"><i
                                        data-feather="circle"></i><span class="menu-item text-truncate"
                                        data-i18n="Preview">Voucher Types</span></a>
                            </li>
                        @endcan
                        @can('Financial Years')
                            <li class="nav-item {{ request()->segment(1) == 'fIscalyear' ? 'active' : '' }}">
                                <a class="d-flex align-items-center" href="{{ url('/fIscalyear') }}"><i
                                        data-feather="circle"></i><span class="menu-item text-truncate"
                                        data-i18n="Preview">Financial Years</span></a>
                            </li>
                        @endcan
                        @can('Level 1')
                            <li class="nav-item {{ request()->segment(1) == 'level' ? 'active' : '' }}">
                                <a class="d-flex align-items-center" href="{{ url('/level') }}"><i
                                        data-feather="circle"></i><span class="menu-item text-truncate" data-i18n="Preview">Level
                                        1</span></a>
                            </li>
                        @endcan
                        @can('Level 2')
                            <li class="nav-item {{ request()->segment(1) == 'level2' ? 'active' : '' }}">
                                <a class="d-flex align-items-center" href="{{ url('/level2') }}"><i
                                        data-feather="circle"></i><span class="menu-item text-truncate" data-i18n="Preview">Level
                                        2</span></a>
                            </li>
                        @endcan
                        {{-- <li class="nav-item {{ request()->segment(1) == 'accountclass' ? 'active' : ''}}">
                    <a class="d-flex align-items-center" href="{{ url('/accountclass') }}"><i data-feather="circle"></i><span class="menu-item text-truncate" data-i18n="Preview">Account Class</span></a>
                </li> --}}
                        @can('Accounts')
                            <li class="nav-item {{ request()->segment(1) == 'accounts' ? 'active' : '' }}">
                                <a class="d-flex align-items-center" href="{{ url('/accounts') }}"><i
                                        data-feather="circle"></i><span class="menu-item text-truncate"
                                        data-i18n="Preview">Accounts</span></a>
                            </li>
                        @endcan
                    </ul>
                </li>
            @endcan
        @endcan
        @can('Users Management')
            <li class="navigation-header"><span data-i18n="Attendance">User Management</span><i
                    data-feather="more-horizontal"></i></li>
            @can('Roles & Permissions')
                <li class="nav-item {{ \Route::getFacadeRoot()->current()->uri() == '/roles' ? 'active' : '' }}">
                    <a class="d-flex align-items-center" href="{{ url('/roles') }}">
                        <i class="fa fa-tasks" aria-hidden="true"></i><span class="menu-title text-truncate"
                            data-i18n="Dashboards">Roles & Permissions</span>
                    </a>
                </li>
            @endcan
            @can('Users')
                <li class="nav-item {{ \Route::getFacadeRoot()->current()->uri() == 'users' ? 'active' : '' }}">
                    <a class="d-flex align-items-center" href="{{ url('/users') }}">
                        <i data-feather="users"></i><span class="menu-title text-truncate"
                            data-i18n="Dashboards">Users</span>
                    </a>
                </li>
            @endcan
        @endcan
    </ul>
</div>
