<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\Traits\LogsActivity;
use Spatie\Activitylog\LogOptions;

class Financialyear extends Model
{
    use HasFactory,LogsActivity;
    protected $table = 'fiscal_years';

    public function getActivitylogOptions(): LogOptions
    {
        $login_user = \Auth::user()->name;
        return LogOptions::defaults()
        ->logOnly(['fiscal_year','start_date','end_date','open_close','status'])
        ->useLogName('FinancialYear')
        ->logOnlyDirty()
        ->setDescriptionForEvent(fn(string $eventName) => "{$login_user} have {$eventName} Financial Year");
    }

    public function vouchers()
    {
        return $this->hasMany(Voucher::class);
    }
}
