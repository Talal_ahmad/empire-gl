<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\Traits\LogsActivity;
use Spatie\Activitylog\LogOptions;
use Spatie\Activitylog\Contracts\Activity;

class Account extends Model
{
    use HasFactory,LogsActivity;

    public function getActivitylogOptions(): LogOptions
    {
        $login_user = \Auth::user()->name;
        return LogOptions::defaults()
        ->logOnly(['code','description','level2_code','account_code','gl_type','name','short_name','address','ntn','stn','email','contact_no','exemption_certificate','account_title','account_number','location','tag'])
        ->useLogName('Account')
        ->logOnlyDirty()
        ->setDescriptionForEvent(fn(string $eventName) => "{$login_user} have {$eventName} Account");
    }

    public function tapActivity(Activity $activity, string $eventName)
    {
        $activity->properties = $activity->properties->put('account_details', $this->accountdetails);
    }

    public function voucherdetails()
    {
        return $this->hasMany(VoucherDetail::class);
    }

    public function accountdetails()
    {
        return $this->hasMany(AccountDetail::class);
    }

    public function level2()
    {
        return $this->belongsTo(Level2::class,'level_2_id');
    }
}
