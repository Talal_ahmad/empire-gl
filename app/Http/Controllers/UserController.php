<?php

namespace App\Http\Controllers;

use DataTables;
use App\Models\User;
use App\Models\CompanyStructure;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Spatie\Permission\Models\Role;
use App\Models\Financialyear;
use App\Models\Location;
use App\Models\Vouchertype;
use App\Models\Account;
use App\Models\Level2;
use App\Models\UserVoucherType;
use Spatie\Permission\Models\Permission;
use Illuminate\Validation\ValidationException;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct(){
        $this->middleware('auth'); 
    }

    public function index(Request $request)
    {
        if($request->ajax()){
            $query = User::leftjoin('model_has_roles' , 'model_has_roles.model_id' , '=' , 'users.id')->leftjoin('roles' , 'roles.id' , '=' , 'model_has_roles.role_id')
            // ->leftjoin('locations','locations.id' , '=' , 'users.location_id')
            // ->leftjoin('fiscal_years','fiscal_years.id' , '=' , 'users.fiscal_years_id')
            ->select('users.id', 'users.name', 'users.email', 'roles.name AS roleName', 'users.profile_photo_path');
            return DataTables::of($query)->make(true);
        }
        return view('admin.users.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $roles = Role::get();
        $location = Location::get();
        $VoucherTypes = Vouchertype::get();
        $financialyear = Financialyear::where('open_close','=','Open')->where('status','=','Active')->get();
        $AccountCodes = Level2::get(['id','code','description']);
        return view('admin.users.create',compact('roles','financialyear','location','VoucherTypes','AccountCodes'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        DB::transaction(function () use($request){
            $this->validate($request, [
                'name' => 'required|string|regex:^([a-zA-Z]+(.)?[\s]*)$^',
                'email' => 'required|email|unique:users',
                'password' => 'required|min:8|max:20|confirmed',
            ]);
            if($request->hasFile('profile_pic')){
                $profile_pic = User::InsertImage($request->file('profile_pic'));
            }
            $user = new User();
            $user->name = $request->input('name');
            $user->email = $request->input('email');
            $user->password = Hash::make($request->input('password'));
            $user->fiscal_years = json_encode($request->input('fiscal_years'));
            $user->locations = json_encode($request->input('locations'));
            $user->account_codes = json_encode($request->input('account_codes'));
            $user->profile_photo_path = isset($profile_pic) ? $profile_pic : NULL;
            $user->save();
            $user->assignRole($request->role);
            foreach ($request->voucherTypes as $key => $type) {
                $user_voucher_type = new UserVoucherType();
                $user_voucher_type->user_id = $user->id;
                $user_voucher_type->voucher_type_id = $type['voucher_type'];
                $user_voucher_type->type_limit = $type['type_limit'];
                $user_voucher_type->permissions = isset($type['permissions']) ? json_encode($type['permissions']) : NULL;
                $user_voucher_type->save();
            }
        });
        return redirect('users')->with('success_message', 'User has been Added Successfully!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        // dd($id);
        $user = User::find($id);
        $user_role = DB::table('model_has_roles')->where('model_id' , $id)->select('role_id')->first();
        $roles = Role::get();
        $location = Location::get();
        $VoucherTypes = Vouchertype::get();
        $UserVoucherTypes = UserVoucherType::where('user_id',$id)->get();
        $financialyear = Financialyear::where('open_close','=','Open')->where('status','=','Active')->get();
        $AccountCodes = Level2::get(['id','code','description']);
        return view('admin.users.edit',compact('user','user_role','roles','financialyear','location','VoucherTypes','AccountCodes','UserVoucherTypes'));
        // $location = DB::table('users')->where('id' , $id)->select('location_id')->first();
        // $fiscal_year = DB::table('users')->where('id' , $id)->select('fiscal_years_id')->first();
        // return response()->json([
        //     'user' => $user,
        //     'role' => $role,
        //     'location' => $location,
        //     'fiscal_year' => $fiscal_year,
        // ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        DB::transaction(function () use($request,$id){
            $this->validate($request, [
                'name' => 'required|string|regex:^([a-zA-Z]+(.)?[\s]*)$^',
                'email' => 'required|email|unique:users,email,'.$id,
                'password' => 'nullable|min:8|max:20|confirmed',
            ]);
            $user = User::find($id);
            if($request->hasFile('profile_pic')){
                if(!empty($user->profile_photo_path)){
                    unlink(public_path().'/profile_pics/'.$user->profile_photo_path);
                }
                $profile_pic = User::InsertImage($request->file('profile_pic'));
            }
            $user->name = $request->input('name');
            $user->email = $request->input('email');
            $user->fiscal_years = json_encode($request->input('fiscal_years'));
            $user->locations = json_encode($request->input('locations'));
            $user->account_codes = json_encode($request->input('account_codes'));
            $user->profile_photo_path = isset($profile_pic) ? $profile_pic : $user->profile_photo_path;
            if($request->password){
                $user->password = Hash::make($request->password);
            }
            $user->update();
            $user->syncRoles($request->role);
            if(count($request->voucherTypes) > 0){
                UserVoucherType::where('user_id',$id)->delete();
                foreach ($request->voucherTypes as $key => $type) {
                    $user_voucher_type = new UserVoucherType();
                    $user_voucher_type->user_id = $user->id;
                    $user_voucher_type->voucher_type_id = $type['voucher_type'];
                    $user_voucher_type->type_limit = $type['type_limit'];
                    $user_voucher_type->permissions = isset($type['permissions']) ? json_encode($type['permissions']) : NULL;
                    $user_voucher_type->save();
                }
            }
        });
        return redirect('users')->with('success_message', 'User has been Updated Successfully!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
