<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DataTables;
use App\Models\Level2;
use App\Models\Level1;
use Illuminate\Support\Facades\DB;
use Illuminate\Validation\ValidationException;

class Level2Controller extends Controller
{
    public function __construct(){
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if($request->ajax()){
            return DataTables::eloquent(Level2::query())->addIndexColumn()->make(true);
        }
        $levels = Level1::get();
        return view('admin.level2.index',get_defined_vars());
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // dd($request->level1_code);
        try{
            $this->validate($request , [
                'code' => 'required|unique:level_2',
                'level1_code' => 'required',
                'description' => 'required',
            ]);
            $level_1 = Level1::where('id','=',$request->level1_code)->first();
            $code = $level_1->code.'-'.$request->code;
            $ischecked = Level2::where('code','=',$code)->first();
            if (!empty($ischecked)) {
                return ['code' => '404','status' => 'Code Already Taken!'];
            }

            $level2 = new Level2;
            $level2->code = $code;
            $level2->level_1_code =  $level_1->code;
            $level2->level_1_id =  $level_1->id;
            $level2->description = $request->description;
            $level2->save();
            return ['code'=>'200','message'=>'success'];
        }
        catch(\Exception | ValidationException $e){
            if($e instanceof ValidationException){
                return ['code'=>'422','errors' => $e->errors()];
            }
            else{
                return ['code'=>'500','error_message'=>$e->getMessage()];
            }
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $Level = Level2::find($id);
        $level1_code = DB::table('level_2')->where('id' , $id)->select('level_1_code')->first();
        return response()->json($Level);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        // dd($request);
        try{
            $this->validate($request , [
                'edit_code' => 'required|unique:level_2,code,'.$id.',id',
                // 'name' => 'required|unique:companies,name,'.$company->id.',id',
            ]);
            $level_1 = Level1::where('code','=',$request->edit_level1_code)->first();
            $code = $level_1->code.'-'.$request->edit_code;
            // $ischecked = Level2::where('code','=',$code)->first();
            // if (!empty($ischecked)) {
            //     return ['code' => '404','status' => 'Code Already Taken!'];
            // }
            $Level = Level2::find($id);
            $Level->code = $code;
            $Level->level_1_code = $level_1->code;
            $Level->level_1_id = $level_1->id;
            $Level->description = $request->edit_description;
            $Level->update();
            return ['code'=>'200','message'=>'success'];
        }
        catch(\Exception | ValidationException $e){
            if($e instanceof ValidationException){
                return ['code'=>'422','errors' => $e->errors()];
            }
            else{
                return ['code'=>'500','error_message'=>$e->getMessage()];
            }
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try{
            $Level = Level2::find($id);
            $Level->delete();
            return ['code'=>'200','message'=>'success'];
        }
        catch(\Exception $e){
            return ['code'=>'500','error_message'=>$e->getMessage()];
        }
    }
}
