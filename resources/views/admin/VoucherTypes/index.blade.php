@extends('admin.layouts.master')
@section('title', 'Vouchertype')

@section('content')
<section class="content-wrapper container-xxl p-0">
    <div class="content-header row">
        <div class="content-header-left col-md-9 col-12 mb-2">
            <div class="row breadcrumbs-top">
                <div class="col-12">
                    <h2 class="content-header-title float-start mb-0">Vouchertype</h2>
                    <div class="breadcrumb-wrapper">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="{{url('/dashboard')}}">Dashboard</a>
                            </li>
                            <li class="breadcrumb-item active">vouchertype
                            </li>
                        </ol>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="content-header row"></div>
    <div class="content-body">
        <section id="basic-datatable">
            <div class="row">
                <div class="col-12">
                    <div class="card pb-2">
                        <table class="table" id="dataTable">
                            <thead>
                                <tr>
                                    <th class="not_include"></th>
                                    <th>Id</th>
                                    <th>VoucherType</th>
                                    <th>SubType</th>
                                    <th>Description</th>
                                    <th class="not_include">Actions</th>
                                </tr>
                            </thead>
                            <tbody>

                            </tbody>
                        </table>
                    </div>
                </div>
                <!--Add Modal -->
                <div class="modal fade text-start" id="add_modal" tabindex="-1" aria-labelledby="myModalLabel17"
                    aria-hidden="true">
                    <div class="modal-dialog modal-lg">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h4 class="modal-title" id="myModalLabel17">Add VoucherType</h4>
                                <button type="button" class="btn-close" data-bs-dismiss="modal"
                                    aria-label="Close"></button>
                            </div>
                            <form class="form" id="add_form">
                                @csrf
                                <div class="modal-body">
                                    <div class="row">
                                        <div class="col-md-6 col-12">
                                            <div class="mb-1">
                                                <label class="form-label" for="name">Name</label>
                                                <input type="text" name="name" id="name" class="form-control"
                                                    placeholder="Enter City Name" required />
                                            </div>
                                        </div>
                                        <div class="col-md-6 col-12">
                                            <div class="mb-1">
                                                <label class="form-label" for="type">Sub Types</label>
                                                <select name="type" id="type" class="form-control select2" data-placeholder="Select Sub Type" required>
                                                    <option value=""></option>
                                                    <option value="1">Customer</option>
                                                    <option value="2">Supplier</option>
                                                    <option value="3">Bank</option>
                                                    <option value="4">General</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-12">
                                            <div class="mb-1">
                                                <label class="form-label" for="description">Description</label>
                                                <input type="text" name="description" id="description" class="form-control"
                                                    placeholder="Enter Description" required />
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="modal-footer">
                                    <button type="reset" class="btn btn-outline-success">Reset</button>
                                    <button type="submit" class="btn btn-primary" id="save">Save</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
            <!--Edit Modal -->
            <div class="modal fade text-start" id="edit_modal" tabindex="-1" aria-labelledby="myModalLabel17"
                aria-hidden="true">
                <div class="modal-dialog modal-lg">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h4 class="modal-title" id="myModalLabel17">Edit Fiscalyear</h4>
                            <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                        </div>
                        <form class="form" id="edit_form">
                            @csrf
                            @method('PUT')
                            <div class="modal-body">
                                <div class="row">
                                    <div class="col-md-6 col-12">
                                        <div class="mb-1">
                                            <label class="form-label" for="name">Name</label>
                                            <input type="text" name="name" id="edit_name" class="form-control"
                                                placeholder="Name" />
                                        </div>
                                    </div>
                                    <div class="col-md-6 col-12">
                                        <div class="mb-1">
                                            <label class="form-label" for="edit_type">Sub Types</label>
                                            <select name="type" id="edit_type" class="form-control select2" data-placeholder="Select Sub Type" required>
                                                <option value=""></option>
                                                <option value="1">Customer</option>
                                                <option value="2">Supplier</option>
                                                <option value="3">Bank</option>
                                                <option value="4">General</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-12">
                                        <div class="mb-1">
                                            <label class="form-label" for="edit_description">Description</label>
                                            <input type="text" name="description" id="edit_description"
                                                class="form-control" placeholder="Enter Description"  />
                                        </div>
                                    </div>
                                </div>
                                <div class="modal-footer">
                                    <button type="reset" class="btn btn-outline-success">Reset</button>
                                    <button type="submit" class="btn btn-primary" id="update">Update</button>
                                </div>
                        </form>
                    </div>
                </div>
            </div>
        </section>
    </div>
</section>

@endsection
@section('scripts')
<script>
var rowid;
$(document).ready(function() {
    dataTable = $('#dataTable').DataTable({
        processing: true,
        serverSide: true,
        responsive: true,
        ajax: "{{ route('vouchertype.index') }}",
        columns: [
            {
                data: 'responsive_id'
            },
            {
                data: 'DT_RowIndex', name: 'DT_RowIndex', orderable: false, searchable: false 
            },
            {
                data: 'voucher_type',
                name: 'name',
            },
            {
                data: 'subType',
                render: function(data, type, full, meta) {
                    if(full.subType == 1){
                        return 'Customer';
                    }
                    if(full.subType == 2){
                        return 'Supplier';
                    }
                    if(full.subType == 3){
                        return 'Bank';
                    }
                    if(full.subType == 4){
                        return 'General'
                    }
                }
            },
            {
                data: 'description',
                name: 'description',

            },
            {
                data: ''
            },
        ],
        "columnDefs": [{
                // For Responsive
                className: 'control',
                orderable: false,
                searchable: false,
                targets: 0
            },
            {
                // Actions
                targets: -1,
                title: 'Actions',
                orderable: false,
                searchable: false,
                render: function(data, type, full, meta) {
                    return (
                        '<a href="javascript:;"  data-bs-toggle="modal" data-bs-target="#edit-modal" class="item-edit" onclick=location_edit(' +
                        full.id + ')>' +
                        feather.icons['edit'].toSvg({
                            class: 'font-medium-4'
                        }) +
                        '</a>' +
                        '<a href="javascript:;" onclick="deleteLocation(' + full.id + ')">' +
                        feather.icons['trash-2'].toSvg({
                            class: 'font-medium-4 text-danger'
                        }) +
                        '</a>'
                    );
                }
            },
            {
                "defaultContent": "-",
                "targets": "_all"
            }
        ],
        "order": [
            [0, 'asc']
        ],
        dom: '<"card-header border-bottom p-1"<"head-label"><"dt-action-buttons text-end"B>><"d-flex justify-content-between align-items-center mx-0 row"<"col-sm-12 col-md-6"l><"col-sm-12 col-md-6"f>>t<"d-flex justify-content-between mx-0 row"<"col-sm-12 col-md-6"i><"col-sm-12 col-md-6"p>>',
        displayLength: 10,
        lengthMenu: [10, 25, 50, 75, 100],
        buttons: [{
                extend: 'collection',
                className: 'btn btn-outline-secondary dropdown-toggle me-2',
                text: feather.icons['share'].toSvg({
                    class: 'font-small-4 me-50'
                }) + 'Export',
                buttons: [{
                        extend: 'print',
                        text: feather.icons['printer'].toSvg({
                            class: 'font-small-4 me-50'
                        }) + 'Print',
                        className: 'dropdown-item',
                        exportOptions: {
                            columns: ':not(.not_include)'
                        }
                    },
                    {
                        extend: 'csv',
                        text: feather.icons['file-text'].toSvg({
                            class: 'font-small-4 me-50'
                        }) + 'Csv',
                        className: 'dropdown-item',
                        exportOptions: {
                            columns: ':not(.not_include)'
                        }
                    },
                    {
                        extend: 'excel',
                        text: feather.icons['file'].toSvg({
                            class: 'font-small-4 me-50'
                        }) + 'Excel',
                        className: 'dropdown-item',
                        exportOptions: {
                            columns: ':not(.not_include)'
                        }
                    },
                    {
                        extend: 'pdf',
                        text: feather.icons['clipboard'].toSvg({
                            class: 'font-small-4 me-50'
                        }) + 'Pdf',
                        className: 'dropdown-item',
                        exportOptions: {
                            columns: ':not(.not_include)'
                        }
                    },
                    {
                        extend: 'copy',
                        text: feather.icons['copy'].toSvg({
                            class: 'font-small-4 me-50'
                        }) + 'Copy',
                        className: 'dropdown-item',
                        exportOptions: {
                            columns: ':not(.not_include)'
                        }
                    }
                ],
                init: function(api, node, config) {
                    $(node).removeClass('btn-secondary');
                    $(node).parent().removeClass('btn-group');
                    setTimeout(function() {
                        $(node).closest('.dt-buttons').removeClass('btn-group')
                            .addClass('d-inline-flex');
                    }, 50);
                }
            },
            {
                text: feather.icons['plus'].toSvg({
                    class: 'me-50 font-small-4'
                }) + 'Add New',
                className: 'create-new btn btn-primary',
                attr: {
                    'data-bs-toggle': 'modal',
                    'data-bs-target': '#add_modal'
                },
                init: function(api, node, config) {
                    $(node).removeClass('btn-secondary');
                }
            }
        ],
        responsive: {
            details: {
                display: $.fn.dataTable.Responsive.display.childRowImmediate,
                type: 'column',
            }
        },
        language: {
            paginate: {
                // remove previous & next text from pagination
                previous: '&nbsp;',
                next: '&nbsp;'
            }
        },
        "drawCallback": function() {
            $('.toggle-class').bootstrapToggle();
            // status();
        },
    });
    $('div.head-label').html('<h6 class="mb-0">List of Vouchertype</h6>');

    $("#add_form").submit(function(e) {
        e.preventDefault();
        $.ajax({
            url: "{{route('vouchertype.store')}}",
            type: "POST",
            data: new FormData(this),
            processData: false,
            contentType: false,
            responsive: true,
            success: function(response) {
                if (response.errors) {
                    $.each(response.errors, function(index, value) {
                        Toast.fire({
                            icon: 'error',
                            title: value
                        })
                    });
                } else if (response.error_message) {
                    Toast.fire({
                        icon: 'error',
                        title: 'An error has been occured! Please Contact Administrator.'
                    })
                } else {
                    $('#add_form')[0].reset();
                    $("#add_modal").modal("hide");
                    dataTable.ajax.reload();
                    Toast.fire({
                        icon: 'success',
                        title: 'Vouchertype has been Added Successfully!'
                    })
                }

            }
        });
    });
    $("#edit_form").submit(function(e) {
        e.preventDefault();
        $.ajax({
            url: "{{url('vouchertype')}}" + "/" + rowid,
            type: 'POST',
            data: new FormData(this),
            contentType: false,
            processData: false,
            success: function(response) {
                if (response.errors) {
                    $.each(response.errors, function(index, value) {
                        Toast.fire({
                            icon: 'error',
                            title: value
                        })
                    });
                } else if (response.error_message) {
                    Toast.fire({
                        icon: 'error',
                        title: 'An error has been occured! Please Contact Administrator.'
                    })
                } else {
                    $('#edit_form')[0].reset();
                    $("#edit_modal").modal("hide");
                    dataTable.ajax.reload();
                    Toast.fire({
                        icon: 'success',
                        title: 'Vouchertype has been Updated Successfully!'
                    })
                }
            }
        });
    });
});

function location_edit(id) {
    rowid = id;
    $.ajax({
        url: "{{url('vouchertype')}}" + "/" + rowid + "/edit",
        type: "GET",
        success: function(response) {
            $('#edit_name').val(response.voucher_type);
            $('#edit_type').val(response.subType).select2();
            $('#edit_description').val(response.description);
            $('#edit_modal').modal('show');
        }
    });
}

function deleteLocation(id) {
    $.confirm({
        icon: 'far fa-question-circle',
        title: 'Confirm!',
        content: 'Are you sure you want to delete!',
        type: 'orange',
        typeAnimated: true,
        buttons: {
            Confirm: {
                text: 'Confirm',
                btnClass: 'btn-orange',
                action: function() {
                    $.ajax({
                        url: "vouchertype/" + id,
                        type: "DELETE",
                        data: {
                            _token: "{{ csrf_token() }}"
                        },
                        success: function(response) {
                            if (response.error_message) {
                                Toast.fire({
                                    icon: 'error',
                                    title: 'An error has been occured! Please Contact Administrator.'
                                })
                            } else if (response.code == 300) {
                                $.alert({
                                    icon: 'far fa-times-circle',
                                    title: 'Oops!',
                                    content: response.message,
                                    type: 'red',
                                    buttons: {
                                        Okay: {
                                            text: 'Okay',
                                            btnClass: 'btn-red',
                                        }
                                    }
                                });
                            } else {
                                dataTable.ajax.reload();
                                Toast.fire({
                                    icon: 'success',
                                    title: 'Vouchertype has been Deleted Successfully!'
                                })
                            }
                        }
                    });
                }
            },
            cancel: function() {
                $.alert('Canceled!');
            },
        }
    });
}
</script>
@endsection