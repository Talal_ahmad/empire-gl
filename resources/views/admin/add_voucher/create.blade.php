@extends('admin.layouts.master')
@section('title', 'Add Voucher')
@section('content')
<section class="content-wrapper container-xxl p-0">
    <div class="content-header row">
        <div class="content-header-left col-md-9 col-12 mb-2">
            <div class="row breadcrumbs-top">
                <div class="col-12">
                    <h2 class="content-header-title float-start mb-0">Voucher</h2>
                    <div class="breadcrumb-wrapper">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="{{ url('/dashboard') }}">Dashboard</a>
                            </li>
                            <li class="breadcrumb-item"><a href="{{ url('/vouchers') }}">Vouchers</a>
                            </li>
                            <li class="breadcrumb-item active"> Add Voucher
                            </li>
                        </ol>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="content-body">
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-header">
                        <h4 class="card-title">Add Voucher</h4>
                    </div>
                    <div class="card-body">
                        <form class="form" id="add_voucher" enctype="multipart/form-data">
                            @csrf
                            <div class="row">
                                <div class="col-md-4 col-12">
                                    <div class="mb-1">
                                        <label class="form-label" for="voucher_type">Voucher Types</label>
                                        <select name="voucher_type" id="voucher_type" class="select2 form-select" data-placeholder="Select Voucher Type" required>
                                            <option value=""></option>
                                            @foreach ($Vouchertypes as $Vouchertype)
                                            <option value="{{ $Vouchertype->id }}">{{ $Vouchertype->voucher_type }}
                                            </option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-4 col-12">
                                    <div class="mb-1">
                                        <label class="form-label" for="date">Date</label>
                                        <input type="date" name="date" id="date" class="startdate_input date form-control" value="{{date('Y-m-d')}}" required>
                                    </div>
                                </div>
                                <div class="col-md-4 col-12">
                                    <label class="form-label" for="bill_no">Bill No</label>
                                    <input type="text" name="bill_no" id="bill_no" class="form-control" placeholder="Bill No">
                                </div>
                                <div class="col-md-4 col-12">
                                    <label class="form-label" for="igp_number">Gate Inward Pass #</label>
                                    <input type="text" name="igp_number" id="igp_number" class="form-control" placeholder="Gate Inward Pass Number">
                                </div>
                                <div class="col-md-4 col-12">
                                    <label class="form-label" for="ogp_number">Gate Outward Pass #</label>
                                    <input type="text" name="ogp_number" id="ogp_number" class="form-control" placeholder="Gate Outward Pass Number">
                                </div>
                                <div class="col-md-4 col-12">
                                    <div class="mb-1">
                                        <label class="form-label" for="igp_ogp_date">Gate Passes Date</label>
                                        <input type="date" name="igp_ogp_date" id="igp_ogp_date" value="{{date('Y-m-d')}}" class="form-control">
                                    </div>
                                </div>
                                <div id="subType">

                                </div>
                                <div class="col-md-6 col-12" id="locationDiv">
                                    <div class="mb-1">
                                        <label class="form-label" for="location">Locations</label>
                                        <select name="location" id="location" class="select2 form-select" data-placeholder="Select Location" required>
                                            <option value=""></option>
                                            @foreach ($locations as $location)
                                            <option value="{{ $location->id }}">{{ $location->location_name }}
                                            </option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-6 col-12" id="attchmentDiv">
                                    <label class="form-label" for="attachments">Attachments</label>
                                    <input type="file" name="attachments[]" id="attachments" class="form-control" multiple/>
                                </div>
                                <div class="col-12">
                                    <label class="form-label" for="general_remarks">General Remarks</label>
                                    <textarea name="general_remarks" id="general_remarks" cols="20" rows="2" class="form-control"></textarea>
                                </div>
                                <div class="row mt-2 d-none" id="entryForm">
                                    <div class="col-12 text-center" style="background: #F3F2F7">
                                        <h4 id="entryDate" style="padding-top: 7px;">{{date('Y-m-d')}}</h4>
                                    </div>
                                    <div class="voucher_entry" id="voucher_entry">
                                        
                                    </div>
                                    <div class="row" id="appendTotalColumns"></div>
                                    <div class="d-flex justify-content-end">
                                        <a href="{{ route('vouchers.index') }}" class="btn btn-outline-danger">Cancel</a>
                                        <button type="submit" class="form_save btn btn-primary ms-1"
                                            id="save">Save</button>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection
@section('scripts')
    <script src="{{asset('assets/js/scripts.js')}}"></script>
    <script>
        var type_limit = 0;
        var i = 0;
        var voucherTypePermissions = [];
        var voucherEntry = [];
        $(document).ready(function() {
            $('#voucher_type').on('change', function(){
                var voucherTypeId = $(this).val();
                $.ajax({
                    url: "{{ url('vouchertype') }}" + "/" + voucherTypeId,
                    type: "GET",
                    success: function(response) {
                        $('#entryForm').removeClass('d-none');
                        type_limit = response.type_limit;
                        voucherTypePermissions = JSON.parse(response.permissions);
                        var subTypeHtml = '';
                        i = 0;
                        if(response.subType == 1){
                            subTypeHtml += `
                            <div class="col-12">
                                <div class="mb-1">
                                    <label class="form-label" for="customer">Customers</label>
                                    <select name="customer" id="customer" class="subType form-select" data-placeholder="Select Customer" required>
                                        <option value=""></option>
                                        @foreach ($customers as $customer)
                                            <option value="{{ $customer->id }}">{{ $customer->name }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            `;
                        }
                        if(response.subType == 2){
                            subTypeHtml += `
                            <div class="col-12">
                                <div class="mb-1">
                                    <label class="form-label" for="supplier">Suppliers</label>
                                    <select name="supplier" id="supplier" class="subType form-select" data-placeholder="Select Supplier" required>
                                        <option value=""></option>
                                        @foreach ($suppliers as $supplier)
                                            <option value="{{ $supplier->id }}">{{ $supplier->name }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            `;
                        }
                        if(response.subType == 3){
                            subTypeHtml += `
                            <div class="row">
                                <div class="col-md-4 col-12">
                                    <div class="mb-1">
                                        <label class="form-label" for="bank">Banks</label>
                                        <select name="bank" id="bank" class="subType form-select" data-placeholder="Select Bank" required>
                                            <option value=""></option>
                                            @foreach ($bankcash as $bank)
                                                <option value="{{ $bank->id }}">{{ $bank->name }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-4 col-12">
                                    <div class="mb-1">
                                        <label class="form-label" for="cheque_no">Cheque No</label>
                                        <input type="text" name="cheque_no" id="cheque_no" class="form-control" placeholder="Cheque No">
                                    </div>
                                </div>
                                <div class="col-md-4 col-12">
                                    <div class="mb-1">
                                        <label class="form-label" for="cheque_date">Cheque Date</label>
                                        <input type="date" name="cheque_date" id="cheque_date" class="form-control" value="{{date('Y-m-d')}}">
                                    </div>
                                </div>
                            </div>
                            `;
                        }
                        $('#subType').html(subTypeHtml);
                        $('.subType').select2();
                        $('#voucher_entry').empty();
                        appendRow(voucherTypePermissions,totalColumn='yes');
                    }
                });
            });

            $('#date').on('change', function(){
                $('#entryDate').text($(this).val());
            });
            $("#add_voucher").submit(function(e) {
                e.preventDefault();
                if(checkENT() != true){
                    return false;
                }
                var voucherAmount = CalculateVoucherAmount().split(',');
                // var type_limit = checkVoucherTypeLimit();
                // if(voucherAmount[0] > type_limit){
                //     Swal.fire({
                //         icon: 'error',
                //         title: 'Oops',
                //         text: 'Your voucher limit excedeed!',
                //     });
                //     return false;
                // }
                ButtonStatus('#save',true);
                blockUI();
                $('.entryRow .select2').prop('disabled',false);
                var formData = new FormData(this);
                formData.append('debit_amount', voucherAmount[0]);
                formData.append('credit_amount', voucherAmount[1]);
                formData.append('type_limit', type_limit);
                $.ajax({
                    url: "{{ route('vouchers.store') }}",
                    type: "POST",
                    data: formData,
                    processData: false,
                    contentType: false,
                    responsive: true,
                    success: function(response) {
                        ButtonStatus('#save',false);
                        $.unblockUI();
                        if (response.errors) {
                            $.each(response.errors, function(index, value) {
                                Toast.fire({
                                    icon: 'error',
                                    title: value
                                })
                            });
                        }
                        else if (response.customMessage) {
                            Swal.fire({
                                icon: 'error',
                                title: 'Oops',
                                text: response.customMessage,
                            });
                        }
                        else if (response.error_message) {
                            Toast.fire({
                                icon: 'error',
                                title: 'An error has been occured! Please Contact Administrator.'
                            })
                        } 
                        else {
                            $('#add_voucher')[0].reset();
                            Toast.fire({
                                icon: 'success',
                                title: 'Voucher has been Added Successfully!'
                            });
                            setTimeout(function(){
                                window.location.href = "{{ url('vouchers') }}";
                            }, 1000);
                        }
                    }
                });
            });
        })

        $(document).on('click','.AddBtn',function(){
            if(checkEmpty(this) != true){
                return false;
            }
            if(checkENT() != true){
                return false;
            }
            lockRow(this);
            appendRow(voucherTypePermissions);
            $(this).closest('.appendBtn').replaceWith(`
            <div style="margin-bottom: 19px;" class="appendBtn">
                <button class="btn btn-sm btn-outline-primary me-50 editRow" type="button" title="Edit Row" onclick="ResetRow(this)">
                        <i class="far fa-edit"></i>
                </button>
                <button class="btn btn-sm btn-outline-danger deleteRow" type="button" title="Delete" onclick="removeRow(this)">
                    <i data-feather="x"></i>
                </button>
            </div>`);
            if (feather) {
                feather.replace({
                    width: 14,
                    height: 14
                });
            }
        });
    </script>
@endsection