<?php

use App\Models\User;
use App\Models\Financialyear;
use App\Models\VoucherDetail;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use Spatie\Activitylog\Models\Activity;


if(!function_exists('loginUserId')){
    function loginUserId(){
        return Auth::user()->id;
    }
}

if(!function_exists('userName')){
    function userName($id){
        return User::find($id)->name;
    }
}

if(!function_exists('loginUserFiscalYears')){
    function loginUserFiscalYears(){
        if(empty(Auth::user()->user_departments)){
            return [];
        }
        return json_decode(Auth::user()->user_departments);
    }
}

if(!function_exists('loginUserLocations')){
    function loginUserLocations(){
        if(empty(Auth::user()->locations)){
            return [];
        }
        return json_decode(Auth::user()->locations);
    }
}

if(!function_exists('loginUserAccountCodes')){
    function loginUserAccountCodes(){
        if(empty(Auth::user()->account_codes)){
            return [];
        }
        return json_decode(Auth::user()->account_codes);
    }
}

if(!function_exists('loginUserVoucherTypes')){
    function loginUserVoucherTypes(){
        $types = DB::table('user_voucher_types')->where('user_id', loginUserId())->pluck('voucher_type_id')->toArray();
        return count($types) > 0 ? $types : []; 
    }
}

if(!function_exists('InsertFile')){
    function InsertFile($file,$folder){
        $fileOrignalName = $file->getClientOriginalName();
        // $file_path = '/exemption_certificates';
        $path = public_path() . $folder;
        $filename = time().'_'.rand(000 ,999).'.'.$file->getClientOriginalExtension();
        $file->move($path, $filename);
        $path = $folder.'/'.$filename;
        return $path;
    }
}

if(!function_exists('InsertMultipleFiles')){
    function InsertMultipleFiles($files,$folder){
        foreach ($files as $key => $file) {
            $fileOrignalName = $file->getClientOriginalName();
            // $file_path = '/exemption_certificates';
            $path = public_path() . $folder;
            $filename = time().'_'.rand(000 ,999).'.'.$file->getClientOriginalExtension();
            $file->move($path, $filename);
            $paths[] = $folder.'/'.$filename;
        }
        return $paths;
    }
}

if(!function_exists('getAccountByID')){
    function getAccountByID($accountID){
        $account = DB::table('accounts')->where('id', $accountID)->get(['id','code','description']);
        return $account;
    }
}

if(!function_exists('UserVoucherTypePermissions')){
    function UserVoucherTypePermissions($voucherTypeId){
        // $data = [];
        $voucherType = DB::table('user_voucher_types')->where('user_id', loginUserId())->where('voucher_type_id',$voucherTypeId)->first(['type_limit','permissions']);
        // if(!empty($voucherType)){
            $data['permissions'] = !empty($voucherType->permissions) ? json_decode($voucherType->permissions) : [];
            $data['type_limit'] = !empty($voucherType->type_limit) ? $voucherType->type_limit : 0;
        // }
        return $data;
    }
}

if(!function_exists('cleanAmount')){
    function cleanAmount($amount){
        $amount = str_replace( ',', '', $amount);
        return $amount;
    }
}

if(!function_exists('openingBalance')){
    function openingBalance($account_id,$date,$flag = false){
        $openingBalance = 0;
        if($flag == true){
            $fiscal_year = Financialyear::where([
                ['open_close','=','Open'],
                ['status','=','Active'],
                ['start_date','<=',$date],
                ['end_date','>=',$date],
            ])->first(['id','start_date']);
            if(!empty($fiscal_year)){
                $openingDebitBalance = VoucherDetail::where('account_id',$account_id)->where([['date', '<', $date],['date', '>=', $fiscal_year->start_date],['amount','>', 0]])->sum('amount');
                $openingCreditBalance = VoucherDetail::where('account_id',$account_id)->where([['date', '<', $date],['date', '>=', $fiscal_year->start_date],['amount','<', 0]])->sum('amount');
                $openingBalance = $openingDebitBalance + $openingCreditBalance;
            }
        }
        else{
            $openingBalance = VoucherDetail::where('account_id',$account_id)->whereDate('date', '<', $date)->sum('amount');
        }
        return $openingBalance;
    }
}

if(!function_exists('checkAccountClass')){
    function checkAccountClass($class_id){
        $accountClass = DB::table('account_classes')->where('id',$class_id)->first(['id', 'name']);
        return $accountClass;
    }
}

if(!function_exists('getLogsByUuid')){
    function getLogsByUuid($logId,$batchUuid){
        return Activity::where('id', '!=', $logId)->forBatch($batchUuid)->get();
    }
}

function numberTowords($number) {
    $Bn = floor($number / 1000000000); // Billions (giga) /
    $number -= $Bn * 1000000000;
    $Gn = floor($number / 1000000);  // Millions (mega) /
    $number -= $Gn * 1000000;
    $kn = floor($number / 1000);     // Thousands (kilo) /
    $number -= $kn * 1000;
    $Hn = floor($number / 100);      // Hundreds (hecto) /
    $number -= $Hn * 100;
    $Dn = floor($number / 10);       // Tens (deca) /
    $n = $number % 10;               // Ones //

    $res = "";

    if ($Bn)
        $res .= numberTowords($Bn) . " Billion";
    if ($Gn)
        $res .= (empty($res) ? "" : " ") . numberTowords($Gn) . " Million";
    if ($kn)
        $res .= (empty($res) ? "" : " ") . numberTowords($kn) . " Thousand";
    if ($Hn)
        $res .= (empty($res) ? "" : " ") . numberTowords($Hn) . " Hundred";

    $ones = array("", "One", "Two", "Three", "Four", "Five", "Six",
        "Seven", "Eight", "Nine", "Ten", "Eleven", "Twelve", "Thirteen",
        "Fourteen", "Fifteen", "Sixteen", "Seventeen", "Eighteen",
        "Nineteen");
    $tens = array("", "", "Twenty", "Thirty", "Forty", "Fifty", "Sixty",
        "Seventy", "Eighty", "Ninety");

    if ($Dn || $n) {
        if (!empty($res))
            $res .= " and ";
        if ($Dn < 2)
            $res .= $ones[$Dn * 10 + $n];
        else {
            $res .= $tens[$Dn];
            if ($n)
                $res .= "-" . $ones[$n];
        }
    }

    if (empty($res))
        $res = "zero";
    return $res;
}