<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DataTables;
use App\Models\Level1;
use Illuminate\Support\Facades\DB;
use App\Models\AccountClass;
use Illuminate\Validation\ValidationException;

class Level1Controller extends Controller
{
    public function __construct(){
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $accountclass = DB::table('account_classes')->get();
        $level_1 = Level1::join('account_classes','level_1.account_class_id', '=' , 'account_classes.id')->select('level_1.*','account_classes.name');
        if($request->ajax()){
            return DataTables::of($level_1)->addIndexColumn()->make(true);
        }
      
        // $accountclasseee = DB::table('account_classes')->join('level_1','level_1.account_class_id', '=','account_classes.id')->get();
        return view('admin.level.index',get_defined_vars());
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    { 
        try{
            $this->validate($request , [
                'code' => 'required|unique:level_1',
                'description' => 'required',
                'account_class' => 'required',
            ]);
            $Level = new Level1();
            $Level->code = $request->code;
            $Level->description = $request->description;
            $Level->account_class_id = $request->account_class;
            $Level->save();
            return ['code'=>'200','message'=>'success'];
        }
        catch(\Exception | ValidationException $e){
            if($e instanceof ValidationException){
                return ['code'=>'422','errors' => $e->errors()];
            }
            else{
                return ['code'=>'500','error_message'=>$e->getMessage()];
            }
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $Level = Level1::find($id);
        return response([
            'Level' => $Level,
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        try{
            $this->validate($request , [
                'code' => 'required|unique:level_1,code,'.$id,
            ]);
            // "form_field_name" => 'required|unique:db_table_name,db_table_column_name,'.$id
           
            $Level = Level1::find($id);
            $Level->code = $request->code;
            $Level->description = $request->edit_description;
            $Level->account_class_id = $request->account_class;
            $Level->update();
            return ['code'=>'200','message'=>'success'];
        }
        catch(\Exception | ValidationException $e){
            if($e instanceof ValidationException){
                return ['code'=>'422','errors' => $e->errors()];
            }
            else{
                return ['code'=>'500','error_message'=>$e->getMessage()];
            }
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try{
            $Level = Level1::find($id);
            $Level->delete();
            return ['code'=>'200','message'=>'success'];
        }
        catch(\Exception $e){
            return ['code'=>'500','error_message'=>$e->getMessage()];
        }
    }
}
