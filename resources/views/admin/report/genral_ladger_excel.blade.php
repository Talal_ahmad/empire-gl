<html>

<head>
    <title>Document</title>
</head>

<body>
    @if (isset($levels) && count($levels) > 0)
        @foreach ($levels as $level1)
            @if (count($level1->level2) > 0)
                @foreach ($level1->level2 as $level2)
                    @if (count($level2->accounts) > 0)
                        @foreach ($level2->accounts as $data)
                            @if (count($data->voucherdetails) > 0)
                                @php
                                    $openingBalance = openingBalance($data->id, request('fromDate'));
                                @endphp
                                    <table>
                                        <thead>
                                            <tr>
                                                <td>{{ env('COMPANY') }}</td>
                                                <td>General Ledger</td>
                                                <td>Control A/c</td>
                                                <td>{{ $data->code }} {{ $data->description }}</td>
                                                <td>From date : {{ request('fromDate') }}</td>
                                                <td>To date : {{ request('toDate') }}</td>
                                            </tr>
                                            <tr>
                                                <th>Date</th>
                                                <th>V.Tape/No.</th>
                                                <th>Description</th>
                                                <th style="text-align: right !important">Debit(Rs.)</th>
                                                <th style="text-align: right !important">Credit(Rs.)</th>
                                                <th style="text-align: right !important">Balance(Rs.)</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td colspan="2"></td>
                                                <td style="white-space: nowrap;"> <b>Opening Balance:</b>
                                                </td>
                                                <td colspan="2"></td>
                                                @if ($openingBalance > 0)
                                                        <td style="text-align: right;white-space: nowrap;"> <b> {{ number_format($openingBalance, 2) }} DR</b>
                                                        </td>
                                                        @else
                                                        <td style="text-align: right;white-space: nowrap;"> <b> {{ number_format(abs($openingBalance), 2) }} CR</b></td>
                                                        @endif
                                                @php
                                                    $blance = 0;
                                                    $dbtotel = 0;
                                                    $crtotel = 0;
                                                    $blance = $blance + $openingBalance;
                                                @endphp
                                            </tr>
                                            @foreach ($data->voucherdetails as $detail)
                                                <tr style="border-top: 2px solid #000;">
                                                    <td style="white-space: nowrap;">
                                                        {{ date('j M y', strtotime($detail->date)) }}</td>
                                                    <td style="white-space: nowrap;">
                                                        {{ $detail->vouchertype->voucher_type . '-' . $detail->location->short_name }}
                                                        {{ $detail->voucher_no }}</td>
                                                    <td>{{ $detail->line_remarks }} -
                                                        {{ !empty($detail->voucher->igp_number) ? $detail->voucher->igp_number : $detail->voucher->ogp_number }}
                                                    </td>
                                                    @if ($detail->amount >= 0)
                                                        <?php $dbtotel = $detail->amount + $dbtotel; ?>
                                                        <td style="text-align: right">
                                                            {{ number_format(abs($detail->amount), 2) }}
                                                        </td>
                                                        <td style="text-align: right">-</td>
                                                    @else
                                                        <td style="text-align: right">-</td>
                                                        <?php $crtotel = $detail->amount + $crtotel; ?>
                                                        <td style="text-align: right">
                                                            {{ number_format(abs($detail->amount), 2) }}
                                                        </td>
                                                    @endif
                                                    <?php
                                                    $blance = $blance + $detail->amount;
                                                    ?>
                                                    @if ($blance < 0)
                                                        <td style="text-align: right">
                                                            {{ '(' . number_format(abs($blance), 2) . ')' }}
                                                        </td>
                                                    @else
                                                        <td style="text-align: right">
                                                            {{ number_format(abs($blance), 2) }}</td>
                                                    @endif
                                                </tr>
                                            @endforeach
                                            <tr style="border-top: 2px solid #000;">
                                            </tr>
                                            <tr>
                                                <td></td>
                                                <td></td>
                                                <td style="white-space: nowrap;"><b> Closing Balance:</b>
                                                </td>
                                                <td style="text-align: right">
                                                    <b>{{ number_format(abs($dbtotel), 2) }}</b> </td>
                                                <td style="text-align: right">
                                                    <b>{{ number_format(abs($crtotel), 2) }}</b> </td>
                                                @if ($blance >= 0)
                                                    <td style="text-align: right">
                                                        <b>{{ number_format(abs($blance), 2) }}</b> </td>
                                                @else
                                                    <td style="text-align: right">
                                                        <b>{{ '(' . number_format(abs($blance), 2) . ')' }}</b>
                                                    </td>
                                                @endif
                                            </tr>
                                        </tbody>
                                    </table>
                            @endif
                        @endforeach
                    @endif
                @endforeach
            @endif
        @endforeach
    @endif
</body>

</html>
